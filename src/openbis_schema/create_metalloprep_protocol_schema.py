#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""

    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for metallographic preparation steps

"""

from pybis import Openbis
from schema_helpers import *

# Maximum number of steps of either grinding or polishing
# We don't add more steps to keep the ELN entry less cluttered

MAX_NUM_STEPS_METALLO = 12


# openBIS codes for the objects (also used in other modules)

code_mechprep = "METALLOGRAPHY_PROTOCOL"
code_grinding = "METALLOGRAPHY_GRINDING_PROTOCOL"
code_mechpolishing = "METALLOGRAPHY_POLISHING_PROTOCOL"
code_electropolish = "METALLOGRAPHY_ELECTROPOLISHING_PROTOCOL"
code_polishing_etching = "METALLOGRAPHY_POLISHING_WITH_ETCHING_PROTOCOL"


def create_metalloprep_protocol_schema(oBis: Openbis):
    """Creates Metallographic Prepration Object Types.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    polish_cloth_type = "DISC_CLOTH_TYPE"
    terms_polish_cloth = [
        {
            "code": "GALAXY_ALPHA",
            "label": "Galaxy Alpha",
            "description": "Galaxy Alpha from QATM",
        },
        {
            "code": "GALAXY_DELTA",
            "label": "Galaxy Delta",
            "description": "Galaxy Delta from QATM",
        },
        {
            "code": "GALAXY_OMEGA",
            "label": "Galaxy Omega",
            "description": "Galaxy Omega from QATM",
        },
        {
            "code": "GALAXY_SIGMA",
            "label": "Galaxy Sigma",
            "description": "Galaxy Sigma from QATM",
        },
        {
            "code": "POLARIS_M",
            "label": "Polaris M",
            "description": "Polaris M Diamond Plate from QATM",
        },
        {"code": "MD_CHEM", "label": "MD-Chem", "description": "MD-Chem from Struers"},
        {"code": "MD_DAC", "label": "MD-Dac", "description": "MD-Dac from Struers"},
        {"code": "MD_DUR", "label": "MD-Dur", "description": "MD-Dur from Struers"},
        {"code": "MD_FLOC", "label": "MD-Floc", "description": "MD-Floc from Struers"},
        {"code": "MD_MOL", "label": "MD-Mol", "description": "MD-Mol from Struers"},
        {"code": "MD_NAP", "label": "MD-Nap", "description": "MD-Nap from Struers"},
        {"code": "MD_PAN", "label": "MD-Pan", "description": "MD-Pan from Struers"},
        {"code": "MD_PLAN", "label": "MD-Plan", "description": "MD-Plan from Struers"},
        {"code": "MD_PLUS", "label": "MD-Plus", "description": "MD-Plus from Struers"},
        {"code": "MD_SAT", "label": "MD-Sat", "description": "MD-Sat from Struers"},
    ]
    voc_cloth_type = oBis.new_vocabulary(
        code=polish_cloth_type,
        description="Disc / Plate / Cloth Type (Polishing + Grinding)",
        terms=terms_polish_cloth,
    )
    register_controlled_vocabulary(oBis, voc_cloth_type, terms_polish_cloth)

    abrasive_type_code = "ABRASIVE_TYPE"
    terms_abrasive = [
        {"code": "ALUMINA", "label": "Alumina (Struers OP-A)", "description": "OP-A"},
        {
            "code": "COLLOIDAL_SILICA",
            "label": "Colloidal silica (Struers OP-U)",
            "description": "OP-U",
        },
        {
            "code": "FUMED_SILICA",
            "label": "Fumed silica (Struers OP-S)",
            "description": "OP-S",
        },
        {
            "code": "DIAMOND_PLATE",
            "label": "Diamond (Diamond Plate)",
            "description": "Diamond (Diamond Plate)",
        },
        {
            "code": "DIAMOND_MONO",
            "label": "Monocrystalline Diamond (Suspension / Paste)",
            "description": "Monocrystalline Diamond (Suspension / Paste)",
        },
        {
            "code": "DIAMOND_POLY",
            "label": "Polycrystalline Diamond (Suspension / Paste)",
            "description": "Polycrystalline Diamond (Suspension / Paste)",
        },
        {
            "code": "DIAMOND_MIX",
            "label": "(Mono + Poly)crystalline Diamond (Suspension / Paste)",
            "description": "Mix of Monocrystalline and Polycrystalline Diamonds",
        },
        {
            "code": "SILICON_CARBIDE",
            "label": "SiC (Sandpaper)",
            "description": "Sandpaper / Schleifpapier",
        },
    ]
    voc_abrasive_type = oBis.new_vocabulary(
        code=abrasive_type_code,
        description="Abrasive used to alter sample",
        terms=terms_abrasive,
    )
    register_controlled_vocabulary(oBis, voc_abrasive_type, terms_abrasive)

    # #########################################################################
    # Property Types
    # #############################p###########################################

    # Number of steps used in grinding / polishing

    pt_nsteps = oBis.new_property_type(
        code="NUMBERSTEPS",
        label="Number of Steps (incl. cleaning)",
        description="Number of Steps in either grinding or polishing (excluding ultrasonic cleaning)",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_nsteps)

    for s_id in range(1, 1 + MAX_NUM_STEPS_METALLO):
        pt_grit = oBis.new_property_type(
            code="GRIT_%s" % s_id,
            label="Grit / Grain Size %s" % s_id,
            description="Grit %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_grit)

        pt_abrasive_type = oBis.new_property_type(
            code="ABRASIVE_TYPE_%s" % s_id,
            label="Abrasive Type %s" % s_id,
            description="Abrasive Type %s" % s_id,
            dataType="CONTROLLEDVOCABULARY",
            vocabulary=voc_abrasive_type.code,
        )
        register_property_type(oBis, pt_abrasive_type)

        pt_lubricant = oBis.new_property_type(
            code="LUBRICANT_%s" % s_id,
            label="Lubricant %s" % s_id,
            description="Lubricant for step %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_lubricant)

        pt_suspension = oBis.new_property_type(
            code="SUSPENSION_%s" % s_id,
            label="Suspension %s" % s_id,
            description="Suspension for step %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_suspension)

        pt_time = oBis.new_property_type(
            code="TIME_%s" % s_id,
            label="Time %s" % s_id,
            description="Time for step %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_time)

        pt_sheets = oBis.new_property_type(
            code="NUM_SHEETS_%s" % s_id,
            label="Number of sheets used in step %s" % s_id,
            description="Number of sheets used in step %s" % s_id,
            dataType="INTEGER",
        )
        register_property_type(oBis, pt_sheets)

        pt_cloth_type = oBis.new_property_type(
            code="DISC_CLOTH_TYPE_%s" % s_id,
            label="Polishing cloth / Grinding plate %s" % s_id,
            description="Polishing cloth type used for step %s / Grinding plate type used for step %s"
            % (s_id, s_id),
            dataType="CONTROLLEDVOCABULARY",
            vocabulary=voc_cloth_type,
        )
        register_property_type(oBis, pt_cloth_type)

        pt_pressure = oBis.new_property_type(
            code="PRESSURE_%s_AUTOPOLISH" % s_id,
            label="Pressure %s" % s_id,
            description="Pressure for step %s - Autopolisher" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_pressure)

        pt_speed = oBis.new_property_type(
            code="SPEED_%s" % s_id,
            label="Speed%s (rpm)" % s_id,
            description="Motor speed (in rpm) during step %s" % s_id,
            dataType="INTEGER",
        )
        register_property_type(oBis, pt_speed)

        pt_lubricant_rate = oBis.new_property_type(
            code="LUBRICANT_RATE_%s" % s_id,
            label="Lubricant rate %s" % s_id,
            description="Time interval between two consecutive additions of lubricant during step %s"
            % s_id,
            dataType="INTEGER",
        )
        register_property_type(oBis, pt_lubricant_rate)

        # Post-polish etching description

        pt_etch = oBis.new_property_type(
            code="ETCHING_DESC_%s" % s_id,
            label="Etching Description %s" % s_id,
            description="How etching is performed after step %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_etch)

        # Post-polish etching duration

        pt_etchtime = oBis.new_property_type(
            code="ETCHING_TIME_%s" % s_id,
            label="Etching Time %s" % s_id,
            description="How long etching is performed after step %s" % s_id,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_etchtime)
    # Pre-polish etching description (free-text)

    pt_etch0 = oBis.new_property_type(
        code="ETCHING_DESC_0",
        label="Etching Description 0",
        description="How etching is performed before first polishing",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_etch0)

    # Pre-polish etching duration (free-text)

    pt_etchtime0 = oBis.new_property_type(
        code="ETCHING_TIME_0",
        label="Etching Time 0",
        description="How long etching is performed before first polishing",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_etchtime0)

    # Etching Solution (free-text)

    pt_etch_sol = oBis.new_property_type(
        code="ETCHINGSOLUTION",
        label="Etching Solution",
        description="Etching Solution",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_etch_sol)

    # #########################################################################
    # Objects
    # #########################################################################

    # Generic Metallographic Preparation Protocol (default for collections)

    obj_mechprep_protocol = oBis.new_object_type(
        code=code_mechprep,
        generatedCodePrefix="METALLO",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_mechprep_protocol)
    obj_mechprep_protocol = oBis.get_object_type(code_mechprep)
    obj_mechprep_protocol.description = (
        "Generic Metallography Protocol - USE ONLY WHEN CREATING A COLLECTION"
    )
    register_object_type(oBis, obj_mechprep_protocol)

    obj_mechprep_protocol = oBis.get_object_type(code_mechprep)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("$BARCODE", 0, None),
        ],
        "Preparation Details": [
            ("NUMBERSTEPS", 1, None),
            ("GRIT_1", 1, None),
            ("GRIT_2", 0, None),
        ],
        "Comments": [
            ("COMMENTS", 0, None),
            ("$XMLCOMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_mechprep_protocol, sec2props)

    # Electro polishing

    obj_electropolish_protocol = oBis.new_object_type(
        code=code_electropolish,
        generatedCodePrefix="ELECTRO_POLISH",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_electropolish_protocol)
    obj_electropolish_protocol = oBis.get_object_type(code_electropolish)
    obj_electropolish_protocol.description = "Electro-polishing Protocol"
    register_object_type(oBis, obj_electropolish_protocol)

    obj_electropolish_protocol = oBis.get_object_type(code_electropolish)

    # Define display order
    # TEMPERATURE and VOTLAGE are defined in general_properties.py and are in
    # SI units

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Properties": [
            ("TEMPERATURE", 1, None),
            ("VOLTAGE", 1, None),
            ("ELECTROLYTE", 1, None),
            ("TIME", 0, None),
        ],
    }
    assign_property_types(obj_electropolish_protocol, sec2props)

    # Grinding Protocol

    obj_grinding_protocol = oBis.new_object_type(
        code=code_grinding,
        generatedCodePrefix="GRIND",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_grinding_protocol)
    obj_grinding_protocol = oBis.get_object_type(code_grinding)
    obj_grinding_protocol.description = "Grinding Protocol"
    register_object_type(oBis, obj_grinding_protocol)

    obj_grinding_protocol = oBis.get_object_type(code_grinding)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("INSTRUMENT", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Grinding Properties": [
            ("NUMBERSTEPS", 1, None),
        ],
    }
    for s_id in range(1, 1 + MAX_NUM_STEPS_METALLO):
        sec2props["Step %s" % s_id] = [
            ("GRIT_%s" % s_id, 1 if s_id == 1 else 0, None),
            ("ABRASIVE_TYPE_%s" % s_id, 0, None),
            ("LUBRICANT_%s" % s_id, 0, None),
            ("TIME_%s" % s_id, 0, None),
            ("NUM_SHEETS_%s" % s_id, 0, None),
            ("DISC_CLOTH_TYPE_%s" % s_id, 0, None),
        ]
    assign_property_types(obj_grinding_protocol, sec2props)

    # Mechanical Polishing Protocol

    obj_mechpolish_protocol = oBis.new_object_type(
        code=code_mechpolishing,
        generatedCodePrefix="MECH_POLISH",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_mechpolish_protocol)
    obj_mechprep_protocol = oBis.get_object_type(code_mechprep)
    obj_mechpolish_protocol.description = "Mechanical Polishing Protocol"
    register_object_type(oBis, obj_mechpolish_protocol)

    obj_mechpolish_protocol = oBis.get_object_type(code_mechpolishing)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("INSTRUMENT", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Polishing Properties": [
            ("NUMBERSTEPS", 1, None),
        ],
    }
    for s_id in range(1, 1 + MAX_NUM_STEPS_METALLO):
        sec2props["Step %s" % s_id] = [
            ("GRIT_%s" % s_id, 1 if s_id == 1 else 0, None),
            ("ABRASIVE_TYPE_%s" % s_id, 0, None),
            ("DISC_CLOTH_TYPE_%s" % s_id, 0, None),
            ("SUSPENSION_%s" % s_id, 0, None),
            ("LUBRICANT_%s" % s_id, 0, None),
            ("TIME_%s" % s_id, 0, None),
            ("PRESSURE_%s_AUTOPOLISH" % s_id, 0, None),
            ("SPEED_%s" % s_id, 0, None),
            ("LUBRICANT_RATE_%s" % s_id, 0, None),
        ]
    assign_property_types(obj_mechpolish_protocol, sec2props)

    # Mechanical Polishing Protocol with Intermediate Etching

    obj_polishetch_protocol = oBis.new_object_type(
        code=code_polishing_etching,
        generatedCodePrefix="MECH_POLISH_ETCH",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_polishetch_protocol)
    obj_polishetch_protocol = oBis.get_object_type(code_polishing_etching)
    obj_polishetch_protocol.description = (
        "Mechanical Polishing with Intermediate Etching Protocol"
    )
    register_object_type(oBis, obj_polishetch_protocol)

    obj_polishetch_protocol = oBis.get_object_type(code_polishing_etching)
    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("INSTRUMENT", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Polishing Properties": [
            ("NUMBERSTEPS", 1, None),
        ],
        "Etching Properties": [
            ("ETCHINGSOLUTION", 1, None),
        ],
        "Step 0": [
            ("ETCHING_DESC_0", 0, None),
            ("ETCHING_TIME_0", 0, None),
        ],
    }
    for s_id in range(1, 1 + MAX_NUM_STEPS_METALLO):
        sec2props["Step %s" % s_id] = [
            ("GRIT_%s" % s_id, 1 if s_id == 1 else 0, None),
            ("ABRASIVE_TYPE_%s" % s_id, 0, None),
            ("DISC_CLOTH_TYPE_%s" % s_id, 0, None),
            ("SUSPENSION_%s" % s_id, 0, None),
            ("LUBRICANT_%s" % s_id, 0, None),
            ("TIME_%s" % s_id, 0, None),
            ("PRESSURE_%s_AUTOPOLISH" % s_id, 0, None),
            ("SPEED_%s" % s_id, 0, None),
            ("LUBRICANT_RATE_%s" % s_id, 0, None),
            ("ETCHING_DESC_%s" % s_id, 0, None),
            ("ETCHING_TIME_%s" % s_id, 0, None),
        ]
    assign_property_types(obj_polishetch_protocol, sec2props)

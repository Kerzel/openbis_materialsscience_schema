#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with typical software and create corresponding collections

"""
from pybis import Openbis
import logging
import pandas as pd
from datetime import date


from .. import create_software_schema
from . import general_codes
from schema_helpers import register_collection, read_most_recent_csv


def fill_software(oBis: Openbis):

    # Read data from csv

    df_software = read_most_recent_csv("data", "software-")

    # Get all existing software to avoid that the transaction fails if we add those we have already.

    existing_software = oBis.get_objects(
        space=general_codes.space_equipment_code, attrs=["code"]
    )
    existing_software_codes = set(existing_software.df["code"].tolist())

    # Get collections codes

    collections_codes = (
        df_software["EXPERIMENT"].str.split("/", expand=True)[3].tolist()
    )

    ## ########################################################################
    ## Create Collections
    ## ########################################################################

    ## Space

    space_equipment = oBis.get_space(code=general_codes.space_equipment_code)

    ## Project

    software_project_identifier = (
        "/" + space_equipment.code + "/" + general_codes.project_software_code
    )
    project_software = oBis.get_project(software_project_identifier)

    ## Collections

    logging.debug("Create collections for software")
    for collection_code in collections_codes:
        collection = oBis.new_collection(
            type="COLLECTION",
            project=project_software.code,
            code=collection_code,
            props={
                "$name": " ".join(
                    word.capitalize() for word in collection_code.split("_")[:-1]
                ),
                "$default_object_type": create_software_schema.code_obj_software,
            },
        )
        collection_identifier = software_project_identifier + "/" + collection_code
        register_collection(oBis, collection, collection_identifier)
    ## ########################################################################
    ## Fill Collections
    ## ########################################################################

    logging.debug("Fill collections with Software")

    transaction = oBis.new_transaction()

    for index, row in df_software.iterrows():
        code = row["CODE"]
        logging.debug(
            "--> Adding {} to Inventory {}/{}:".format(
                code, index + 1, len(df_software)
            )
        )
        if code in existing_software_codes:
            logging.debug(f"Skipping {code}")
            continue
        props = {
            k.lower(): v
            for k, v in row.to_dict().items()
            if v != "" and k not in ["CODE", "TYPE", "EXPERIMENT"]
        }
        software = oBis.new_object(
            code=code,
            type=create_software_schema.code_obj_software,
            space=space_equipment.code,
            experiment=row["EXPERIMENT"],
            props=props,
        )
        transaction.add(software)
    transaction.commit()

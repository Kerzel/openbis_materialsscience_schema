#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for Scanning Electron Microscopy

    This part contains all relevant schema definitions for SEM imaging.
    The properties of the dataset type (SE) were primarily developed using
    a TESCAN CLARA microscope and extracted from the corresponding TIFF images.
"""


from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects and datasets

code_obj_sem_exp = "SEM_EXP"
code_ds_sem_image = "SEM_DATA"
code_ds_sem_image_annotated = "SEM_DATA_ANNOTATED"


def create_sem_schema(oBis: Openbis):
    """Creates SEM Experiment Object Type and SEM Dataset Type.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """
    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    terms_sem_insitu = [
        {"code": "INSEM", "label": "InSEM", "description": "InSEM"},
        {"code": "MICROMECHA", "label": "Micromecha", "description": "Micromecha"},
        {"code": "KAMMRATHW", "label": "KammrathW", "description": "KammrathW"},
    ]
    voc_sem_insitu = oBis.new_vocabulary(
        code="SEM_INSITU_DEVICE.DEVICES",
        description="InSitu devices for SEM experiments",
        terms=terms_sem_insitu,
    )
    register_controlled_vocabulary(oBis, voc_sem_insitu, terms_sem_insitu)

    terms_electron_gun = [
        {"code": "FEG", "label": "FEG", "description": "Field Emission Gun"},
        {"code": "SCHOTTKY", "label": "Schottky", "description": "Schottky Gun"},
    ]
    voc_electron_gun = oBis.new_vocabulary(
        code="GUN_TYPE_VOCAB", description="Electron Gun Type", terms=terms_electron_gun
    )
    register_controlled_vocabulary(oBis, voc_electron_gun, terms_electron_gun)

    # #########################################################################
    # Property Types
    # #########################################################################

    pt_sem_insitu_device = oBis.new_property_type(
        code="SEM_INSITU_DEVICE",
        label="InSitu devices for SEM Experiments",
        description="InSitu devices for SEM Experiments",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_sem_insitu.code,
    )
    register_property_type(oBis, pt_sem_insitu_device)

    pt_pixel_size_x = oBis.new_property_type(
        code="PIXEL_SIZE_X_NM",
        label="Pixel Size X (nm)",
        description="Physical size of the pixel in the horizontal direction in nanometers",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pixel_size_x)

    pt_pixel_size_y = oBis.new_property_type(
        code="PIXEL_SIZE_Y_NM",
        label="Pixel Size Y (nm)",
        description="Physical size of the pixel in the vertical direction in nanometers",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pixel_size_y)

    pt_potential_mode = oBis.new_property_type(
        code="POTENTIALMODE",
        label="Potential Mode",
        description="Potential Mode as given by the manufacturer",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_potential_mode)

    pt_chamber_pressure = oBis.new_property_type(
        code="CHAMBER_PRESSURE",
        label="Chamber Pressure (Pa)",
        description="Chamber Pressure in Pascals",
        dataType="REAL",
    )
    register_property_type(oBis, pt_chamber_pressure)

    pt_gun_tilt_x = oBis.new_property_type(
        code="GUNTILTX",
        label="Gun Tilt X (%)",
        description="GunTiltX setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gun_tilt_x)

    pt_gun_tilt_y = oBis.new_property_type(
        code="GUNTILTY",
        label="Gun Tilt Y (%)",
        description="GunTiltY setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gun_tilt_y)

    pt_gun_shift_x = oBis.new_property_type(
        code="GUNSHIFTX",
        label="Gun Shift X (%)",
        description="GunShiftX setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gun_shift_x)

    pt_gun_shift_y = oBis.new_property_type(
        code="GUNSHIFTY",
        label="Gun Shift Y (%)",
        description="GunShiftY setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gun_shift_y)

    pt_gun_type = oBis.new_property_type(
        code="GUN_TYPE",
        label="Gun Type",
        description="Electron Gun Type",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_electron_gun.code,
    )
    register_property_type(oBis, pt_gun_type)

    pt_lens_mode = oBis.new_property_type(
        code="LENS_MODE",
        label="Lens Mode",
        description="Lens Mode as given by the manufacturer",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_lens_mode)

    pt_scan_mode = oBis.new_property_type(
        code="SCAN_MODE",
        label="Scan Mode",
        description="Scan Mode as given by the manufacturer",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_scan_mode)

    pt_scan_rotation = oBis.new_property_type(
        code="SCAN_ROTATION",
        label="Scan Rotation (deg)",
        description="Scan Rotation in degrees",
        dataType="REAL",
    )
    register_property_type(oBis, pt_scan_rotation)

    pt_scan_speed = oBis.new_property_type(
        code="SCAN_SPEED",
        label="Scan Speed Setting",
        description="Scan Speed setting as given by the manufacturer",
        dataType="REAL",
    )
    register_property_type(oBis, pt_scan_speed)

    pt_spot_size_setting = oBis.new_property_type(
        code="SPOT_SIZE_SETTING",
        label="Spot Size Setting",
        description="Spot Size setting as given by the manufacturer",
        dataType="REAL",
    )
    register_property_type(oBis, pt_spot_size_setting)

    pt_stigmator_x = oBis.new_property_type(
        code="STIGMATORX",
        label="Stigmator X (%)",
        description="StigmatorX setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stigmator_x)

    pt_stigmator_y = oBis.new_property_type(
        code="STIGMATORY",
        label="Stigmator Y (%)",
        description="StigmatorY setting in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stigmator_y)

    pt_emission_current = oBis.new_property_type(
        code="EMISSION_CURRENT_UA",
        label="Emission Current (μA)",
        description="Emission Current in μA",
        dataType="REAL",
    )
    register_property_type(oBis, pt_emission_current)

    pt_extraction_voltage = oBis.new_property_type(
        code="EXTRACTION_VOLTAGE",
        label="Extraction Voltage (V)",
        description="Extraction Voltage in volts",
        dataType="REAL",
    )
    register_property_type(oBis, pt_extraction_voltage)

    pt_stage_x = oBis.new_property_type(
        code="STAGE_X_MM",
        label="Stage X (mm)",
        description="Stage displacement in X direction in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_x)

    pt_stage_y = oBis.new_property_type(
        code="STAGE_Y_MM",
        label="Stage Y (mm)",
        description="Stage displacement in Y direction in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_y)

    pt_stage_z = oBis.new_property_type(
        code="STAGE_Z_MM",
        label="Stage Z (mm)",
        description="Stage displacement in Z direction in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_z)

    pt_stage_tilt_alpha = oBis.new_property_type(
        code="STAGE_TILT_A",
        label="Stage Tilt A (deg)",
        description="StageTilt angle alpha in degrees",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_tilt_alpha)

    pt_stage_tilt_beta = oBis.new_property_type(
        code="STAGE_TILT_B",
        label="Stage Tilt B (deg)",
        description="StageTilt angle beta in degrees",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_tilt_beta)

    pt_stage_rotation = oBis.new_property_type(
        code="STAGE_ROTATION",
        label="Stage Rotation (deg)",
        description="StageRotation in degrees",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_rotation)

    pt_aperture_size = oBis.new_property_type(
        code="APERTURE_SIZE",
        label="Aperture Size (μm)",
        description="Aperture Size in μm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_aperture_size)

    pt_working_distance = oBis.new_property_type(
        code="WD",
        label="Working Distance (mm)",
        description="Working Distance in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_working_distance)

    pt_accelerating_voltage = oBis.new_property_type(
        code="ACCELERATING_VOLTAGE",
        label="Acceleration Voltage (kV)",
        description="Acceleration voltage / Electron High Tension in kV",
        dataType="REAL",
    )
    register_property_type(oBis, pt_accelerating_voltage)

    pt_magnification_reference = oBis.new_property_type(
        code="MAGNIFICATIONREFERENCE",
        label="Magnification Reference",
        description="Magnification reference as given by the manufacturer",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_magnification_reference)

    pt_magnification = oBis.new_property_type(
        code="MAGNIFICATION_REAL",
        label="Magnification",
        description="Magnification",
        dataType="REAL",
    )
    register_property_type(oBis, pt_magnification)

    pt_virtual_observer_distance = oBis.new_property_type(
        code="VIRTUALOBSERVERDISTANCE",
        label="Virtual Observer Distance (mm)",
        description="VirtualObserverDistance in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_virtual_observer_distance)

    pt_depth_of_docus = oBis.new_property_type(
        code="DEPTHOFFOCUS",
        label="Depth Of Focus (mm)",
        description="DepthOfFocus in mm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_depth_of_docus)

    pt_line_time = oBis.new_property_type(
        code="LINE_TIME",
        label="Line Time (ms)",
        description="Line time in ms",
        dataType="REAL",
    )
    register_property_type(oBis, pt_line_time)

    pt_frame_time = oBis.new_property_type(
        code="FRAME_TIME",
        label="Frame Time (s)",
        description="Frame time in s",
        dataType="REAL",
    )
    register_property_type(oBis, pt_frame_time)

    pt_spot_size = oBis.new_property_type(
        code="SPOT_SIZE",
        label="Spot Size (nm)",
        description="Spot Size in nm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_spot_size)

    pt_measurement_position = oBis.new_property_type(
        code="MEASUREMENT_POSITION",
        label="Measurement Position",
        description="Position of the measurement on the sample",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_measurement_position)

    pt_insitu_details = oBis.new_property_type(
        code="INSITU_DETAIL",
        label="Details about in-situ experiment",
        description="Details about in-situ experiment",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_insitu_details)

    pt_hfw = oBis.new_property_type(
        code="HFW",
        label="Horizontal Field Size (μm)",
        description="Horizontal Field Size in micrometers",
        dataType="REAL",
    )
    register_property_type(oBis, pt_hfw)

    pt_vfw = oBis.new_property_type(
        code="VFW",
        label="Vertical Field Size (μm)",
        description="Vertical Field Size in micrometers",
        dataType="REAL",
    )
    register_property_type(oBis, pt_vfw)

    pt_contrast = oBis.new_property_type(
        code="CONTRAST",
        label="Contrast (%)",
        description="Contrast as a percentage",
        dataType="REAL",
    )
    register_property_type(oBis, pt_contrast)

    pt_brightness = oBis.new_property_type(
        code="BRIGHTNESS",
        label="Brightness (%)",
        description="Brightness as a percentage",
        dataType="REAL",
    )
    register_property_type(oBis, pt_brightness)

    pt_has_databar = oBis.new_property_type(
        code="HAS_DATABAR",
        label="Has DataBar",
        description="Indicates the presence of a databar in the image",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_has_databar)

    pt_databar_size = oBis.new_property_type(
        code="DATABAR_SIZE",
        label="DataBar Size",
        description="Height of databar in pixels",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_databar_size)

    pt_databar_start = oBis.new_property_type(
        code="DATABAR_START",
        label="DataBar Start",
        description="Index of first pixel in the databar",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_databar_start)

    pt_databar_end = oBis.new_property_type(
        code="DATABAR_END",
        label="DataBar End",
        description="Index of last pixel in the databar",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_databar_end)

    pt_number_of_annotations = oBis.new_property_type(
        code="NUM_ANNOTATIONS",
        label="Number of annotations",
        description="Number of annotations",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_number_of_annotations)

    pt_stem_flag = oBis.new_property_type(
        code="STEM_FLAG",
        label="STEM",
        description="Indicates if microscope is used in STEM mode",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_stem_flag)

    # #########################################################################
    # Objects
    # #########################################################################

    # Any experiment conducted at a Scanning Electron Microscope
    # e.g. Taking an SEM image, Capturing an EBSD map, etc.

    obj_sem_exp = oBis.new_object_type(
        code=code_obj_sem_exp,
        generatedCodePrefix=code_obj_sem_exp,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_sem_exp)
    obj_sem_exp = oBis.get_object_type(code_obj_sem_exp)
    obj_sem_exp.description = "SEM Experiment (SEM Imaging, EBSD, EDS)"
    register_object_type(oBis, obj_sem_exp)

    obj_sem_exp = oBis.get_object_type(code_obj_sem_exp)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$SHOW_IN_PROJECT_OVERVIEW", 0, None),
            ("FINISHED_FLAG", 0, None),
            ("START_DATE", 1, None),
            ("END_DATE", 0, None),
            ("COMMENTS", 0, None),
            ("MEASUREMENT_POSITION", 0, None),
        ],
        "Experimental Details": [
            ("EXPERIMENTAL_STEP.EXPERIMENTAL_GOALS", 0, None),
            ("EXPERIMENTAL_STEP.EXPERIMENTAL_DESCRIPTION", 0, None),
            ("EXPERIMENTAL_STEP.EXPERIMENTAL_RESULTS", 0, None),
        ],
        "InSitu Mechanical Testing": [
            ("SEM_INSITU_DEVICE", 0, None),
            ("INSITU_DETAIL", 0, None),
        ],
        "Environment": [
            ("ROOM_TEMP", 0, None),
            ("ROOM_REL_HUMIDITY", 0, None),
            ("GAS_EXP", 0, None),
        ],
        "SEM Details": [
            ("PRETILT_ANGLE", 0, None),
            ("ACCELERATING_VOLTAGE", 0, None),
            ("STEM_FLAG", 0, None),
        ],
    }
    assign_property_types(obj_sem_exp, sec2props)

    # #########################################################################
    # Dataset Types
    # #########################################################################

    # SEM IMAGE

    ds_sem_image = oBis.new_dataset_type(
        code=code_ds_sem_image,
        description="SEM Image",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_sem_image)

    ds_sem_image = oBis.get_dataset_type(code_ds_sem_image)
    sec2props = {
        "General": [
            ("$NAME", 0, None),  # Name, Comments -> manual entry
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Aquisiton Details": [
            ("SOFTWAREVERSION", 0, None),
            ("INSTITUTION", 0, None),
            ("USER_NAME", 0, None),
            ("DATE", 0, None),
            ("TIME", 0, None),
            ("DATETIME", 0, None),
        ],
        "Image": [
            ("PIXEL_SIZE_X_NM", 0, None),
            ("PIXEL_SIZE_Y_NM", 0, None),
            ("HFW", 0, None),
            ("VFW", 0, None),
            ("MAGNIFICATION_REAL", 0, None),
            ("MAGNIFICATIONREFERENCE", 0, None),
            ("CONTRAST", 0, None),
            ("BRIGHTNESS", 0, None),
            ("LINEAR_LUT", 0, None),
            ("IS_STEM", 0, None),
        ],
        "Databar": [
            ("HAS_DATABAR", 0, None),
            ("DATABAR_START", 0, None),
            ("DATABAR_END", 0, None),
            ("DATABAR_SIZE", 0, None),
        ],
        "Data": [
            ("DATA_SIZE_X", 0, None),
            ("DATA_SIZE_Y", 0, None),
            ("BIT_DEPTH", 0, None),
            ("NUMBER_OF_CHANNELS", 0, None),
            ("NUMBER_OF_FRAMES", 0, None),
        ],
        "Settings": [
            ("CHAMBER_PRESSURE", 0, None),
            ("SCAN_ROTATION", 0, None),
            ("STIGMATORX", 0, None),
            ("STIGMATORY", 0, None),
            ("APERTURE_SIZE", 0, None),
            ("DWELL_TIME", 0, None),
            ("LINE_TIME", 0, None),
            ("FRAME_TIME", 0, None),
            ("EXTRACTION_VOLTAGE", 0, None),
            ("EMISSION_CURRENT_UA", 0, None),
            ("SPOT_SIZE_SETTING", 0, None),
            ("SCAN_MODE", 0, None),
            ("SCAN_SPEED", 0, None),
            ("POTENTIALMODE", 0, None),
            ("LENS_MODE", 0, None),
            ("WD", 0, None),
            ("DEPTHOFFOCUS", 0, None),
            ("VIRTUALOBSERVERDISTANCE", 0, None),
        ],
        "Electron Gun": [
            ("GUNTILTX", 0, None),
            ("GUNTILTY", 0, None),
            ("GUNSHIFTX", 0, None),
            ("GUNSHIFTY", 0, None),
            ("GUN_TYPE", 0, None),
        ],
        "Electron Beam": [
            ("ACCELERATING_VOLTAGE", 0, None),
            ("SPOT_SIZE", 0, None),
            ("BEAM_CURRENT_NA", 0, None),
        ],
        "Stage": [
            ("STAGE_X_MM", 0, None),
            ("STAGE_Y_MM", 0, None),
            ("STAGE_Z_MM", 0, None),
            ("STAGE_ROTATION", 0, None),
            ("STAGE_TILT_A", 0, None),
            ("STAGE_TILT_B", 0, None),
        ],
        "Detector 1": [
            ("DETECTOR1_NAME", 0, None),
            ("DETECTOR1_TYPE", 0, None),
            ("DETECTOR1_GAIN", 0, None),
            ("DETECTOR1_OFFSET", 0, None),
        ],
        "Detector 2": [
            ("DETECTOR2_NAME", 0, None),
            ("DETECTOR2_TYPE", 0, None),
            ("DETECTOR2_GAIN", 0, None),
            ("DETECTOR2_OFFSET", 0, None),
        ],
        "Device": [
            ("DEVICE_MANUFACTURER", 0, None),
            ("DEVICE_NAME", 0, None),
        ],
    }
    assign_property_types(ds_sem_image, sec2props)

    # Annotated SEM image (e.g. through Object Detection Algorithm)

    ds_sem_image_annotated = oBis.new_dataset_type(
        code=code_ds_sem_image_annotated,
        description="Annotated SEM Image",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_sem_image_annotated)

    ds_sem_image_annotated = oBis.get_dataset_type(code_ds_sem_image_annotated)
    sec2props = {
        "General": [
            ("$NAME", 0, None),
            ("COMMENTS", 0, None),  # Name, Comments -> manual entry
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Annotations": [
            ("NUM_ANNOTATIONS", 0, None),
        ],
    }
    assign_property_types(ds_sem_image_annotated, sec2props)

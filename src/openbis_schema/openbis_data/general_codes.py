#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Collect all identifiers and codes that are needed to access the various spaces, projects etc in openBIS when filling the schema
"""

space_materials_code = "MATERIALS"
project_consumables_code = "CONSUMABLES"
project_material_types_code = "MATERIAL_TYPES"

space_equipment_code = "EQUIPMENT"
project_devices_code = "DEVICES"
project_software_code = "SOFTWARE"
project_compute_code = "COMPUTE"
project_storage_code = "STORAGE"

space_methods_code = "METHODS"
project_protocols_code = "PROTOCOLS"

space_samples_code = "SAMPLES"
project_samples_demo_code = "DEMO_SAMPLES"
project_samples_exp_code = "SAMPLES_EXPERIMENTAL"
project_samples_sim_code = "SAMPLES_SIMULATION"

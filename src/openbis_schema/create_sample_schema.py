#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for scientific samples

"""

from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

code_obj_sample = "SAMPLE"


from .general_properties import MAX_NUM_ELEMENTS


def create_sample_schema(oBis: Openbis):
    """Creates a Sample Object Type with corresponding Property Types.

    Here we define the scientific samples
    1) produced experimentally (melting, cutting, metallography, etc.)
    2) generated for simulation workflows (DFT, EBSD, etc.)

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    # Sample Subtype

    terms_subtype = [
        {"code": "THIN_FILM", "label": "Thin Film", "description": "Thin Film"},
        {
            "code": "DIFFUSION_COUPLE",
            "label": "Diffusion Couple",
            "description": "Diffusion Couple",
        },
        {"code": "BULK", "label": "Bulk", "description": "Bulk from casting / melting"},
        {"code": "TEM_LAMELLA", "label": "TEM Lamella", "description": "TEM Lamella"},
        {"code": "APT_TIP", "label": "APT Tip", "description": "APT Tip"},
        {"code": "POWDER", "label": "Powder", "description": "Powder"},
    ]
    voc_subtype = oBis.new_vocabulary(
        code="SAMPLE_SUBTYPE_VOCAB",
        description="Subtype of physical sample",
        terms=terms_subtype,
    )
    register_controlled_vocabulary(oBis, voc_subtype, terms_subtype)

    # Composition / Content

    terms_composition = [
        {"code": "ATOMIC_FRACTION", "label": "at. %", "description": "Atomic Fraction"},
        {"code": "MASS_FRACTION", "label": "wt. %", "description": "Mass Fraction"},
        {"code": "MOLAR_FRACTION", "label": "mol. %", "description": "Molar Fraction"},
        {
            "code": "VOLUME_FRACTION",
            "label": "vol. %",
            "description": "Volume Fraction",
        },
    ]
    voc_composition = oBis.new_vocabulary(
        code="COMPOSITION_TYPE",
        description="How the composition of mixtures is described",
        terms=terms_composition,
    )
    register_controlled_vocabulary(oBis, voc_composition, terms_composition)

    # Single Crystal / Bicrystal / Polycrystal

    terms_crystal_type = [
        {
            "code": "POLYCRYSTALLINE",
            "label": "Polycrystal",
            "description": "Polycrystal",
        },
        {"code": "BICRYSTALLINE", "label": "Bicrystal", "description": "Bicrystal"},
        {
            "code": "OLIGOCRYSTALLINE",
            "label": "Oligocrystal",
            "description": "Oligocrystal",
        },
        {
            "code": "MONOCRYSTALLINE",
            "label": "Monocrystal",
            "description": "Monocrystal",
        },
    ]
    voc_crystal_type = oBis.new_vocabulary(
        code="CRYSTAL_TYPE",
        description="Single crystal / Bicrystal / Polycrystal / Oligocrytsal",
        terms=terms_crystal_type,
    )
    register_controlled_vocabulary(oBis, voc_crystal_type, terms_crystal_type)

    # Point Groups

    point_groups = {
        "PG1": "1",
        "PG-1": "-1",
        "PG2": "2",
        "PG2_M": "2/m",
        "PG3": "3",
        "PG-3": "-3",
        "PG-3M": "-3m",
        "PG3M": "3m",
        "PG4": "4",
        "PG4_M": "4/m",
        "PG4_MMM": "4/mmm",
        "PG-4": "-4",
        "PG-42M": "-42m",
        "PG-43M": "-43m",
        "PG4MM": "4mm",
        "PG6": "6",
        "PG6_M": "6/m",
        "PG6_MMM": "6/mmm",
        "PG-6": "-6",
        "PG-6M2": "-6m2",
        "PG6MM": "6mm",
        "PG23": "23",
        "PG32": "32",
        "PG222": "222",
        "PG422": "422",
        "PG432": "432",
        "PG622": "622",
        "PGM": "m",
        "PGM-3": "m-3",
        "PGM-3M": "m-3m",
        "PGMM2": "mm2",
        "PGMMM": "mmm",
    }
    terms_point_group = []
    for k, v in point_groups.items():
        minus_pos = v.find("-")
        if minus_pos > -1:
            v = v[:minus_pos] + v[minus_pos + 1] + "\u0305" + v[minus_pos + 2 :]
        terms_point_group.append(
            {
                "code": k,
                "label": v,
                "description": "Crystallographic Point Group " + v,
            }
        )
    voc_point_group = oBis.new_vocabulary(
        code="POINT_GROUP_VOCAB",
        description="Crystallographic Point Groups (International Symbol)",
        terms=terms_point_group,
    )
    register_controlled_vocabulary(oBis, voc_point_group, terms_point_group)

    # #########################################################################
    # Plugins
    # #########################################################################

    validation_plugin = oBis.new_plugin(
        name="SAMPLE_VALIDATOR",
        pluginType="ENTITY_VALIDATION",
        entityKind="SAMPLE",  # Sample meaning Object
        script=open("jython_scripts/SampleValidator.py", "r").read(),
    )
    validation_plugin.description = "Validate sample entry (composition, location)"
    register_plugin(oBis, validation_plugin)

    dynamic_property_plugin_1 = oBis.new_plugin(
        name="SAMPLE_CONTENT",
        pluginType="DYNAMIC_PROPERTY",
        entityKind="SAMPLE",  # Sample meaning Object
        script=open("jython_scripts/SampleContent.py", "r").read(),
    )
    dynamic_property_plugin_1.description = (
        "Returns a list of comma-seperated elements contained in a sample"
    )
    register_plugin(oBis, dynamic_property_plugin_1)

    dynamic_property_plugin_2 = oBis.new_plugin(
        name="SAMPLE_CONTENT_FULL",
        pluginType="DYNAMIC_PROPERTY",
        entityKind="SAMPLE",  # Sample meaning Object
        script=open("jython_scripts/SampleContentFull.py", "r").read(),
    )
    dynamic_property_plugin_2.description = "Returns a list of comma-seperated elements contained in a sample with atomic percentages"
    register_plugin(oBis, dynamic_property_plugin_2)

    # #########################################################################
    # Property Types
    # #########################################################################

    pt_comp_desc = oBis.new_property_type(
        code="SAMPLE_SUBTYPE",
        label="Subtype",
        description="Descriptor of physical sample",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_subtype.code,
    )
    register_property_type(oBis, pt_comp_desc)

    # The samples consist of a number of elements / components.
    # Ideally, the composition in terms of elements is specified in atomic %.
    # For edge cases, we provide an option to choose from (at.|mol.|wt.|vol.).

    pt_comp_desc = oBis.new_property_type(
        code="COMPOSITION_DESC",
        label="Composition Description",
        description="How the composition of the sample is described",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_composition.code,
    )
    register_property_type(oBis, pt_comp_desc)

    # We don't add more elements to keep the ELN entry less cluttered

    for eid in range(1, 1 + MAX_NUM_ELEMENTS):
        eord = ordinal(eid)
        pt_element = oBis.new_property_type(
            code="ELEMENT_%s" % eid,
            label="Element %s" % eid,
            description="Symbol of the %s element in the sample" % eord,
            dataType="VARCHAR",
        )
        register_property_type(oBis, pt_element)

        pt_element_atomic_percent = oBis.new_property_type(
            code="ELEMENT_%s_AT_PERCENT" % eid,
            label="%% Element %s" % eid,
            description="Atomic percentage of the %s element" % eord,
            dataType="REAL",
        )
        register_property_type(oBis, pt_element_atomic_percent)

        pt_element_number = oBis.new_property_type(
            code="ELEMENT_%s_NUMBER" % eid,
            label="# Element %s" % eid,
            description="Number of atoms of the %s element in the sample" % eord,
            dataType="INTEGER",
        )
        register_property_type(oBis, pt_element_number)  # Only from simulation files
    # Structure Information (mainly simulation) - Unit cell lengths

    pt_cell_lengths = oBis.new_property_type(
        code="UNIT_CELL_LENGTHS",
        label="Unit Cells Lengths (Å)",
        description="a, b, c (in Ångstroms)",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_cell_lengths)

    # Structure Information (mainly simulation) - Unit cell angles

    pt_cell_angles = oBis.new_property_type(
        code="UNIT_CELL_ANGLES",
        label="Unit Cells Angles (deg)",
        description="alpha, beta, gamma (in degrees)",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_cell_angles)

    # Crystal Type

    pt_crystal_type = oBis.new_property_type(
        code="CRYSTALTYPE",
        label="Crystal Type",
        description="Crystal type (mono, bi, poly, oligo)",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_crystal_type.code,
    )
    register_property_type(oBis, pt_crystal_type)

    # Structure Information (mainly simulation) - Orientation

    pt_orient = oBis.new_property_type(
        code="CRYSTAL_ORIENTATION",
        label="Orientation",
        description="Crystal Orientation",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_orient)

    # Structure Information (mainly simulation) - Sub-Lattice

    pt_sublattice = oBis.new_property_type(
        code="SUBLATTICE",
        label="Sub-Lattice",
        description="Sub-Lattice",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_sublattice)

    # Structure Information (mainly simulation) - Point Group

    pt_point_group = oBis.new_property_type(
        code="POINT_GROUP",
        label="Point Group",
        description="Point Group (short international notation)",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_point_group.code,
    )
    register_property_type(oBis, pt_point_group)

    # Structure Information (mainly simulation) - Space Group

    pt_space_group = oBis.new_property_type(
        code="SPACE_GROUP",
        label="Space Group",
        description="Space Group (International Number)",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_space_group)

    # Structure Information (mainly simulation) - Phases

    pt_phases = oBis.new_property_type(
        code="PHASES",
        label="Phases",
        description="Phases (comma-seperated)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_phases)

    # Structure Information (mainly simulation) - Defects

    pt_defects = oBis.new_property_type(
        code="DEFECTS",
        label="Defects",
        description="Defects (comma-seperated)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_defects)

    # Corrosion flag

    pt_corrosion = oBis.new_property_type(
        code="HAS_CORROSION",
        label="Has Corrosion",
        description="Has the sample been exposed to corrosion effects",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_corrosion)

    # Processed / Transformed flag

    pt_processed = oBis.new_property_type(
        code="IS_TRANSFORMED",
        label="Is Processed / Transformed",
        description="Indicates that the sample is no longer available because it was transformed into another form",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_processed)

    # Archived flag

    pt_archived = oBis.new_property_type(
        code="IS_ARCHIVED",
        label="Archived",
        description="Indicates that the sample was put into archive",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_archived)

    # Dimension: free-text field for (physical) dimensions of the sample

    pt_sample_dim = oBis.new_property_type(
        code="SAMPLE_DIM",
        label="Sample Dimensions",
        description="Sample Dimensions (10 mm x 10 mm x 10 mm or 10 mm ⌀)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_sample_dim)

    # Structure File Location

    pt_file_location = oBis.new_property_type(
        code="FILE_LINK",
        label="File Location",
        description="URL of Structure File, if applicable",
        dataType="HYPERLINK",
    )
    register_property_type(oBis, pt_file_location)

    pt_elements = oBis.new_property_type(
        code="LIST_OF_ELEMENTS",
        label="List Of Elements",
        description="List of elements sorted by atomic percentage and alphabetically in case of ex aequo",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_elements)

    pt_compo = oBis.new_property_type(
        code="LIST_OF_ELEMENTS_FULL",
        label="Composition",
        description="List of elements and atomic percentages sorted by atomic percentage and alphabetically in case of ex aequo",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_compo)

    # #########################################################################
    # Objects
    # #########################################################################

    obj_sample = oBis.new_object_type(
        code=code_obj_sample,
        generatedCodePrefix=code_obj_sample,
        autoGeneratedCode=True,
        validationPlugin=validation_plugin.name,
    )
    register_object_type(oBis, obj_sample)
    obj_sample = oBis.get_object_type(code_obj_sample)
    obj_sample.description = "Physical or virtual sample"
    register_object_type(oBis, obj_sample)

    obj_sample = oBis.get_object_type(code_obj_sample)

    # Only ELEMENT_1 is manadatory to account for pure samples

    composition_properties = [
        ("LIST_OF_ELEMENTS", 0, dynamic_property_plugin_1.name),
        ("LIST_OF_ELEMENTS_FULL", 0, dynamic_property_plugin_2.name),
        ("COMPOSITION_DESC", 1, None),
    ]
    for eid in range(1, 1 + MAX_NUM_ELEMENTS):
        composition_properties += [
            ("ELEMENT_%s" % eid, 1 if eid == 1 else 0, None),
            ("ELEMENT_%s_AT_PERCENT" % eid, 1 if eid == 1 else 0, None),
        ]
    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("SAMPLE_SUBTYPE", 0, None),
            ("LOCATION", 1, None),
            ("SUBLOCATION", 0, None),
            ("SAMPLE_DIM", 1, None),
            ("WEIGHT_G", 0, None),
            ("DATE", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Composition": composition_properties,
        "Structure Details": [
            ("UNIT_CELL_LENGTHS", 0, None),
            ("UNIT_CELL_ANGLES", 0, None),
            ("CRYSTAL_ORIENTATION", 0, None),
            ("CRYSTALTYPE", 0, None),
            ("SPACE_GROUP", 0, None),
            ("POINT_GROUP", 0, None),
            ("SUBLATTICE", 0, None),
            ("PHASES", 0, None),
            ("DEFECTS", 0, None),
            ("FILE_LINK", 0, None),
        ],
        "Structure Details - Atom Counts": [
            ("ELEMENT_%s_NUMBER" % eid, 0, None)
            for eid in range(1, 1 + MAX_NUM_ELEMENTS)
        ],
        "Miscellaneous": [
            ("HAS_CORROSION", 0, None),
            ("IS_TRANSFORMED", 0, None),
            ("IS_ARCHIVED", 0, None),
        ],
    }
    assign_property_types(obj_sample, sec2props, empty_value="empty")
    # empty_value = 'empty' if defined previously and already used
    # Only for VARCHAR fields $NAME, DATE, LOCATION, SAMPLE_DIM, and ELEMENT_1

# openBIS Schema Extension for research in Materials Science and Engineering



## Overview

This repository contains the python scripts to initialise an [openBIS](https://openbis.ch/) instance with a comprehensive set of metadata schemata, as well as example inventory for instruments and consumables.
The schemata contain details for a number of experimental techniques, as well as simulation and machine learning.

The schemata are based on the requirements of the collaborative research centre [CRC 1394](https://www.sfb1394.rwth-aachen.de/index.php?id=2&L=0): Structural and Chemical Atomic Complexity - From Defect Phase Diagrams to Materials Properties.

The schemata and examples will, of course, not perfectly match the needs of all laboratories in the field of materials science and engineering, but hopefully provide a suitable starting point.

## Using the code
Before you start, you need to setup a (new) openBIS instance. The scripts need to be executed by an admin with `INSTANCE_ADMIN` rights.

In a first step, you need to create the schemata. This creates the necessary property types, object types, etc.
```mermaid
graph TD;
    A[Create General Entities] --> B[Create Schema For Samples];
    B --> C[Create Schema For Consumables];
    C --> D[Create Schema For Scientific Instruments];
    D --> E[Create Schema For Software];
    E --> F[Create Schema For Atomistic And DFT Simulations];
    F --> G[Create Schema For Metallographic Preparation Protocols];
    G --> H[Create Schema For Sample Creation And Preparation];
    H --> I[Create Schema For Mechanical Testing Protocols And Experiments];
    I --> J[Create Schema For Slip Line Analysis];
    J --> K[Create Schema For Scanning Electron Microscopy];
    K --> L[Create Schema For EBSD Experiments / Simulation];
    L --> M[Create Schema For Energy Dispersive Spectroscopy];
    M --> N[Create Schema For Optical Microscopy];
    N --> O["Create Schema For (Scanning) Transmission Electron Microscopy"];
    O --> P[Create Schema For Atom Probe Tomography];
    P --> Q[Create Schema For X-Ray Diffraction];
    Q --> R[Create Schema For Electrochemical / Corrosion Analysis];
```
In a second step, you can fill the schema with examples such as instruments, consumables, etc. You might need to manually create `Inventory Spaces` using the ELN-LIMS GUI before adding items to the inventory.  
```mermaid
graph TD;
    A[Create Collections] --> B[Fill Consumables];
    B --> C[Fill Equipment];
    C --> D[Fill Software];
    D --> E[Fill Samples];
```

The code depends on pyBIS (`1.37.0`) that is used to communicate with the openBIS instance (`20.10.7.1`).  
Please set the environment variables `OPENBIS_ENDPOINT` and `OPENBIS_TOKEN` before running the scripts.
```
cd src
python create_openbis_schema.py 
python fill_openbis_schema.py 
```

## Contributors and acknowledgments

### Key Contributors:
- Ulrich Kerzel [0000-0002-4939-6726](https://orcid.org/0000-0002-4939-6726)
- Khalil Rejiba [0009-0009-6465-4023](https://orcid.org/0009-0009-6465-4023)

### Collaborators:
- [SFB1394](https://www.imm.rwth-aachen.de/index.php?id=32&L=1)
- [NFDI-MatWerk](https://nfdi-matwerk.de/)
- [Institute of Physical Metallurgy and Materials Physics](https://www.imm.rwth-aachen.de/index.php?id=2&L=1)  
We acknowledge the significant role played by these institutions and consortia in providing the necessary resources to make this work possible. Their expertise and support have been indispensable throughout the development of this work.

## Funding
Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) as part of Collaborative Research Centre CRC 1394 - Structural and Chemical Atomic Complexity - From Defect Phase Diagrams to Material Properties (project number 409476157) and under the National Research Data Infrastructure – NFDI 38/1 (project number 460247524)

## License
Apache 2.0

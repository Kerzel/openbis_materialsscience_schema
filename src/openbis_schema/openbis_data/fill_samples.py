#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Create collections for experimental and simulation samples and add some examples

"""
from pybis import Openbis
import logging
from datetime import date


from .. import create_sample_schema
from . import general_codes
from schema_helpers import register_collection, register_object


collection_samples_demo_code = "DEMO_SAMPLES_COLLECTION"
collection_samples_exp_code = "SAMPLES_EXPERIMENTAL_COLLECTION"
collection_samples_sim_code = "SAMPLES_SIMULATION_COLLECTION"


def fill_samples(oBis: Openbis):

    ## ########################################################################
    ## Create Collections
    ## ########################################################################

    ## Space

    logging.debug("Space Samples")
    space_samples = oBis.get_space(code=general_codes.space_samples_code)

    ## Project

    project_samples_demo = oBis.get_project(
        "/" + space_samples.code + "/" + general_codes.project_samples_demo_code
    )
    # project_samples_exp = oBis.get_project("/" + space_samples.code + "/" + general_codes.project_samples_exp_code)
    # project_samples_sim = oBis.get_project("/" + space_samples.code + "/" + general_codes.project_samples_sim_code)

    ## Collections

    logging.debug("Create collections for samples")

    # Demo samples

    collection_samples_demo = oBis.new_collection(
        type="COLLECTION",
        project=project_samples_demo.code,
        code=collection_samples_exp_code,
        props={
            "$name": "Demo Samples",
            "$default_object_type": create_sample_schema.code_obj_sample,
        },
    )
    collection_identifier = (
        project_samples_demo.identifier + "/" + collection_samples_exp_code
    )
    register_collection(oBis, collection_samples_demo, collection_identifier)
    collection_samples_demo = oBis.get_collection(collection_identifier)

    ## ########################################################################
    ## Fill Collections
    ## ########################################################################

    logging.debug("Fill collections for samples")
    demo_sample = oBis.new_object(
        type=create_sample_schema.code_obj_sample,
        space=space_samples.code,
        experiment=collection_samples_demo.identifier,
        code="DEMO_SAMPLE_001",
        props={
            "$name": "Demo_Sample_1",
            "location": "RWTH",
            "sample_dim": "100 mm x 100 mm  x 100 mm ",
            "date": date.today().isoformat(),
            "element_1": "Au",
            "element_1_at_percent": 100,
            "composition_desc": "ATOMIC_FRACTION",
        },
    )
    register_object(demo_sample)

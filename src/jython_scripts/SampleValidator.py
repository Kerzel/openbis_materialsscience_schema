def validate(entity, isNew):
    """Checks if atomic percentages are entered correctly and if location is valid."""
    ATOMIC_SYMBOLS = [
        'H','He',
        'Li','Be','B','C','N','O','F','Ne',
        'Na','Mg','Al','Si','P','S','Cl','Ar',
        'K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr',
        'Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe',
        'Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn',
        'Fr','Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg','Cn','Nh','Fl','Mc','Lv','Ts','Og'
    ]
    if isNew:
        properties = entity.properties()
        element_dict_with_percentages = {}
        for p in properties:
            property_code = p.propertyTypeCode()
            value = p.renderedValue()
            if property_code == "LOCATION":
                if not (
                    value.startswith("RWTH")
                    or value.startswith("MPIE")
                    or value.startswith("RUB")
                    or value.startswith("FZJ")
                    or value.startswith("LEM3")
                    or value == "processed"
                    or value == "virtual"
                ):
                    return "Location should be 'processed' or 'virtual' " \
                        "or ORGANIZATION-GROUP:ROOMNUMBER, ORGANIZATION must be one of the following: RWTH,RUB,MPIE,FJZ,LEM3"
            if (
                property_code.startswith("ELEMENT")
                and "PERCENT" not in property_code
                and value != "empty"
            ):
                element_dict_with_percentages[property_code] = [value, 0]
                if value not in ATOMIC_SYMBOLS:
                    return "Please use atomic symbols"
        percent_sum = 0
        for code, (element_symbol, _) in element_dict_with_percentages.items():
            for p in properties:
                property_code = p.propertyTypeCode()
                value = p.renderedValue()
                if property_code == code + "_AT_PERCENT" and value != "empty":
                    element_dict_with_percentages[code][1] = value
                    percent_sum += float(value)
        if not all(x[1] for x in element_dict_with_percentages.values()):
            return "Please enter full composition"
        if any(x[1] == "0.0" for x in element_dict_with_percentages.values()):
            return "Please enter a non null value for the percentage"
        if percent_sum < 99.999 or percent_sum > 100.001:
            return "Make sure percentages add up to 100%"

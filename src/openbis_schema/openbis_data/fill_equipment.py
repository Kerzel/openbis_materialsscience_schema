#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with devices and equipment and create corresponding collections

"""
from pybis import Openbis
import logging
import pandas as pd

from .. import create_instrument_schema
from . import general_codes
from schema_helpers import register_collection, read_most_recent_csv


code_collection_mech_test = "MECH_TEST_COLLECTION"
code_collection_misc_devices = "MISC_DEVICES_COLLECTION"
code_collection_furnaces = "FURNACE_COLLECTION"
code_collection_microscopes = "MICROSCOPE_COLLECTION"
code_collection_mech_prep = "MECH_PREP_COLLECTION"
code_collection_electrodes = "ELECTRODES_COLLECTION"
code_collection_thin_film_synthesis = "THIN_FILM_SYNTH_COLLECTION"


def fill_equipment(oBis: Openbis):

    # Read data from csv

    df_devices = read_most_recent_csv("data", "devices-")
    NUMERICAL_PROPERTIES = ["LOAD_MAX", "STRAIN_RATE_MIN", "DISPLACEMENT_RANGE"]

    # Get all existing consumables to avoid that the transaction fails if we add those we have already.

    existing_devices = oBis.get_objects(
        space=general_codes.space_equipment_code, attrs=["code"]
    )
    existing_devices_codes = set(existing_devices.df["code"].tolist())

    ## ########################################################################
    ## Create Collections
    ## ########################################################################

    ## Space

    space_equipment = oBis.get_space(code=general_codes.space_equipment_code)

    ## Project

    devices_project_identifier = (
        "/" + space_equipment.code + "/" + general_codes.project_devices_code
    )
    project_devices = oBis.get_project(devices_project_identifier)

    ## Collections

    # Mechanical Testing

    logging.debug("Create collections for Mechanical Testing")
    collection_mech_test = oBis.new_collection(
        type="COLLECTION",
        project=general_codes.project_devices_code,
        code=code_collection_mech_test,
        props={
            "$name": "Mechanical Testing",
            "$default_object_type": create_instrument_schema.code_obj_mech_test_device,
        },
    )
    collection_identifier = devices_project_identifier + "/" + code_collection_mech_test
    register_collection(oBis, collection_mech_test, collection_identifier)

    # Electron Microscopes

    logging.debug("Create collections for Elecron Microscopes")
    collection_microscopes = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_microscopes,
        props={
            "$name": "Electron Microscopes",
            "$default_object_type": create_instrument_schema.code_obj_electron_microscope,
        },
    )
    collection_identifier = (
        devices_project_identifier + "/" + code_collection_microscopes
    )
    register_collection(oBis, collection_microscopes, collection_identifier)

    # Misc. Devices

    logging.debug("Create collections for misc. devices")
    collection_misc_devices = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_misc_devices,
        props={
            "$name": "Misc. Devices (OM + XRAY + APT)",
            "$default_object_type": create_instrument_schema.code_obj_instrument,
        },
    )
    collection_identifier = (
        devices_project_identifier + "/" + code_collection_misc_devices
    )
    register_collection(oBis, collection_misc_devices, collection_identifier)

    # Mechanical Preparation

    logging.debug("Create collections for mechanical preparation devices")
    collection_mechprep = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_mech_prep,
        props={
            "$name": "Mechanical Preparation",
            "$default_object_type": create_instrument_schema.code_obj_instrument,
        },
    )
    collection_identifier = devices_project_identifier + "/" + code_collection_mech_prep
    register_collection(oBis, collection_mechprep, collection_identifier)

    # Furnaces

    logging.debug("Create collections for furnaces")
    collection_furnaces = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_furnaces,
        props={
            "$name": "Furnaces",
            "$default_object_type": create_instrument_schema.code_obj_furnace,
        },
    )
    collection_identifier = devices_project_identifier + "/" + code_collection_furnaces
    register_collection(oBis, collection_furnaces, collection_identifier)

    # Electrodes

    logging.debug("Create collections for electrodes")
    collection_electrodes = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_electrodes,
        props={
            "$name": "Electrodes",
            "$default_object_type": create_instrument_schema.code_obj_electrode,
        },
    )
    collection_identifier = (
        devices_project_identifier + "/" + code_collection_electrodes
    )
    register_collection(oBis, collection_electrodes, collection_identifier)

    # Thin Film Synthesis

    logging.debug("Create collections for thin film synthesis devices")
    collection_thin_film_synthesis = oBis.new_collection(
        type="COLLECTION",
        project=project_devices.code,
        code=code_collection_thin_film_synthesis,
        props={
            "$name": "Thin Film Synthesis",
            "$default_object_type": create_instrument_schema.code_obj_instrument,
        },
    )
    collection_identifier = (
        devices_project_identifier + "/" + code_collection_thin_film_synthesis
    )
    register_collection(oBis, collection_thin_film_synthesis, collection_identifier)

    ## ########################################################################
    ## Fill Collections
    ## ########################################################################

    logging.debug("Fill collections with Devices")

    transaction = oBis.new_transaction()

    for index, row in df_devices.iterrows():
        code = row["CODE"]
        logging.debug(
            "--> Adding {} to Inventory {}/{}:".format(code, index + 1, len(df_devices))
        )
        if row["CODE"] in existing_devices_codes:
            logging.debug("Skipping {}".format(code))
            continue
        props = {
            k.lower(): v
            for k, v in row.to_dict().items()
            if v != "" and k not in ["CODE", "TYPE", "EXPERIMENT"]
        }
        props = {
            k: float(v) if k.upper() in NUMERICAL_PROPERTIES else v
            for k, v in props.items()
        }

        device = oBis.new_object(
            code=code,
            type=row["TYPE"],
            space=space_equipment.code,
            experiment=row["EXPERIMENT"],
            props=props,
        )
        transaction.add(device)
    transaction.commit()

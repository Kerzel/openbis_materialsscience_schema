#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pybis import Openbis
import os
import sys

import logging

from .. import create_metalloprep_protocol_schema
from .. import create_sampleprep_protocol_schema
from . import fill_equipment
from . import fill_consumables

"""
Copyright 2023 Ulrich Kerzel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with typical protocols for mechanical preparation
"""
from . import general_codes


collection_metalloprep_code      = 'METALLOGRAPHIC_PREP_COLLECTION'
collection_heattreatment_code    = 'HEATTREATMENT_COLLECTION'
collection_casting_code          = 'CASTING_COLLECTION'

obj_grind_code = 'DEFAULT_GRIND'

def fill_metalloprep_protocols(oBis : Openbis):

    ## ################################################
    ## create collections
    ## ################################################

    ##
    ## Space where the collection lives in
    ##
    # Inventory spaces cannot (yet?) be created in PyBIS,
    # they are an ELN only option. Need to create them in the ELN and then access them here
    #
    logging.debug('Space Methods')
    space_methods = oBis.get_space(code=general_codes.space_methods_code)

    ##
    ##  project
    ##
    project_protocols = oBis.get_project('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code)


    ##
    ## Collections 
    ##

    #
    # Metallographic preparation
    #
    logging.debug('create collection for metallographic preparation')
    collection_metalloprep_new = oBis.new_collection(
        type='COLLECTION',
        project=project_protocols.code,
        code=collection_metalloprep_code,
        props={
            "$name" : "Metallographic Preparation Protocols",
            "$default_object_type" : create_metalloprep_protocol_schema.code_obj_mechprep
        }
    )
    try:
        collection_metalloprep_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))
    collection_metalloprep = oBis.get_collection('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+collection_metalloprep_code) 


    #
    # Heat treatment
    #
    logging.debug('create collection for heat treatment')
    collection_heattreatment_new = oBis.new_collection(
        type='COLLECTION',
        project=project_protocols.code,
        code=collection_heattreatment_code,
        props={
            "$name" : "Heat Treatment Protocols",
            "$default_object_type" : create_sampleprep_protocol_schema.code_obj_heattreatment
        }
    )
    try:
        collection_heattreatment_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))
    collection_heattreatment = oBis.get_collection('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+collection_heattreatment_code) 

    #
    # Casting
    #
    logging.debug('create collection for casting')
    collection_casting_new = oBis.new_collection(
        type='COLLECTION',
        project=project_protocols.code,
        code=collection_casting_code,
        props={
            "$name" : "Casting Protocols",
            "$default_object_type" : create_sampleprep_protocol_schema.code_obj_casting
        }
    )
    try:
        collection_casting_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))
    collection_casting = oBis.get_collection('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+collection_casting_code) 
   


    ## ################################################
    ## fill collections
    ## ################################################
    logging.debug('Fill collections for metallographic preparations')

    #
    # default grinding 
    #
    logging.debug('Fill default grinding')
    default_grind_new = oBis.new_object(
    type  = create_metalloprep_protocol_schema.code_obj_mechprep,
    space = general_codes.space_methods_code,
    experiment=  collection_metalloprep.identifier,
    code = obj_grind_code,
    props={
        '$name'         : 'Default Grinding',
        'numbersteps'   : '2',
        'grit_1'        : '2000',
        'grit_2'        : '4000',
        }   
    )
    try:
        default_grind_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))

    default_grind = oBis.get_object('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+obj_grind_code)
    default_grind.set_parents(['/'+general_codes.space_equipment_code +'/'+general_codes.project_devices_code      +'/'+fill_equipment.obj_struers_330_code,
                               '/'+general_codes.space_materials_code +'/'+general_codes.project_consumables_code  +'/'+fill_consumables.code_obj_ethanol])
    default_grind.save()
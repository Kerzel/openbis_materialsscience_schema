#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pybis import Openbis
import os
import sys
from datetime import date

import logging

from .. import create_ebsd_schema
from . import fill_software 
from . import general_codes

"""
Copyright 2023 Ulrich Kerzel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with typical consumables and create corresponding collections

"""


collection_EBSDSim_code         = 'PROTOCOLS_EBSDSIM'

obj_mc_20kv_emsoft_code         = 'DEFAULT_MC_EMSOFT_20kV'
obj_master_default_emsoft_code  = 'DEFAULT_MASTER_EMSOFT'

def fill_ebsdsim_protocols(oBis):

    ## ################################################
    ## create collections
    ## ################################################

    
    space_methods = oBis.get_space(code=general_codes.space_methods_code)

    project_protocols = oBis.get_project('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code)

    ##
    ## Collections 
    ##

    #
    # 
    #
    logging.debug('create collection for EBSD Simulation')
    collection_ebsdsim_new = oBis.new_collection(
        type='COLLECTION',
        project=project_protocols.code,
        code=collection_EBSDSim_code,
        props={
            "$name" : "EBSD Simulation",
            "$default_object_type" : create_ebsd_schema.code_obj_ebsdsim_mc
        }
    )
    try:
        collection_ebsdsim_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))
    collection_ebsdsim = oBis.get_collection('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+collection_EBSDSim_code)    


    ## ################################################
    ## fill collections
    ## ################################################
    logging.debug('Fill collections for EBSD Simulation') 

    #
    # Default 20kV Monte Carlo Simulation in EMSoft
    #
    logging.debug('Fill Default 20kV Monte Carlo Simulation in EMSoft')
    obj_mc_20kv_emsoft_new = oBis.new_object(
    type  = create_ebsd_schema.code_obj_ebsdsim_mc,
    space = space_methods.code,
    experiment=  collection_ebsdsim.identifier,
    code = obj_mc_20kv_emsoft_code,
    props={
        '$name'             : 'Default_MC_EMSoft20kV',
        'date'              : str(date.today()),
        'description'       : 'Standard settings for EBSD Monte Carlo simulation using EMSoft',
        'comments'          : '',
        'ebsdsim_mode'      : 'EMSOFT_MODE_FULL',
        'num_electrons'     : '2000000000',
        'tiltangle'         : '70',
        'tiltangle_rd'      : '0',
        'hv'                : '20',
        'min_energy'        : '10',
        'energy_bin_size'   : '1', 
        'depth_max'         : '100',
        'depth_step'        : '1',
        'foil_thickness'    : '200'
        }   
    )
    try:
        obj_mc_20kv_emsoft_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))

    obj_mc_20kv_emsoft = oBis.get_object('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code +'/'+obj_mc_20kv_emsoft_code)
    obj_mc_20kv_emsoft.set_parents(['/'+general_codes.space_equipment_code +'/'+general_codes.project_software_code    +'/'+fill_software.obj_emsoft_unknown_code])
    obj_mc_20kv_emsoft.save()



    #
    # Default Masterpattern simulation in EMSoft
    #
    logging.debug('Fill Default Masterpattern simulation in EMSoft')
    obj_master_default_emsoft_new = oBis.new_object(
    type  = create_ebsd_schema.code_obj_ebsdsim_master,
    space = space_methods.code,
    experiment=  collection_ebsdsim.identifier,
    code = obj_master_default_emsoft_code,
    props={
        '$name'             : 'Default Masterpattern EMSoft',
        'date'              : str(date.today()),
        'description'       : 'Default settings for masterpattern simulation in EMSsoft',
        'dmin'              : '0.05',
        'npx'               : '500',
        'lat_grid_type'     : 'Lambert'
        }   
    )
    try:
        obj_master_default_emsoft_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))

    obj_master_default_emsoft = oBis.get_object('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code +'/'+obj_master_default_emsoft_code)
    obj_master_default_emsoft.set_parents(['/'+general_codes.space_equipment_code +'/'+general_codes.project_software_code    +'/'+fill_software.obj_emsoft_unknown_code,
                                           '/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code +'/'+obj_mc_20kv_emsoft_code
                                          ])   
    obj_master_default_emsoft.save()


    # #
    # # 
    # #
    # logging.debug('Fill ')
    # obj_ = oBis.new_object(
    # type  = create_software_schema.code_obj_software,
    # space = space_equipment.code,
    # experiment=  collection_.identifier,
    # code = obj_e_code,
    # props={
    #     '$name'             : 'E',
    #     'date'              : str(date.today()),
    #     'description'       : '',
    #     'softwarename'      : '',
    #     'softwareversion'   : '',
    #     'commit_hash'       : ''
    #     }   
    # )
    # try:
    #     obj_.save()
    # except ValueError as e:
    #     if str(e).find('already exists') :
    #         print('Already registered: {}'.format(e))

#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for Atom Probe Tomography

"""


from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

code_ds_apt_data = "APT_DATA"


def create_apt_schema(oBis: Openbis):
    """Creates Atom Probe Tomography Dataset Type.

        We create "empty" types so that we can upload data to the right place.
        Once parsers are developped, we update the properties.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """
    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    terms_pulse_vocab = [
        {
            "code": "VOLTAGE_PULSE",
            "label": "Voltage Pulse",
            "description": "Voltage Pulse",
        },
        {"code": "LASER_PULSE", "label": "Laser Pulse", "description": "Laser Pulse"},
    ]
    voc_pulse_type = oBis.new_vocabulary(
        code="APT_PULSE_TYPE_VOCAB",
        description="Types of pulsing used in APT experiment",
        terms=terms_pulse_vocab,
    )
    register_controlled_vocabulary(oBis, voc_pulse_type, terms_pulse_vocab)

    # #########################################################################
    # Property Types
    # #########################################################################

    pt_pulse_energy = oBis.new_property_type(
        code="TEMP_LASER_PULSE_ENERGY",
        label="Pulse Energy (pJ)",
        description="Laser Pulse Energy in pJ",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pulse_energy)  # TEMPORARY

    pt_pulse_frequency = oBis.new_property_type(
        code="TEMP_PULSE_FREQUENCY",
        label="Pulse Frequency (kHz)",
        description="Pulse Frequency in kHz",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pulse_frequency)  # TEMPORARY

    pt_acquired_ions = oBis.new_property_type(
        code="TEMP_NUMBER_OF_IONS",
        label="Aquired Ions",
        description="Number of acquired ions",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_acquired_ions)  # TEMPORARY

    pt_end_voltage = oBis.new_property_type(
        code="TEMP_END_VOLTAGE",
        label="End Voltage (kV)",
        description="End Voltage in kV",
        dataType="REAL",
    )
    register_property_type(oBis, pt_end_voltage)  # TEMPORARY

    pt_specimen_condition = oBis.new_property_type(
        code="TEMP_SPECIMEN_CONDITION",
        label="Specimen Condition",
        description="Specimen Condition",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_specimen_condition)  # TEMPORARY

    pt_specimen_base_temp = oBis.new_property_type(
        code="TEMP_SPECIMEN_BASE_TEMPERATURE",
        label="Base Temperature (K)",
        description="Specimen base temperature in K",
        dataType="REAL",
    )
    register_property_type(oBis, pt_specimen_base_temp)  # TEMPORARY

    pt_detection_rate = oBis.new_property_type(
        code="TEMP_DETECTION_RATE",
        label="Detection Rate (%)",
        description="Detection Rate [%]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_detection_rate)  # TEMPORARY

    pt_pulse_fraction = oBis.new_property_type(
        code="TEMP_PULSE_FRACTION",
        label="Pulse Fraction (%)",
        description="Pulse Fraction [%]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pulse_fraction)  # TEMPORARY

    pt_total_time = oBis.new_property_type(
        code="TEMP_RUN_TIME",
        label="Run Time (min)",
        description="Run Time [min]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_total_time)  # TEMPORARY

    pt_pulse_type = oBis.new_property_type(
        code="TEMP_APT_PULSE_TYPE",
        label="Pulse Type",
        description="Type of pulsing used in APT experiment",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_pulse_type.code,
    )
    register_property_type(oBis, pt_pulse_type)  # TEMPORARY

    # pt_CHANGEME = oBis.new_property_type(
    #     code = '',
    #     label = '',
    #     description = '',
    #     dataType = 'VARCHAR',
    # )
    # register_property_type(oBis, pt_CHANGEME)

    # #########################################################################
    # Objects
    # #########################################################################

    # code_obj_CHNAGEME = ''
    # obj_CHANGEME = oBis.new_object_type(
    #     code = code_obj_CHNAGEME,
    #     generatedCodePrefix = '',
    #     autoGeneratedCode = True,
    # )
    # register_object_type(oBis, obj_CHANGEME)
    # obj_CHANGEME = oBis.get_object_type(code_obj_CHNAGEME)
    # obj_CHANGEME.description = ''
    # register_object_type(oBis, obj_CHANGEME)

    # obj_CHANGEME = oBis.get_object_type(code_obj_CHNAGEME)

    # # Define display order
    # sec2props = {
    #     'General': [
    #         ('$NAME', 1, None), ('$SHOW_IN_PROJECT_OVERVIEW', 0, None),
    #         ('FINISHED_FLAG', 0, None), ('START_DATE', 0, None), ('END_DATE', 0, None),
    #         ('COMMENTS', 0, None),
    #     ],
    #     'Section Name': [
    #         ('PROPERTY_CODE', is_mandatory, plugin),
    #     ],
    # }
    # assign_property_types(obj_CHANGEME, sec2props)

    # #########################################################################
    # Dataset Types
    # #########################################################################

    ds_apt_data = oBis.new_dataset_type(
        code=code_ds_apt_data,
        description="Data from Atom Probe Tomography (APT)",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_apt_data)

    ds_apt_data = oBis.get_dataset_type(code_ds_apt_data)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Manual Entry": [
            ("TEMP_APT_PULSE_TYPE", 0, None),
            ("TEMP_PULSE_FREQUENCY", 0, None),
            ("TEMP_LASER_PULSE_ENERGY", 0, None),
            ("TEMP_NUMBER_OF_IONS", 0, None),
            ("TEMP_DETECTION_RATE", 0, None),
            ("TEMP_PULSE_FRACTION", 0, None),
            ("TEMP_END_VOLTAGE", 0, None),
            ("TEMP_SPECIMEN_CONDITION", 0, None),
            ("TEMP_SPECIMEN_BASE_TEMPERATURE", 0, None),
            ("TEMP_RUN_TIME", 0, None),
        ],
        # 'Section Name': [
        #     ('PROPERTY_CODE', is_mandatory, None),
        # ],
    }
    assign_property_types(ds_apt_data, sec2props)

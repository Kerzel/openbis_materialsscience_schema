#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Fill general parts, e.g. spaces, collections, etc.
"""


from pybis import Openbis
from . import general_codes
from schema_helpers import register_project


def fill_general(oBis: Openbis):

    ## Spaces (manually created in Inventory)

    # Inventory spaces cannot (yet?) be created in pyBIS, they are an ELN-LIMS only option.
    # We need to create them in the ELN-LIMS and then access them here.

    spaces = [
        general_codes.space_materials_code,
        general_codes.space_equipment_code,
        general_codes.space_methods_code,
        general_codes.space_samples_code,
    ]

    manual_setup_done = True
    for space_code in spaces:
        try:
            oBis.get_space(space_code)
        except:
            manual_setup_done = False
            break
    assert (
        manual_setup_done
    ), f"Please create space {space_code} in Inventory using ELN-LIMS GUI. The following spaces are expected: {spaces}"

    space_materials = oBis.get_space(code=general_codes.space_materials_code)
    space_equipment = oBis.get_space(code=general_codes.space_equipment_code)
    space_methods = oBis.get_space(code=general_codes.space_methods_code)
    space_samples = oBis.get_space(code=general_codes.space_samples_code)

    ## Projects

    # Consumables

    project_consumables = space_materials.new_project(
        code=general_codes.project_consumables_code,
        description="Folder for Consumables",
    )
    register_project(oBis, project_consumables)

    # Material Types

    project_material_types = space_materials.new_project(
        code=general_codes.project_consumables_code,
        description="Folder for high-level Material Types",
    )
    register_project(oBis, project_material_types)

    # Devices

    project_devices = space_equipment.new_project(
        code=general_codes.project_devices_code, description="Folder for Devices"
    )
    register_project(oBis, project_devices)

    # Protocols

    project_protocols = space_methods.new_project(
        code=general_codes.project_protocols_code, description="Folder for Protocols"
    )
    register_project(oBis, project_protocols)

    # Software

    project_software = space_equipment.new_project(
        code=general_codes.project_software_code, description="Folder for Software"
    )
    register_project(oBis, project_software)

    # Samples - Demo

    project_samples_demo = space_samples.new_project(
        code=general_codes.project_samples_demo_code,
        description="Folder for Samples used in demonstrations",
    )
    register_project(oBis, project_samples_demo)

    # # Samples - Experimental
    # project_samples_exp = space_samples.new_project(
    #     code = general_codes.project_samples_exp_code,
    #     description="Folder for experimental samples"
    # )
    # register_project(oBis, project_samples_exp)

    # # Samples - Simulation
    # project_samples_sim = space_samples.new_project(
    #     code = general_codes.project_samples_sim_code,
    #     description="Folder for simulation samples"
    # )
    # register_project(oBis, project_samples_sim)

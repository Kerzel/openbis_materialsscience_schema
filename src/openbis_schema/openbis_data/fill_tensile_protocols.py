#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pybis import Openbis
import os
import sys
from datetime import date

import logging

"""
Copyright 2023 Ulrich Kerzel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with typical protocols for tensile testing

"""
from .. import create_tensile_schema
from . import general_codes
from . import fill_equipment

collection_tensiletest_code = 'TENSILE_TEST_COLLECTION'
obj_dzm170c_default_code    = 'DZM_170C_default'

def fill_tensile_protocols(oBis : Openbis):
    ## ################################################
    ## create collections
    ## ################################################

    ##
    ## Space where the collection lives in
    ##
    
    # existing default space
    logging.debug('Space Methods')
    space_methods = oBis.get_space(code=general_codes.space_methods_code)

    ##
    ##  project
    ##
    project_protocols = oBis.get_project('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code)


    ##
    ## Collections 
    ##

    #
    # 
    #
    logging.debug('create collection for tensile testing protocols')
    collection_tensiletest_new = oBis.new_collection(
        type='COLLECTION',
        project=project_protocols.code,
        code=collection_tensiletest_code,
        props={
            "$name" : "Tensile Testing Protocols",
            "$default_object_type" : create_tensile_schema.code_obj_tensile_protocol
        }
    )
    try:
        collection_tensiletest_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))
    collection_tensiletest = oBis.get_collection('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+collection_tensiletest_code) 


    ## ################################################
    ## fill collections
    ## ################################################    
    logging.debug('Fill collections for Tensile Testing Protocols ')

    obj_dzm170c_default_new = oBis.new_object(
    type        = create_tensile_schema.code_obj_tensile_protocol,
    space       = general_codes.space_methods_code,
    experiment  = collection_tensiletest.identifier,
    code        = obj_dzm170c_default_code,
    props={
        '$name'                 :  obj_dzm170c_default_code,
        'date'                  :  str(date.today()),
        'description'           : 'default protocol for tensile testing at 170 °C',
        'tensile_loading_rate'  : '0.0005',
        'tensile_p'             : '400',
        'tensile_i'             : '0.07',
        'tensile_d'             : '0',
        'sampling_rate'         : '5',
        'deformation_mode'      : 'tensile'

        }   
    )
    try:
        obj_dzm170c_default_new.save()
    except ValueError as e:
        if str(e).find('already exists') :
            print('Already registered: {}'.format(e))


    # add parents: DZM
    obj_dzm170c_default = oBis.get_object('/'+general_codes.space_methods_code+'/'+general_codes.project_protocols_code+'/'+obj_dzm170c_default_code)
    obj_dzm170c_default.set_parents(['/'+general_codes.space_equipment_code +'/'+general_codes.project_devices_code      +'/'+fill_equipment.obj_dzm_code])
    obj_dzm170c_default.save()
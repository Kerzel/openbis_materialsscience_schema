def get_elements(e):
    """Returns a list of comma-seperated elements contained in a sample.
    Sorts by atomic percentage, and alphabetically in case of ex aequo.
    """
    properties = e.properties()
    if properties is None:
        return "NoElements"
    else:
        element_dict_with_percentages = {}
        for p in properties:
            property_code = p.propertyTypeCode()
            value = p.renderedValue()
            if (
                property_code.startswith("ELEMENT")
                and property_code.count("_") == 1
                and value != "empty"
            ):
                element_dict_with_percentages[property_code] = [value, 0]
        for code, (element_symbol, _) in element_dict_with_percentages.items():
            for p in properties:
                property_code = p.propertyTypeCode()
                value = p.renderedValue()
                if property_code == (code) + "_AT_PERCENT" and value != "empty":
                    element_dict_with_percentages[code][1] = value
        elements_unsorted = element_dict_with_percentages.values()

        # elements_sorted = sorted(elements_unsorted , key=lambda x: (-x[1], x[0]))

        elements_sorted = []
        for atom_symbol, atomic_percentage in elements_unsorted:
            intermediate = (-float(atomic_percentage), atom_symbol)
            elements_sorted.append(intermediate)
        elements_sorted.sort()
        result = ",".join(x[1] for x in elements_sorted)
        return result


def calculate():
    return get_elements(entity)

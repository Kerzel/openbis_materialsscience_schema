#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for 

"""


from pybis import Openbis
from schema_helpers import *


from .general_properties import MAX_NUM_DETECTORS


# openBIS codes for the objects (also used in other modules)

code_ds_tem_data = "TEM_DATA"
code_ds_stem_eds_data = "STEM-EDS_DATA"


def create_tem_schema(oBis: Openbis):
    """Creates (S)TEM Dataset Types.
    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """
    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    terms_imaging_mode = [
        {"code": "IMAGING", "label": "Imaging", "description": "Imaging"},
        {"code": "DIFFRACTION", "label": "Diffraction", "description": "Diffraction"},
    ]
    voc_imaging_mode = oBis.new_vocabulary(
        code="TEM_IMAGING_MODE_VOCAB",
        description="TEM Imaging Mode",
        terms=terms_imaging_mode,
    )
    register_controlled_vocabulary(oBis, voc_imaging_mode, terms_imaging_mode)

    # #########################################################################
    # Property Types
    # #########################################################################

    pt_imaging_mode = oBis.new_property_type(
        code="TEM_IMAGING_MODE",
        label="Imaging Mode",
        description="TEM Imaging Mode",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_imaging_mode.code,
    )
    register_property_type(oBis, pt_imaging_mode)

    pt_pixel_size_x = oBis.new_property_type(
        code="PIXEL_SIZE_X",
        label="Pixel Size X",
        description="Pixel Size X",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pixel_size_x)

    pt_pixel_unit_x = oBis.new_property_type(
        code="PIXEL_UNIT_X",
        label="Pixel Unit X",
        description="Pixel Unit X",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_pixel_unit_x)

    pt_pixel_size_y = oBis.new_property_type(
        code="PIXEL_SIZE_Y",
        label="Pixel Size Y",
        description="Pixel Size Y",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pixel_size_y)

    pt_pixel_unit_y = oBis.new_property_type(
        code="PIXEL_UNIT_Y",
        label="Pixel Unit Y",
        description="Pixel Unit Y",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_pixel_unit_y)

    pt_magnification_actual = oBis.new_property_type(
        code="MAGNIFICATION_ACTUAL",
        label="Actual Magnification",
        description="Actual Magnification",
        dataType="REAL",
    )
    register_property_type(oBis, pt_magnification_actual)

    pt_camera_length = oBis.new_property_type(
        code="CAMERA_LENGTH_MM",
        label="Camera Length (mm)",
        description="Camera Length in milimeters",
        dataType="REAL",
    )
    register_property_type(oBis, pt_camera_length)

    pt_exposure_time = oBis.new_property_type(
        code="EXPOSURE_TIME",
        label="Exposure Time (s)",
        description="Exposure time in seconds",
        dataType="REAL",
    )
    register_property_type(oBis, pt_exposure_time)

    pt_beam_convergence = oBis.new_property_type(
        code="BEAM_CONVERGENCE_MRAD",
        label="Beam Convergence (mrad)",
        description="Bean convergence angle (mrad)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_beam_convergence)

    pt_stage_x = oBis.new_property_type(
        code="STAGE_X_UM",
        label="Stage X (µm)",
        description="Stage displacement in X direction in µm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_x)

    pt_stage_y = oBis.new_property_type(
        code="STAGE_Y_UM",
        label="Stage Y (µm)",
        description="Stage displacement in Y direction in µm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_y)

    pt_stage_z = oBis.new_property_type(
        code="STAGE_Z_UM",
        label="Stage Z (µm)",
        description="Stage displacement in Z direction in µm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_stage_z)

    pt_stage_holder = oBis.new_property_type(
        code="STAGE_HOLDER_TYPE",
        label="Stage Holder",
        description="Type of stage holder",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_stage_holder)

    pt_specimen_name = oBis.new_property_type(
        code="SPECIMEN_NAME",
        label="Specimen",
        description="Specimen",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_specimen_name)

    for i in range(1, 1 + MAX_NUM_DETECTORS):
        pt_detector_temperature = oBis.new_property_type(
            code=f"DETECTOR{i}_TEMP",
            label=f"Detector{i} Temperature (°C)",
            description=f"Detector{i} Temperature (°C)",
            dataType="REAL",
        )
        register_property_type(oBis, pt_detector_temperature)
    # #########################################################################
    # Objects
    # #########################################################################

    # code_obj_CHNAGEME = ''
    # obj_CHANGEME = oBis.new_object_type(
    #     code = code_obj_CHNAGEME,
    #     generatedCodePrefix = '',
    #     autoGeneratedCode = True,
    # )
    # register_object_type(oBis, obj_CHANGEME)
    # obj_CHANGEME = oBis.get_object_type(code_obj_CHNAGEME)
    # obj_CHANGEME.description = ''
    # register_object_type(oBis, obj_CHANGEME)

    # obj_CHANGEME = oBis.get_object_type(code_obj_CHNAGEME)

    # # Define display order
    # sec2props = {
    #     'General': [
    #         ('$NAME', 1, None), ('$SHOW_IN_PROJECT_OVERVIEW', 0, None),
    #         ('FINISHED_FLAG', 0, None), ('START_DATE', 0, None), ('END_DATE', 0, None),
    #         ('COMMENTS', 0, None),
    #     ],
    #     'Section Name': [
    #         ('PROPERTY_CODE', is_mandatory, plugin),
    #     ],
    # }
    # assign_property_types(obj_CHANGEME, sec2props)

    # #########################################################################
    # Dataset Types
    # #########################################################################

    ds_tem_data = oBis.new_dataset_type(
        code=code_ds_tem_data,
        description="Data from (Scanning) Transmission Electron Microscopy | (S)TEM",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_tem_data)

    ds_tem_data = oBis.get_dataset_type(code_ds_tem_data)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("COMMENTS", 0, None),
            ("DATE", 0, None),
            ("TIME", 0, None),
            ("DATETIME", 0, None),
            ("SPECIMEN_NAME", 0, None),
            ("SOFTWAREVERSION", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Image": [
            ("PIXEL_SIZE_X", 0, None),
            ("PIXEL_UNIT_X", 0, None),
            ("PIXEL_SIZE_Y", 0, None),
            ("PIXEL_UNIT_Y", 0, None),
            ("MAGNIFICATION_REAL", 0, None),
            ("MAGNIFICATION_ACTUAL", 0, None),
            # ('HFW', 0, None), ('VFW', 0, None),
            # ('CONTRAST', 0, None), ('BRIGHTNESS', 0, None), ('LINEAR_LUT', 0, None),
            ("TEM_IMAGING_MODE", 0, None),
            ("IS_STEM", 0, None),
        ],
        "Data": [
            ("DATA_SIZE_X", 0, None),
            ("DATA_SIZE_Y", 0, None),
            ("BIT_DEPTH", 0, None),
            ("DATA_TYPE", 0, None),
            ("NUMBER_OF_CHANNELS", 0, None),
            ("NUMBER_OF_FRAMES", 0, None),
            ("NUMBER_OF_DATASETS", 0, None),
        ],
        "Settings": [
            ("CAMERA_LENGTH_MM", 0, None),
            ("EXPOSURE_TIME", 0, None),
            ("DWELL_TIME", 0, None),
            ("LINE_TIME", 0, None),
            ("FRAME_TIME", 0, None),
            ("EXTRACTION_VOLTAGE", 0, None),
            ("EMISSION_CURRENT_UA", 0, None),
            ("SPOT_SIZE_SETTING", 0, None),
        ],
        "Electron Beam": [
            ("ACCELERATING_VOLTAGE", 0, None),
            ("SPOT_SIZE", 0, None),
            ("BEAM_CURRENT_NA", 0, None),
            ("BEAM_CONVERGENCE_MRAD", 0, None),
        ],
        "Electron Gun": [("GUN_TYPE", 0, None)],
        "Detector 1": [
            ("DETECTOR1_NAME", 0, None),
            ("DETECTOR1_TYPE", 0, None),
            ("DETECTOR1_GAIN", 0, None),
            ("DETECTOR1_OFFSET", 0, None),
            ("DETECTOR1_TEMP", 0, None),
        ],
        "Detector 2": [
            ("DETECTOR2_NAME", 0, None),
            ("DETECTOR2_TYPE", 0, None),
            ("DETECTOR2_GAIN", 0, None),
            ("DETECTOR2_OFFSET", 0, None),
            ("DETECTOR2_TEMP", 0, None),
        ],
        "Detector 3": [
            ("DETECTOR3_NAME", 0, None),
            ("DETECTOR3_TYPE", 0, None),
            ("DETECTOR3_GAIN", 0, None),
            ("DETECTOR3_OFFSET", 0, None),
            ("DETECTOR3_TEMP", 0, None),
        ],
        "Detector 4": [
            ("DETECTOR4_NAME", 0, None),
            ("DETECTOR4_TYPE", 0, None),
            ("DETECTOR4_GAIN", 0, None),
            ("DETECTOR4_OFFSET", 0, None),
            ("DETECTOR4_TEMP", 0, None),
        ],
        "Detector 5": [
            ("DETECTOR5_NAME", 0, None),
            ("DETECTOR5_TYPE", 0, None),
            ("DETECTOR5_GAIN", 0, None),
            ("DETECTOR5_OFFSET", 0, None),
            ("DETECTOR5_TEMP", 0, None),
        ],
        "Detector 6": [
            ("DETECTOR6_NAME", 0, None),
            ("DETECTOR6_TYPE", 0, None),
            ("DETECTOR6_GAIN", 0, None),
            ("DETECTOR6_OFFSET", 0, None),
            ("DETECTOR6_TEMP", 0, None),
        ],
        "Stage": [
            ("STAGE_X_UM", 0, None),
            ("STAGE_Y_UM", 0, None),
            ("STAGE_Z_UM", 0, None),
            ("STAGE_TILT_A", 0, None),
            ("STAGE_TILT_B", 0, None),
            ("STAGE_HOLDER_TYPE", 0, None),
        ],
        "Device": [
            ("DEVICE_MANUFACTURER", 0, None),
            ("DEVICE_NAME", 0, None),
        ],
        # 'Section Name': [
        #     ('PROPERTY_CODE', is_mandatory, plugin),
        # ],
    }
    assign_property_types(ds_tem_data, sec2props)

    ds_stem_eds_data = oBis.new_dataset_type(
        code=code_ds_stem_eds_data,
        description="Data from STEM with EDS",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_stem_eds_data)

    ds_stem_eds_data = oBis.get_dataset_type(code_ds_stem_eds_data)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "STEM details": [
            ("DEVICE_NAME", 0, None),
            ("USER_NAME", 0, None),
            ("GUN_TYPE", 0, None),
            ("ACCELERATING_VOLTAGE", 0, None),
            ("EXTRACTION_VOLTAGE", 0, None),
            ("EMISSION_CURRENT_UA", 0, None),
            ("MAGNIFICATION_REAL", 0, None),
            ("CAMERA_LENGTH_MM", 0, None),
            ("SPOT_SIZE_SETTING", 0, None),
            ("STAGE_X_UM", 0, None),
            ("STAGE_Y_UM", 0, None),
            ("STAGE_Z_UM", 0, None),
            ("STAGE_TILT_A", 0, None),
            ("STAGE_TILT_B", 0, None),
        ],
    }

    assign_property_types(ds_stem_eds_data, sec2props)

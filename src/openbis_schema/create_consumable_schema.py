#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for consumables

    Consumables such as chemicals, etc.
"""


from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

code_obj_chemical = "CHEMICAL"
code_obj_prep_material = "PREPARATION_MATERIAL"


def create_consumable_schema(oBis: Openbis):
    """Creates Consumable Object Types.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    # Chemical Subtypes

    terms_chemicals = [
        {"code": "LUBRICANT", "label": "Lubricant", "description": "Lubricant"},
        {"code": "POLISH", "label": "Polishing", "description": "Polishing Solution"},
        {"code": "ETCHING", "label": "Etching", "description": "Etching Solution"},
        {"code": "GENERIC", "label": "Generic", "description": "Generic"},
        {"code": "WASTE", "label": "Waste", "description": "Waste"},
        {"code": "ELECTROLYTE", "label": "Electrolyte", "description": "Electrolyte"},
        {"code": "SUPPLY", "label": "Supply", "description": "Supply"},
        {"code": "COMPONENT", "label": "Component", "description": "Component"},
        {
            "code": "RAW_MATERIAL",
            "label": "Raw Material",
            "description": "Raw Material",
        },
    ]
    voc_chemical_type = oBis.new_vocabulary(
        code="CHEMICAL_TYPE.TYPES",
        description="Types of chemicals used",
        terms=terms_chemicals,
    )
    register_controlled_vocabulary(oBis, voc_chemical_type, terms_chemicals)

    # Preperation material subtypes (Metallography)

    terms_metallo = [
        {
            "code": "ABRASIVE",
            "label": "Abrasives",
            "description": "Mechanical abrasive material",
        },
        {
            "code": "POLISH",
            "label": "Polishing Material",
            "description": "Polishing Material, e.g. polishing cloth, etc.",
        },
        {"code": "SUPPLY", "label": "Supply", "description": "Supply"},
    ]
    voc_prep_material_type = oBis.new_vocabulary(
        code="PREPARATION_MATERIAL.TYPES",
        description="Types of preparation material used in metallography",
        terms=terms_metallo,
    )
    register_controlled_vocabulary(oBis, voc_prep_material_type, terms_metallo)

    # #########################################################################
    # Property Types
    # #########################################################################

    # Chemical Type

    pt_chemical_type = oBis.new_property_type(
        code="CHEMICAL_TYPE",
        label="Type of chemical",
        description="Type or use of chemical",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_chemical_type.code,
    )
    register_property_type(oBis, pt_chemical_type)

    # Preparation Material Type

    pt_prep_material_type = oBis.new_property_type(
        code="PREPARATIONMATERIAL_TYPE",
        label="Type of preparation material",
        description="Type of preparation material",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_prep_material_type.code,
    )
    register_property_type(oBis, pt_prep_material_type)

    # Article Number

    pt_article_number = oBis.new_property_type(
        code="ARTICLE_NUMBER",
        label="Article Number",
        description="Article Number",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_article_number)

    # CAS number - Chemical Identifier

    pt_cas_number = oBis.new_property_type(
        code="CAS_NUMBER",
        label="CAS Number",
        description="Categorisation according to CAS",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_cas_number)

    # REACH number
    # https://echa.europa.eu/regulations/reach/understanding-reach

    pt_reach_number = oBis.new_property_type(
        code="REACH_NUMBER",
        label="REACH Number",
        description="Categorisation according to REACH (European Chemicals Agency)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_reach_number)

    # EC number
    # https://echa.europa.eu/information-on-chemicals/ec-inventory

    pt_ec_number = oBis.new_property_type(
        code="EC_NUMBER",
        label="EC Number",
        description="EC number (EC Inventory / European Chemicals Agency)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_ec_number)

    # ID of the security information sheet

    pt_security_id = oBis.new_property_type(
        code="ID_SECURITY_SHEET",
        label="ID Security Information Sheet",
        description="Reference ID of the security information sheet with further details on handling the chemical",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_security_id)

    # ID of the usage information sheet

    pt_usage_id = oBis.new_property_type(
        code="ID_USAGE_SHEET",
        label="ID Usage Information Sheet",
        description="Reference ID of the usage information sheet with further details on handling the chemical",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_usage_id)

    # List of labs using the consumable
    # Helps user find the consumables contained in their lab

    pt_used_in = oBis.new_property_type(
        code="USED_IN",
        label="Used In",
        description="List of labs (comma-seperated)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_used_in)

    # Concentration

    pt_concentration = oBis.new_property_type(
        code="CONCENTRATION_PERCENT",
        label="Concentration (%)",
        description="Concentration of chemical in %",
        dataType="REAL",
    )
    register_property_type(oBis, pt_concentration)

    # Grit / Grain Size

    pt_grit = oBis.new_property_type(
        code="GRIT",
        label="Grit / GrainSize (μm)",
        description="Grain size in μm (approximate)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_grit)

    # #########################################################################
    # Objects
    # #########################################################################

    # Chemical

    obj_chemical = oBis.new_object_type(
        code=code_obj_chemical,
        generatedCodePrefix="CHEMICAL",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_chemical)
    obj_chemical = oBis.get_object_type(code_obj_chemical)
    obj_chemical.description = "Chemical"
    register_object_type(oBis, obj_chemical)

    obj_chemical = oBis.get_object_type(code_obj_chemical)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("ALT_NAME", 0, None),
            ("ID_INTERNAL", 0, None),
            ("$BARCODE", 0, None),
            ("DATE", 0, None),
            ("CHEMICAL_TYPE", 0, None),
            ("COMMENTS", 0, None),
            ("USED_IN", 0, None),
        ],
        "Details": [
            ("REACH_NUMBER", 0, None),
            ("EC_NUMBER", 0, None),
            ("ID_SECURITY_SHEET", 0, None),
            ("ID_USAGE_SHEET", 0, None),
            ("CONCENTRATION_PERCENT", 0, None),
        ],
        "Supplier": [
            ("COMPANY", 0, None),
            ("ARTICLE_NUMBER", 0, None),
            ("CAS_NUMBER", 0, None),
        ],
    }
    assign_property_types(obj_chemical, sec2props)

    # Preparation Material

    obj_prep_material = oBis.new_object_type(
        code=code_obj_prep_material,
        generatedCodePrefix="PREPMATERIAL",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_prep_material)
    obj_prep_material = oBis.get_object_type(code_obj_prep_material)
    obj_prep_material.description = "Material used in metallographic preparation"
    register_object_type(oBis, obj_prep_material)

    obj_prep_material = oBis.get_object_type(code_obj_prep_material)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("ALT_NAME", 0, None),
            ("$BARCODE", 0, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("PREPARATIONMATERIAL_TYPE", 0, None),
            ("GRIT", 0, None),
            ("ID_INTERNAL", 0, None),
            ("USED_IN", 0, None),
        ],
        "Supplier": [
            ("COMPANY", 0, None),
            ("ARTICLE_NUMBER", 0, None),
        ],
    }
    assign_property_types(obj_prep_material, sec2props)

#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for EBSD simulation and experiments

"""


from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

# Protocols for EBSD Simulation

code_obj_ebsd_sim_mc = "EBSDSIM_MONTECARLO_PROTOCOL"
code_obj_ebsd_sim_master = "EBSDSIM_MASTER_PROTOCOL"
code_obj_ebsdsim_screen = "EBSDSIM_SCREENPATTERN_PROTOCOL"

# Datasets for EBSD Simulation

code_ds_ebsd_sim_internal = "EBSD_SIM_INTERNAL"
code_ds_ebsd_sim_master = "EBSD_SIM_MASTERPATTERN"
code_ds_ebsdsim_screen = "EBSD_SIM_SCREENPATTERN"

# Datasets for experimental EBSD measurements

code_ds_ebsd_exp = "EBSD_EXP_DATA"  # Only EBSD maps
code_ds_ebsd_eds_data = "EBSD-EDS_DATA"  # EBSD + EDS


# # We use the SEM Experiment to document an EBSD measurement
# # and upload the data with the corresponding data type


def create_ebsd_schema(oBis: Openbis):
    """Creates EBSD Object Types and Data Types.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    # EBSD Simulation mode (for EMsoft)

    terms = [
        {
            "code": "EMSOFT_MODE_BSE1",
            "label": "ECP",
            "description": "Electron Channeling Pattern",
        },
        {"code": "EMSOFT_MODE_FULL", "label": "EBSD", "description": "EBSD"},
        {
            "code": "EMSOFT_MODE_IVOL",
            "label": "IVOL",
            "description": "Interaction Volume output",
        },
    ]
    voc_ebsd_sim_mode = oBis.new_vocabulary(
        code="EBSDSIM_MODE",
        description="Mode of the EBSD Simulation (EMsoft)",
        terms=terms,
    )
    register_controlled_vocabulary(oBis, voc_ebsd_sim_mode, terms)

    # EBSD lateral grid type

    terms = [
        {"code": "LAMBERT", "label": "Lambert", "description": "Lambert grid"},
        {
            "code": "LEGENDRE",
            "label": "Legendre",
            "description": "Legendre grid for Spherical Indexing runs",
        },
    ]
    voc_ebsd_lat_grid_type = oBis.new_vocabulary(
        code="LAT_GRID_TYPE",
        description="Latitudinal grid type",
        terms=terms,
    )
    register_controlled_vocabulary(oBis, voc_ebsd_lat_grid_type, terms)

    # Intensity scaling mode

    terms = [
        {"code": "NOSCALING", "label": "No scaling", "description": "No scaling"},
        {"code": "LINEAR", "label": "Linear", "description": "Linear"},
        {
            "code": "GAMMA",
            "label": "Gamma correction",
            "description": "Gamma correction",
        },
    ]
    voc_ebsd_intensity_scaling_mode = oBis.new_vocabulary(
        code="EBSD_INTENSITY_SCALING_MODE",
        description="Intensity Scaling Mode",
        terms=terms,
    )
    register_controlled_vocabulary(oBis, voc_ebsd_intensity_scaling_mode, terms)

    # #########################################################################
    # Property Types
    # #########################################################################

    # First Euler Angle

    pt_euler_angle_1 = oBis.new_property_type(
        code="EULER_ANGLE_1",
        label="Euler Angle 1",
        description="Euler Angle (1/3)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_euler_angle_1)

    # Second Euler Angle

    pt_euler_angle_2 = oBis.new_property_type(
        code="EULER_ANGLE_2",
        label="Euler Angle 2",
        description="Euler Angle (2/3)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_euler_angle_2)

    # Third Euler Angle

    pt_euler_angle_3 = oBis.new_property_type(
        code="EULER_ANGLE_3",
        label="Euler Angle 3",
        description="Euler Angle (3/3)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_euler_angle_3)

    # Filename for Euler Angle definition

    pt_Eulerangle_Filename = oBis.new_property_type(
        code="EULERANGLE_FILENAME",
        label="Filename for Euler Angle definition",
        description="Filename for Euler Angle definition",
        dataType="REAL",
    )
    register_property_type(oBis, pt_Eulerangle_Filename)

    # Type of Euler angle

    pt_euler_angle_type = oBis.new_property_type(
        code="EULERANGLE_TYPE",
        label="Type of Euler angle",
        description="Type of Euler angle",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_euler_angle_type)

    # Minimum d-spacing

    pt_d_spacing_min = oBis.new_property_type(
        code="DMIN",
        label="d_min (nm)",
        description="Smallest d-spacing to take into account",
        dataType="REAL",
    )
    register_property_type(oBis, pt_d_spacing_min)

    # NPX

    pt_num_pixels_x = oBis.new_property_type(
        code="NPX",
        label="NPixelsX MP",
        description="Number of pixels along x-direction of the square master pattern (2*npx+1 = total number)",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_num_pixels_x)

    # Latitudinal grid type

    pt_ebsd_lat_grid_type = oBis.new_property_type(
        code="LAT_GRID_TYPE",
        label="Latitudinal Grid Type",
        description="Latitudinal grid type",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_ebsd_lat_grid_type.code,
    )
    register_property_type(oBis, pt_ebsd_lat_grid_type)

    # EBSD Sim mode

    pt_ebsd_sim_mode = oBis.new_property_type(
        code="EBSDSIM_MODE",
        label="Mode",
        description="Mode of the EBSD Simulation",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_ebsd_sim_mode.code,
    )
    register_property_type(oBis, pt_ebsd_sim_mode)

    # Number of electrons

    pt_num_electrons = oBis.new_property_type(
        code="NUM_ELECTRONS",
        label="# Electrons",
        description="Number of incident electrons",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_num_electrons)

    # Incident Beam Energy

    pt_incident_beam_enregy = oBis.new_property_type(
        code="HV",
        label="Incident Beam Energy (keV)",
        description="Incident Beam Energy [keV]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_incident_beam_enregy)

    pt_energy_min = oBis.new_property_type(
        code="MIN_ENERGY",
        label="Min. Energy (keV)",
        description="minimum energy [keV]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_energy_min)

    pt_energy_bin_size = oBis.new_property_type(
        code="ENERGY_BIN_SIZE",
        label="Energy Bin Size (keV)",
        description="Energy Bin Size [keV]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_energy_bin_size)

    pt_depth_max = oBis.new_property_type(
        code="DEPTH_MAX",
        label="Max. Depth (nm)",
        description="Maximum depth (nm) to consider for exit depth statistics",
        dataType="REAL",
    )
    register_property_type(oBis, pt_depth_max)

    pt_depth_step = oBis.new_property_type(
        code="DEPTH_STEP",
        label="Depth Step Size (nm)",
        description="depth step size [nm]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_depth_step)

    pt_foil_thickness = oBis.new_property_type(
        code="FOIL_THICKNESS",
        label="Foil Thickness (nm)",
        description="Total foil thickness (nm, must be larger than depth)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_foil_thickness)

    # Bethe Parameters

    pt_bethe_params = oBis.new_property_type(
        code="BETHE_PARAMETERS",
        label="Bethe Parameters",
        description="Bethe parameters (comma-seperated)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_bethe_params)

    # Bethe Parameters Filename

    pt_bethe_params_file = oBis.new_property_type(
        code="BETHE_PARAM_FILENAME",
        label="Filename for Bethe Parameters",
        description="Filename for Bethe Parameters",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_bethe_params_file)

    # EMsoft Screenpattern

    # Distance scintillator - illumination

    pt_ebsd_l = oBis.new_property_type(
        code="EBSD_L",
        label="EBSD_L (µm)",
        description="Distance between scintillator and illumination point in microns",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ebsd_l)

    # Tilt angle camera

    pt_tilt_camera = oBis.new_property_type(
        code="THETA_CAMERA",
        label="Tilt angle camera (deg)",
        description="Tilt angle of the camera (positive below horizontal in degrees)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_tilt_camera)

    # CCD Pixel size

    pt_ccd_pixel_size = oBis.new_property_type(
        code="CCD_PIXEL_SIZE",
        label="CCD pixel size (µm)",
        description="CCD pixel size on the scintillator surface in microns (delta in EMsoft)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ccd_pixel_size)

    # Number of pixels on CCD (X)

    pt_ccd_pixel_x = oBis.new_property_type(
        code="CCD_PIXEL_X",
        label="# Pixels CCD X",
        description="Number of pixels on CCD in x-direction",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_ccd_pixel_x)

    # Number of pixels on CCD (Y)

    pt_ccd_pixel_y = oBis.new_property_type(
        code="CCD_PIXEL_Y",
        label="# Pixels CCD Y",
        description="Number of pixels on CCD in y-direction",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_ccd_pixel_y)

    # Pattern centre coordinates in units of pixels

    pt_ebsd_pattern_centre_x = oBis.new_property_type(
        code="EBSD_PATTERN_CENTRE_X",
        label="xpc",
        description="Pattern center X coordinate in pixels",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ebsd_pattern_centre_x)

    # Pattern centre coordinates in units of pixels

    pt_ebsd_pattern_centre_y = oBis.new_property_type(
        code="EBSD_PATTERN_CENTRE_Y",
        label="ypc",
        description="Pattern center Y coordinate in pixels",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ebsd_pattern_centre_y)

    # Angle between normal of sample and detector

    pt_angle_sample_normal_detector = oBis.new_property_type(
        code="ANGLE_SAMPLENORMAL_DETECTOR",
        label="Omega",
        description="Angle between normal of sample and detector",
        dataType="REAL",
    )
    register_property_type(oBis, pt_angle_sample_normal_detector)

    # transfer lens barrel distortion parameter

    pt_alpha_barrel_distortion = oBis.new_property_type(
        code="ALPHA_BARREL_DISTORTION",
        label="alphaBD",
        description="Transfer lens barrel distortion parameter",
        dataType="REAL",
    )
    register_property_type(oBis, pt_alpha_barrel_distortion)

    # EMsoft binning mode

    pt_binning_mode = oBis.new_property_type(
        code="EMSOFT_BINNING_MODE",
        label="Binning Mode",
        description="Binning Mode in EMsoft",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_binning_mode)

    # lattice deformation

    pt_lattice_deformation = oBis.new_property_type(
        code="APPLY_LATTICE_DEFORMATION",
        label="Apply lattice deformation",
        description="Use a polar decomposition of the deformation tensor Fmatrix (which results in an approximation of the pattern)",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_lattice_deformation)

    # deformation tensor

    pt_deformation_tensor = oBis.new_property_type(
        code="LATTICE_DEFORMATION_TENSOR",
        label="Ftensor",
        description="Lattice deformation tensor in column-major form",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_deformation_tensor)

    # energy range in the intensity summation [keV]

    pt_ebsd_sim_energy_min = oBis.new_property_type(
        code="EBSDIM_ENERGYRANGE_MIN",
        label="Energy Min (keV)",
        description="Energy range minimum in the intensity summation [keV]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ebsd_sim_energy_min)

    # Energy range in the intensity summation [keV]

    pt_ebsd_sim_energy_max = oBis.new_property_type(
        code="EBSDIM_ENERGYRANGE_MAX",
        label="Energy Max (keV)",
        description="Energy range maximum in the intensity summation [keV]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ebsd_sim_energy_max)

    # Intensity scaling mode

    pt_intensity_scaling_mode = oBis.new_property_type(
        code="EBSDSIM_INTENSITY_SCALING_MODE",
        label="Intensity scaling mode",
        description="Intensity scaling mode of the EBSD Simulation",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_ebsd_intensity_scaling_mode.code,
    )
    register_property_type(oBis, pt_intensity_scaling_mode)

    # Gamma correction factor

    pt_gamma_correction = oBis.new_property_type(
        code="GAMMA_CORRECTION_FACTOR",
        label="Gamma correction factor",
        description="Output scaling (EMsoft): the output intensities are scaled according to a linear scale or a gamma scale",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gamma_correction)

    # Tilt angle

    pt_tilt_angle_rd = oBis.new_property_type(
        code="TILTANGLE",
        label="Tilt angle (deg)",
        description="Tilt angle from horizontal [degrees]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_tilt_angle_rd)

    # Tilt angle around RD axis

    pt_tilt_angle_rd = oBis.new_property_type(
        code="TILTANGLE_RD",
        label="Tilt angle RD axis (deg)",
        description="sample tilt angle around RD axis [degrees]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_tilt_angle_rd)

    # masterpattern filename

    pt_masterfilename = oBis.new_property_type(
        code="MASTERPATTERN_FILENAME",
        label="Filename of the masterpattern",
        description="Filename of the masterpattern",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_masterfilename)

    # Electron multiplier

    pt_electron_multiplier = oBis.new_property_type(
        code="ELECTRON_MULTIPLIER",
        label="Electron Multiplier",
        description="Electron Multiplier",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_electron_multiplier)

    # numsx = number of pixels along x-direction of square projection [odd number!] (EMsoft)

    pt_npixelx = oBis.new_property_type(
        code="NPIXELX",
        label="NPixelsX MC",
        description="Number of pixels along x-direction of square projection (Monte-Carlo)",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_npixelx)

    # Angle Start (EMsoft BSE mode)

    pt_anglestart = oBis.new_property_type(
        code="ANGLE_START",
        label="Start Angle (deg)",
        description="Start Angle",
        dataType="REAL",
    )
    register_property_type(oBis, pt_anglestart)

    # Angle End (EMsoft BSE mode)

    pt_angle_end = oBis.new_property_type(
        code="ANGLE_END",
        label="End Angle (deg)",
        description="End Angle (deg)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_angle_end)

    # Angle Step (EMsoft BSE mode)

    pt_angle_step = oBis.new_property_type(
        code="ANGLE_STEP",
        label="Angle Step Size (deg)",
        description="Angle Step Size (deg)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_angle_step)

    # Interaction volume X

    pt_interaction_vol_x = oBis.new_property_type(
        code="INTERACTION_VOLUME_X",
        label="ivolx",
        description="Number of Voxels in the x-direction",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_interaction_vol_x)

    # Interaction volume Y

    pt_interaction_vol_y = oBis.new_property_type(
        code="INTERACTION_VOLUME_Y",
        label="ivoly",
        description="Number of Voxels in the y-direction",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_interaction_vol_y)

    # Interaction volume Z

    pt_interaction_vol_z = oBis.new_property_type(
        code="INTERACTION_VOLUME_Z",
        label="ivolz",
        description="Number of Voxels in the z-direction",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_interaction_vol_z)

    # Uniform masterpattern

    pt_uniform_master = oBis.new_property_type(
        code="UNIFORM_MASTERPATTERN",
        label="Uniform Masterpattern",
        description="Uniform Masterpattern (only for background studies)",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_uniform_master)

    # Apply noise flag

    pt_apply_noise = oBis.new_property_type(
        code="APPLY_NOISE",
        label="Apply Noise",
        description="Apply Noise",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_apply_noise)

    # Use energy weighting

    pt_energywt = oBis.new_property_type(
        code="ENERGY_WEIGHTING",
        label="Use Energy Weighting",
        description="Use Energy Weighting",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_energywt)

    # Apply mask

    pt_apply_mask = oBis.new_property_type(
        code="APPLY_MASK",
        label="Apply Mask",
        description="Apply Mask",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_apply_mask)

    # Mask radius

    pt_mask_radius = oBis.new_property_type(
        code="MASK_RADIUS",
        label="Mask radius",
        description="Mask radius in pixels after application of the binning operation",
        dataType="REAL",
    )
    register_property_type(oBis, pt_mask_radius)

    # High Pass Filter (EMsoft)

    pt_hipass = oBis.new_property_type(
        code="HI_PASS_FILTER",
        label="High Pass Filter w",
        description="High pass filter w parameter; 0.05 is a reasonable value",
        dataType="REAL",
    )
    register_property_type(oBis, pt_hipass)

    # Regions for adaptive histogram equalization

    pt_num_regions = oBis.new_property_type(
        code="REGIONS_ADAPTIVE_HIST_EQUALISATION",
        label="# Regions AHE",
        description="Number of regions for adaptive histogram equalization",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_num_regions)

    # Include background flag

    pt_include_background = oBis.new_property_type(
        code="INCLUDE_BACKGROUND",
        label="Include Background",
        description="Include Background",
        dataType="BOOLEAN",
    )
    register_property_type(oBis, pt_include_background)

    # EDAX Team zip

    # Number of areas in an analysis file (e.g. EDAX Team zip)

    pt_num_areas = oBis.new_property_type(
        code="NUM_AREAS",
        label="# Areas",
        description="Number of analysed areas",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_num_areas)

    # Number of samples in an analysis file (e.g. EDAX Team zip)

    pt_num_samples = oBis.new_property_type(
        code="NUM_SAMPLES",
        label="# Samples",
        description="Number of samples in file",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_num_samples)

    # Project Name

    pt_project_name = oBis.new_property_type(
        code="PROJECT_NAME",
        label="Project Name",
        description="Name of the analysis project",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_project_name)

    # Sample Name

    pt_sample_name = oBis.new_property_type(
        code="SAMPLE_NAME",
        label="Sample Name(s)",
        description="Name of the sample(s)",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_sample_name)

    # #########################################################################
    # Objects
    # #########################################################################

    # EBSD Monte Carlo simulation protocol (mainly EMsoft)

    obj_ebsd_sim_mc = oBis.new_object_type(
        code=code_obj_ebsd_sim_mc,
        generatedCodePrefix="EBSDSIM_MC",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_ebsd_sim_mc)
    obj_ebsd_sim_mc = oBis.get_object_type(code_obj_ebsd_sim_mc)
    obj_ebsd_sim_mc.description = (
        "Settings for the Monte-Carlo simulation of EBSD image"
    )
    register_object_type(oBis, obj_ebsd_sim_mc)

    obj_ebsd_sim_mc = oBis.get_object_type(code_obj_ebsd_sim_mc)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
        ],
        "Monte Carlo": [
            ("EBSDSIM_MODE", 1, None),
            ("NUM_ELECTRONS", 1, None),
            ("TILTANGLE", 1, None),
            ("TILTANGLE_RD", 0, None),
            ("HV", 1, None),
            ("MIN_ENERGY", 1, None),
            ("ENERGY_BIN_SIZE", 0, None),
            ("DEPTH_MAX", 0, None),
            ("DEPTH_STEP", 0, None),
            ("FOIL_THICKNESS", 0, None),
        ],
    }
    assign_property_types(obj_ebsd_sim_mc, sec2props)

    # EBSD Sim Master Protocol (mainly EMsoft)

    obj_ebsd_sim_master = oBis.new_object_type(
        code=code_obj_ebsd_sim_master,
        generatedCodePrefix="EBSDSIM_MASTER",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_ebsd_sim_master)
    obj_ebsd_sim_master = oBis.get_object_type(code_obj_ebsd_sim_master)
    obj_ebsd_sim_master.description = "Settings for the generation of an EBSD masterpattern from the MonteCarlo simulation"
    register_object_type(oBis, obj_ebsd_sim_master)

    obj_ebsd_sim_master = oBis.get_object_type(code_obj_ebsd_sim_master)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
        ],
        "Masterpattern Simulation": [
            ("DMIN", 1, None),
            ("NPX", 1, None),
            ("LAT_GRID_TYPE", 1, None),
        ],
    }
    assign_property_types(obj_ebsd_sim_master, sec2props)

    # EBSD Sim Screen Pattern Protocol (mainly EMsoft)

    obj_ebsd_sim_screen = oBis.new_object_type(
        code=code_obj_ebsdsim_screen,
        generatedCodePrefix="EBSDSIM_SCREEN",
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_ebsd_sim_screen)
    obj_ebsd_sim_screen = oBis.get_object_type(code_obj_ebsdsim_screen)
    obj_ebsd_sim_screen.description = (
        "Settings for the screen pattern simulation for EBSD patterns"
    )
    register_object_type(oBis, obj_ebsd_sim_screen)

    obj_ebsd_sim_screen = oBis.get_object_type(code_obj_ebsdsim_screen)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
        ],
        "Screen pattern settings": [
            ("EBSD_L", 0, None),
            ("THETA_CAMERA", 0, None),
            ("CCD_PIXEL_SIZE", 0, None),
            ("CCD_PIXEL_X", 0, None),
            ("CCD_PIXEL_X", 0, None),
            ("EBSD_PATTERN_CENTRE_X", 0, None),
            ("EBSD_PATTERN_CENTRE_Y", 0, None),
            ("ANGLE_SAMPLENORMAL_DETECTOR", 0, None),
            ("ALPHA_BARREL_DISTORTION", 0, None),
            ("EBSDIM_ENERGYRANGE_MIN", 0, None),
            ("EBSDIM_ENERGYRANGE_MAX", 0, None),
            ("EULERANGLE_CONVENTION", 0, None),
            ("DWELL_TIME", 0, None),
            ("BEAM_CURRENT_NA", 0, None),
            ("EMSOFT_BINNING_MODE", 0, None),
            ("APPLY_LATTICE_DEFORMATION", 0, None),
            ("LATTICE_DEFORMATION_TENSOR", 0, None),
            ("GAMMA_CORRECTION_FACTOR", 0, None),
        ],
    }
    assign_property_types(obj_ebsd_sim_screen, sec2props)

    # #########################################################################
    # Dataset types
    # #########################################################################

    # Internal file (output of MC simulation, specific to a software package)

    ds_ebsd_sim_internal = oBis.new_dataset_type(
        code=code_ds_ebsd_sim_internal,
        description="Internal file of the simulation software, e.g. output of Monte Carlo simulation",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_ebsd_sim_internal)

    ds_ebsd_sim_internal = oBis.get_dataset_type(code_ds_ebsd_sim_internal)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Job Details": [
            ("SOFTWARENAME", 1, None),
            ("SOFTWAREVERSION", 0, None),
            ("DATETIME", 0, None),
            ("DATETIME_END", 0, None),
            ("USER_NAME", 0, None),
        ],
        "Monte Carlo": [
            ("NUM_ELECTRONS", 0, None),
            ("ELECTRON_MULTIPLIER", 0, None),
            ("ENERGY_BIN_SIZE", 0, None),
            ("HV", 0, None),
            ("MIN_ENERGY", 0, None),
            ("EBSDSIM_MODE", 0, None),
            ("DEPTH_MAX", 0, None),
            ("DEPTH_STEP", 0, None),
            ("OUTPUT_FILENAME", 0, None),
            ("CRYSTAL_STRUCTURE_FILENAME", 0, None),
            ("NPIXELX", 0, None),
            ("TILTANGLE", 0, None),
            ("TILTANGLE_RD", 0, None),
            ("ANGLE_START", 0, None),
            ("ANGLE_END", 0, None),
            ("ANGLE_STEP", 0, None),
            ("INTERACTION_VOLUME_X", 0, None),
            ("INTERACTION_VOLUME_Y", 0, None),
            ("INTERACTION_VOLUME_Z", 0, None),
        ],
    }
    assign_property_types(ds_ebsd_sim_internal, sec2props)

    # Masterpattern file

    ds_ebsd_sim_master = oBis.new_dataset_type(
        code=code_ds_ebsd_sim_master,
        description="Masterpattern for the EBSD Simulation",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_ebsd_sim_master)

    ds_ebsd_sim_master = oBis.get_dataset_type(code_ds_ebsd_sim_master)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Job Details": [
            ("SOFTWARENAME", 1, None),
            ("SOFTWAREVERSION", 0, None),
            ("DATETIME", 0, None),
            ("DATETIME_END", 0, None),
            ("USER_NAME", 0, None),
        ],
        "Monte Carlo": [
            ("NUM_ELECTRONS", 0, None),
            ("ELECTRON_MULTIPLIER", 0, None),
            ("ENERGY_BIN_SIZE", 0, None),
            ("HV", 0, None),
            ("MIN_ENERGY", 0, None),
            ("EBSDSIM_MODE", 0, None),
            ("DEPTH_MAX", 0, None),
            ("DEPTH_STEP", 0, None),
            ("OUTPUT_FILENAME", 0, None),
            ("CRYSTAL_STRUCTURE_FILENAME", 0, None),
            ("NPIXELX", 0, None),
            ("TILTANGLE", 0, None),
            ("TILTANGLE_RD", 0, None),
            ("ANGLE_START", 0, None),
            ("ANGLE_END", 0, None),
            ("ANGLE_STEP", 0, None),
            ("INTERACTION_VOLUME_X", 0, None),
            ("INTERACTION_VOLUME_Y", 0, None),
            ("INTERACTION_VOLUME_Z", 0, None),
        ],
        "Master Pattern": [
            ("BETHE_PARAMETERS", 0, None),
            ("BETHE_PARAM_FILENAME", 0, None),
            ("DMIN", 0, None),
            ("NPX", 0, None),
            ("LAT_GRID_TYPE", 0, None),
            ("UNIFORM_MASTERPATTERN", 0, None),
            ("ENERGY_WEIGHTING", 0, None),
        ],
    }
    assign_property_types(ds_ebsd_sim_master, sec2props)

    # Screen pattern file

    ds_ebsd_sim_screen = oBis.new_dataset_type(
        code=code_ds_ebsdsim_screen,
        description="Screenpattern for the EBSD Simulation",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_ebsd_sim_screen)

    ds_ebsd_sim_screen = oBis.get_dataset_type(code_ds_ebsdsim_screen)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "Job Details": [
            ("SOFTWARENAME", 1, None),
            ("SOFTWAREVERSION", 0, None),
            ("DATETIME", 0, None),
            ("DATETIME_END", 0, None),
            ("USER_NAME", 0, None),
        ],
        "Screen Pattern": [
            ("MASTERPATTERN_FILENAME", 0, None),
            ("EULERANGLE_FILENAME", 0, None),
            ("EULERANGLE_CONVENTION", 0, None),
            ("EULERANGLE_TYPE", 0, None),
            ("EULER_ANGLE_1", 0, None),
            ("EULER_ANGLE_2", 0, None),
            ("EULER_ANGLE_3", 0, None),
            ("APPLY_LATTICE_DEFORMATION", 0, None),
            ("INCLUDE_BACKGROUND", 0, None),
            ("EBSDIM_ENERGYRANGE_MIN", 0, None),
            ("EBSDIM_ENERGYRANGE_MAX", 0, None),
            ("BEAM_CURRENT_NA", 0, None),
            ("DWELL_TIME", 0, None),
            ("EBSD_L", 0, None),
            ("THETA_CAMERA", 0, None),
            ("CCD_PIXEL_SIZE", 0, None),
            ("CCD_PIXEL_X", 0, None),
            ("CCD_PIXEL_Y", 0, None),
            ("EBSD_PATTERN_CENTRE_X", 0, None),
            ("EBSD_PATTERN_CENTRE_Y", 0, None),
            ("ALPHA_BARREL_DISTORTION", 0, None),
            ("BIT_DEPTH", 0, None),
            ("DATA_TYPE", 0, None),
            ("APPLY_NOISE", 0, None),
            ("EMSOFT_BINNING_MODE", 0, None),
            ("GAMMA_CORRECTION_FACTOR", 0, None),
            ("APPLY_MASK", 0, None),
            ("MASK_RADIUS", 0, None),
            ("HI_PASS_FILTER", 0, None),
            ("REGIONS_ADAPTIVE_HIST_EQUALISATION", 0, None),
            ("EBSDSIM_INTENSITY_SCALING_MODE", 0, None),
            ("CRYSTAL_STRUCTURE_FILENAME", 0, None),
        ],
    }
    assign_property_types(ds_ebsd_sim_screen, sec2props)

    #  EBSD Exprimental Data
    #  (generic DS type for any EBSD related data)

    ds_ebsd_exp = oBis.new_dataset_type(
        code=code_ds_ebsd_exp,
        description="Experimental EBSD record",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_ebsd_exp)

    ds_ebsd_exp = oBis.get_dataset_type(code_ds_ebsd_exp)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "EBSD Details": [
            ("PROJECT_NAME", 0, None),
            ("SAMPLE_NAME", 0, None),
            ("NUM_SAMPLES", 0, None),
            ("NUM_AREAS", 0, None),
        ],
    }
    assign_property_types(ds_ebsd_exp, sec2props)

    # contained in EDAX TEAM/ZIP and EDAX APEX/H5
    # additional info in EDAX APEX/H5
    # - product_version
    # - TimeStamp
    # - company

    ds_ebsd_eds_data = oBis.new_dataset_type(
        code=code_ds_ebsd_eds_data,
        description="Data from EBSD with EDS",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_ebsd_eds_data)

    ds_ebsd_eds_data = oBis.get_dataset_type(code_ds_ebsd_eds_data)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        "EBSD Details": [
            ("PROJECT_NAME", 0, None),
            ("SAMPLE_NAME", 0, None),
            ("NUM_SAMPLES", 0, None),
            ("NUM_AREAS", 0, None),
        ],
        # 'Section Name': [
        #     ('PROPERTY_CODE', is_mandatory, plugin),
        # ],
    }
    assign_property_types(ds_ebsd_eds_data, sec2props)

#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create the schema and metadata information

    Create the schema and metadata information for the use of OpenBIS in
    materials science, in particular SFB/CRC 1394.
    The focus of this collaborative research consortium is to understand
    the role of defect phases in materials science.
    This mandates a strong collaboration between theory, simulation,
    experimental work, machine learning, mathematical optimisation, and others.
    Therefore, the metadata schema is very complex to be able to cover
    all relevant data types, experimental devices and machines.

"""


from pybis import Openbis
import logging
import argparse
import io
import os
from contextlib import redirect_stdout
from schema_helpers import get_and_set_token


# Schema definitions

from openbis_schema.general_properties import create_general_properties
from openbis_schema.create_software_schema import create_software_schema
from openbis_schema.create_sample_schema import create_sample_schema
from openbis_schema.create_instrument_schema import create_instrument_schema
from openbis_schema.create_consumable_schema import create_consumable_schema

# Protocols / Relevant details can be entered in generic experimental steps

from openbis_schema.create_sampleprep_protocol_schema import (
    create_sampleprep_protocol_schema,
)
from openbis_schema.create_metalloprep_protocol_schema import (
    create_metalloprep_protocol_schema,
)

# Protocols, custom experiments and data

from openbis_schema.create_simulation_schema import create_simulation_schema
from openbis_schema.create_tensile_schema import create_tensile_schema
from openbis_schema.create_sem_schema import create_sem_schema
from openbis_schema.create_ebsd_schema import create_ebsd_schema
from openbis_schema.create_eds_schema import create_eds_schema
from openbis_schema.create_opticalmicroscopy_schema import create_lom_schema
from openbis_schema.create_tem_schema import create_tem_schema
from openbis_schema.create_apt_schema import create_apt_schema
from openbis_schema.create_electrochem_schema import create_electrochem_schema
from openbis_schema.create_xray_diffraction_schema import create_xray_diffraction_schema
from openbis_schema.create_slip_line_analysis_schema import (
    create_slip_line_analysis_schema,
)


def main(oBis: Openbis):
    # Fetch token from environment variable

    get_and_set_token(oBis)

    logging.info("Create general entities")
    create_general_properties(oBis=oBis)

    logging.info("Create schema for samples")
    create_sample_schema(oBis=oBis)

    logging.info("Create schema for consumables")
    create_consumable_schema(oBis=oBis)

    logging.info("Create schema for scientific instruments")
    create_instrument_schema(oBis=oBis)

    logging.info("Create schema for software")
    create_software_schema(oBis=oBis)

    logging.info("Create schema for atomistic and DFT simulations")
    create_simulation_schema(oBis=oBis)

    logging.info("Create schema for metallographic preparation protocols")
    create_metalloprep_protocol_schema(oBis=oBis)

    logging.info("Create schema for sample creation and preparation")
    create_sampleprep_protocol_schema(oBis=oBis)

    logging.info("Create schema for mechanical testing protocols and experiments")
    create_tensile_schema(oBis=oBis)

    logging.info("Create schema for slip line analysis")
    create_slip_line_analysis_schema(oBis=oBis)

    logging.info("Create schema for scanning electron microscopy")
    create_sem_schema(oBis=oBis)

    logging.info("Create schema for EBSD experiments / simulation")
    create_ebsd_schema(oBis=oBis)

    logging.info("Create schema for energy dispersive spectroscopy")
    create_eds_schema(oBis=oBis)

    logging.info("Create schema for optical microscopy")
    create_lom_schema(oBis=oBis)

    logging.info("Create schema for (scanning) transmission electron microscopy")
    create_tem_schema(oBis=oBis)

    logging.info("Create schema for atom probe tomography")
    create_apt_schema(oBis=oBis)

    logging.info("Create schema for xray diffraction")
    create_xray_diffraction_schema(oBis=oBis)

    logging.info("Create schema for electrochemical / corrosion anaylsis")
    create_electrochem_schema(oBis=oBis)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", dest="debug")
    parser.add_argument("--test", action="store_true", dest="test")

    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    # Create connection to openBIS server

    TEST_SERVER_URL = "https://openbis-t.imm.rwth-aachen.de/:8443"
    PROD_SERVER_URL = "https://openbis.imm.rwth-aachen.de/:8443"
    try:
        openbis_url = os.environ["OPENBIS_ENDPOINT"]
    except KeyError:
        openbis_url = PROD_SERVER_URL
    if args.test:
        openbis_url = TEST_SERVER_URL
    oBis = Openbis(openbis_url, verify_certificates=False)

    if args.debug:
        main(oBis)
    else:
        trap = io.StringIO()
        with redirect_stdout(trap):
            main(oBis)
# N.B.
# At CRC1394/IMM, previous protocols were already created manually
# in the web-interface.
# These are re-created here with a script using a fixed code
# (whereas the manual creation used auto-defined code)
# because the other protocols were used already by users,
# we cannot overwrite them (nor change the code)
# thus, whenever we run this function,
# we need to delete the new protocols created.

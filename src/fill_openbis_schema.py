#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Fill the schema defined in "create_openbis_schema.py"
    
    Fill the schema with collections, typical entries in collections,
    in particular for the use within SFB/CRC 1394.
    The focus of this collaborative research consortium is to understand
    the role of defect phases in materials science.
    This mandates a strong collaboration between theory, simulation,
    experimental work, machine learning, mathematical optimisation, and others.
    Therefore, the metadata schema is very complex to be able to cover
    all relevant data types, experimental devices and machines.
    
"""


from pybis import Openbis
import logging
import argparse
import io
import os
from contextlib import redirect_stdout
from schema_helpers import get_and_set_token

from openbis_schema.openbis_data.fill_general import fill_general

from openbis_schema.openbis_data.fill_consumables import fill_consumables
from openbis_schema.openbis_data.fill_equipment import fill_equipment
from openbis_schema.openbis_data.fill_software import fill_software

from openbis_schema.openbis_data.fill_metalloprep_protocols import (
    fill_metalloprep_protocols,
)
from openbis_schema.openbis_data.fill_tensile_protocols import fill_tensile_protocols
from openbis_schema.openbis_data.fill_ebsdsim_protocols import fill_ebsdsim_protocols
from openbis_schema.openbis_data.fill_samples import fill_samples


def main(oBis: Openbis):
    # Fetch token from environment variable

    get_and_set_token(oBis)

    # General project, collections, etc, e.g. "top-level" stuff

    logging.info("Fill general")
    fill_general(oBis=oBis)

    logging.info("Fill consumables")
    fill_consumables(oBis=oBis)

    logging.info("Fill equipment")
    fill_equipment(oBis=oBis)

    logging.info("Fill software")
    fill_software(oBis=oBis)

    logging.info("Fill samples")
    fill_samples(oBis=oBis)

    # logging.info('Fill metallographic preparation protocols')
    # fill_metalloprep_protocols(oBis=oBis)

    # logging.info('Fill tensile testing protocols')
    # fill_tensile_protocols(oBis=oBis)

    # logging.info('Fill EBSD protocols')
    # fill_ebsdsim_protocols(oBis=oBis)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", dest="debug")
    parser.add_argument("--test", action="store_true", dest="test")

    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    # Create connection to openBIS server

    TEST_SERVER_URL = "https://openbis-t.imm.rwth-aachen.de/:8443"
    PROD_SERVER_URL = "https://openbis.imm.rwth-aachen.de/:8443"
    try:
        openbis_url = os.environ["OPENBIS_ENDPOINT"]
    except KeyError:
        openbis_url = PROD_SERVER_URL
    if args.test:
        openbis_url = TEST_SERVER_URL
    oBis = Openbis(openbis_url, verify_certificates=False)

    if args.debug:
        main(oBis)
    else:
        trap = io.StringIO()
        with redirect_stdout(trap):
            main(oBis)

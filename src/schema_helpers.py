#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""

    Copyright 2025 Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


    _summary_ : Define helper functions for schema creation

"""
import os
from pathlib import Path
import sys
import logging
from pybis import Openbis
import pandas as pd
from datetime import datetime


def ordinal(n):
    """Returns the ordinal representation of an integer.

    Args:
        n (int): The input integer.

    Returns:
        str: The ordinal representation (e.g. "1st", "2nd", "3rd", etc.).
    """
    if n in [11, 12, 13]:
        suffix = "th"
    else:
        suffix = ["th", "st", "nd", "rd", "th"][min(n % 10, 4)]
    return str(n) + suffix


def get_and_set_token(oBis: Openbis):
    logging.info(f"Logging into OpenBIS Server: {oBis.url}")
    try:
        token = os.environ["OPENBIS_TOKEN"]
        logging.debug("OpenBIS token found")
    except KeyError as e:
        token = None
        logging.debug("OpenBIS token not found: {}".format(e))
    if token == None:
        logging.critical("No valid OpenBIS token found, aborting.")
        sys.exit(1)
    oBis.set_token(token, save_token=True)
    logging.info("OpenBIS login successful.")


def register_controlled_vocabulary(oBis, vocabulary, terms):
    logging.debug("Register controlled vocabulary: %s" % vocabulary.code)
    try:
        vocabulary.save()
        logging.debug("Controlled vocabulary successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            logging.debug("Controlled vocabulary already registered!")
            old_vocabulary = oBis.get_vocabulary(vocabulary.code)
            old_codes = old_vocabulary.get_terms().df.code.to_list()
            new_terms = pd.DataFrame(terms)
            new_codes = new_terms.code.to_list()

            # Update description

            v = getattr(vocabulary, "description")
            setattr(old_vocabulary, "description", v)

            # Update terms

            for term in new_terms.itertuples(index=False):
                term = oBis.new_term(
                    code=term.code,
                    label=term.label,
                    description=term.description,
                    vocabularyCode=vocabulary.code,
                )
                if term.code not in old_codes:
                    term.save()
                else:
                    old_term = old_vocabulary.get_terms()[term.code]
                    for k in ["label", "description", "official"]:
                        v = getattr(term, k)
                        setattr(old_term, k, v)
                        old_term.save()
            for old_code in old_codes:
                if old_code not in new_codes:
                    old_term = old_vocabulary.get_terms()[old_code]
                    # old_term.delete()
                    # FIXME
                    # Cannot update vocabulary term through pybis (1.36.3)
            old_vocabulary.save()
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_plugin(oBis, plugin):
    plugin_name = plugin.name
    logging.debug("Register plugin: %s" % plugin_name)
    try:
        plugin.save()
        logging.debug("Plugin successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            logging.debug("Plugin already registered!")

            old_plugin = oBis.get_plugin(plugin_name)
            for k in ["description", "script", "available"]:
                v = getattr(plugin, k)
                setattr(old_plugin, k, v)
            # old_plugin.save()
            # FIXME
            # Cannot update plugin through pybis (1.36.3)

            # Workaround (Delete old plugin and replace it with the new one)

            old_plugin = oBis.get_plugin(plugin_name)
            plugin_type = old_plugin.pluginType
            property_type = None
            for openbis_object in old_plugin.entityKinds:
                if openbis_object == "SAMPLE":
                    for object_type in oBis.get_sample_types():
                        if object_type.validationPlugin == plugin_name:
                            setattr(object_type, "validationPlugin", None)
                            object_type.save()
                        df = object_type.get_property_assignments().df
                        if "plugin" not in df.columns:
                            df["plugin"] = None
                        df = df[df.plugin == plugin_name]
                        for _, row in df.iterrows():
                            code = row["code"]
                            property_type = oBis.get_property_type(code)
                            object_type.revoke_property(code, force=True)
                            object_type.save()
            old_plugin.delete("Update Schema")
            plugin.save()
            if plugin_type == "ENTITY_VALIDATION":
                setattr(object_type, "validationPlugin", plugin_name)
            if plugin_type == "DYNAMIC_PROPERTY" and property_type is not None:
                object_type.assign_property(
                    prop=property_type.code,
                    plugin=plugin_name,
                )
            object_type.save()
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_property_type(oBis, property_type):
    logging.debug("Register property type: %s" % property_type.code)
    try:
        property_type.save()
        logging.debug("Property type successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            logging.debug("Property type already registered!")
            old_property_type = oBis.get_property_type(property_type.code)
            for k in ["label", "description"]:
                v = getattr(property_type, k)
                setattr(old_property_type, k, v)
            old_property_type.save()
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_object_type(oBis, object_type):
    logging.debug("Register object type: %s" % object_type.code)
    try:
        object_type.save()
        logging.debug("Object type successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            # Update description and plugin

            old_object_type = oBis.get_object_type(object_type.code)
            for attr in ["description", "validationPlugin"]:
                v = getattr(object_type, attr)
                setattr(old_object_type, attr, v)
            old_object_type.save()
            logging.debug("Object type already registered!")
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_dataset_type(oBis, dataset_type):
    logging.debug("Register dataset type: %s" % dataset_type.code)
    try:
        dataset_type.save()
        logging.debug("Object type successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            logging.debug("Object type already registered!")
            # Update description

            old_dataset_type = oBis.get_dataset_type(dataset_type.code)
            v = getattr(dataset_type, "description")
            setattr(old_dataset_type, "description", v)
            old_dataset_type.save()
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def assign_property_types(entity_type, sec2props, empty_value=None):
    # In OpenBIS, sections are displayed in reverse insertion order

    sec2props = dict(reversed(sec2props.items()))

    # initialValueForExistingEntities = "empty" for already existing fields
    # (e.g. if defined previously and already used)
    # FIXME Better handle datatype and default values

    properties_list = []
    for section_name, properties in sec2props.items():
        for prop_ord, (prop_name, is_mand, plugin) in enumerate(properties, 1):
            intial_value = empty_value if is_mand else None
            entity_type.assign_property(
                prop=prop_name,
                section=section_name,
                ordinal=prop_ord,
                mandatory=is_mand,
                showInEditView=True,
                initialValueForExistingEntities=intial_value,
                plugin=plugin,
            )
            properties_list.append(prop_name)
    properties = ", ".join(p for p in properties_list)
    code = entity_type.code
    logging.debug(
        "Property types: %s successfully assigned to Object type %s !"
        % (properties, code)
    )


def register_project(oBis, project):
    try:
        project.save()
        logging.debug("Project successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            old_project = oBis.get_project(project.code)
            v = getattr(project, "description")
            setattr(old_project, "description", v)
            old_project.save()
            logging.debug("Project already registered!")
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_collection(oBis, collection, collection_identifier):
    try:
        collection.save()
        logging.debug("Collection successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            old_collection = oBis.get_collection(collection_identifier)
            for k in ["$name", "$default_object_type"]:
                v = collection.props.get(k)
                if v:
                    old_collection.props[k] = v
            old_collection.save()
            logging.debug("Collection already registered!")
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def register_object(object):
    try:
        object.save()
        logging.debug("Object successfully registered!")
    except ValueError as e:
        if str(e).find("already exists") > -1:
            logging.debug("Object already registered!")
        elif str(e).find("Access denied") > -1:
            logging.debug("You do NOT have ADMIN rights")
        else:
            logging.debug(str(e))


def read_most_recent_csv(
    directory: str,
    prefix: str = "software-",
    extension: str = ".csv",
    date_fromat: str = "%Y%m",
):
    paths = Path(directory).glob(f"{prefix}*{extension}")
    files = sorted(
        paths,
        key=lambda f: datetime.strptime(f.stem.split(prefix[-1])[1], date_fromat),
        reverse=True,
    )
    if not files:
        raise FileNotFoundError(f"No valid {extension} files found in {directory}.")
    return pd.read_csv(files[0], keep_default_na=False)

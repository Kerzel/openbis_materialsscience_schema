#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Fill the existing schema with typical consumables and create corresponding collections

"""
from pybis import Openbis
import logging
import pandas as pd
import numpy as np

from .. import create_consumable_schema
from . import general_codes
from schema_helpers import register_collection, register_object, read_most_recent_csv


code_collection_abrasives = "ABRASIVE_CONSUMABLE_COLLECTION"
code_collection_etching = "ETCHING_CONSUMABLE_COLLECTION"
code_collection_electrolytes = "ELECTROLYTE_CONSUMABLE_COLLECTION"
code_collection_general = "GENERAL_CONSUMABLE_COLLECTION"
code_collection_lubricants = "LUBRICANTS_CONSUMABLE_COLLECTION"
code_collection_polishing = "POLISH_CONSUMABLE_COLLECTION"
code_collection_raw_materials = "RAWMATERIALS_CONSUMABLE_COLLECTION"
code_collection_supply = "SUPPLY_CONSUMABLE_COLLECTION"


code_obj_ethanol = "S603010030"
code_obj_water_tap = "TAP_WATER"
code_obj_water_distilled = "DIST_WATER"
code_obj_nickel = "NICKEL"
code_obj_yttrium = "YTTRIUM"
code_obj_erbium = "ERBIUM"


def fill_consumables(oBis: Openbis):

    # When populating the inventory from scratch, we first used the DaMaRIS database as a starting point.
    # c.f. https://git.rwth-aachen.de/Kerzel/openbis_materialsscience_schema/-/blob/5f6d063b/src/openbis_schema/openbis_data/fill_consumables.py
    # We now provide a file containing the curated list of consumables.

    # Read data from csv

    df_consumables = read_most_recent_csv("data", "consumables-")
    NUMERICAL_PROPERTIES = ["CONCENTRATION_PERCENT", "GRIT"]

    ## ########################################################################
    ## Create Collections
    ## ########################################################################

    ## Space

    space_materials = oBis.get_space(code=general_codes.space_materials_code)

    ## Project

    consumables_project_identifier = (
        "/" + space_materials.code + "/" + general_codes.project_consumables_code
    )
    project_consumables = oBis.get_project(consumables_project_identifier)

    ## Collections

    # General

    logging.debug("Create collection for general consumables")
    collection_general = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_general,
        props={
            "$name": "General Chemicals",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_general
    )
    register_collection(oBis, collection_general, collection_identifier)
    collection_general = oBis.get_collection(collection_identifier)

    # Lubricants

    logging.debug("Create collection for lubricants")
    collection_lubricants = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_lubricants,
        props={
            "$name": "Lubricants",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_lubricants
    )
    register_collection(oBis, collection_lubricants, collection_identifier)
    collection_lubricants = oBis.get_collection(collection_identifier)

    # Etching solutions

    logging.debug("Create collection for etching solutions")
    collection_etching = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_etching,
        props={
            "$name": "Etching Solutions",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_etching
    )
    register_collection(oBis, collection_etching, collection_identifier)
    collection_etching = oBis.get_collection(collection_identifier)

    # Polishing suspensions

    logging.debug("Create collection for polishing suspensions")
    collection_polishing = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_polishing,
        props={
            "$name": "Polishing Suspensions",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_polishing
    )
    register_collection(oBis, collection_polishing, collection_identifier)
    collection_polishing = oBis.get_collection(collection_identifier)

    # Electrolytes

    logging.debug("Create collection for electrolytes")
    collection_electrolytes = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_electrolytes,
        props={
            "$name": "Electrolytes",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_electrolytes
    )
    register_collection(oBis, collection_electrolytes, collection_identifier)
    collection_electrolytes = oBis.get_collection(collection_identifier)

    # Supply

    logging.debug("Create collection for supply")
    collection_supply = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_supply,
        props={
            "$name": "Supply",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_supply
    )
    register_collection(oBis, collection_supply, collection_identifier)
    collection_supply = oBis.get_collection(collection_identifier)

    # Abrasives

    logging.debug("Create collection for abrasives")
    collection_abrasives = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_abrasives,
        props={
            "$name": "Abrasives / Polishing Materials",
            "$default_object_type": create_consumable_schema.code_obj_prep_material,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_abrasives
    )
    register_collection(oBis, collection_abrasives, collection_identifier)
    collection_abrasives = oBis.get_collection(collection_identifier)

    # Raw materials (e.g. for creating new samples, etc)

    logging.debug("create collection for Raw Materials")
    collection_raw_materials = oBis.new_collection(
        type="COLLECTION",
        project=project_consumables.code,
        code=code_collection_raw_materials,
        props={
            "$name": "Raw Materials",
            "$default_object_type": create_consumable_schema.code_obj_chemical,
        },
    )
    collection_identifier = (
        consumables_project_identifier + "/" + code_collection_raw_materials
    )
    register_collection(oBis, collection_raw_materials, collection_identifier)
    collection_raw_materials = oBis.get_collection(collection_identifier)

    ## ########################################################################
    ## Fill Collections
    ## ########################################################################

    # Ethanol

    logging.debug("Fill ethanol")
    ethanol = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_general.identifier,
        code=code_obj_ethanol,
        props={
            "$name": "Ethanol",
            "chemical_type": "GENERIC",
            "cas_number": "64-17-5",
            "alt_name": "Alkohol | Ethyl alcohol | Ethylalkohol",
            "used_in": "IMM",
        },
    )
    register_object(ethanol)

    # Tap Water

    logging.debug("Fill tap water")
    water_tap = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_general.identifier,
        code=code_obj_water_tap,
        props={
            "$name": "Water, Tap",
            "chemical_type": "GENERIC",
            "cas_number": "7732-18-5",
            "alt_name": "Tap Water | Leitungswasser",
            "used_in": "IMM",
        },
    )
    register_object(water_tap)

    # Distilled Water

    logging.debug("Fill distilled water")
    water_distilled = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_general.identifier,
        code=code_obj_water_distilled,
        props={
            "$name": "Water, Distilled",
            "chemical_type": "GENERIC",
            "cas_number": "7732-18-5",
            "alt_name": "Distilled Water | Destilliertes Wasser",
            "used_in": "IMM",
        },
    )
    register_object(water_distilled)

    # Nickel (raw material)

    logging.debug("Fill nickel")
    nickel = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_raw_materials.identifier,
        code=code_obj_nickel,
        props={
            "$name": "Nickel (IEHK)",
            "alt_name": "Ni",
            "chemical_type": "RAW_MATERIAL",
            "cas_number": "7440-02-0",
            "used_in": "IMM,IEHK",
            "comments": "Provided by Prof. Dr.-Ing. Hauke Springer",
        },
    )
    register_object(nickel)

    # Yittrium (raw material)

    logging.debug("Fill Yttrium")
    yttrium = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_raw_materials.identifier,
        code=code_obj_yttrium,
        props={
            "$name": "Yittrium (IEHK)",
            "alt_name": "Y",
            "chemical_type": "RAW_MATERIAL",
            "cas_number": "7440-65-5",
            "used_in": "IMM,IEHK",
            "comments": "Provided by Prof. Dr.-Ing. Hauke Springer",
        },
    )
    register_object(yttrium)

    # Erbium (raw material)

    logging.debug("Fill Erbium")
    erbium = oBis.new_object(
        type=create_consumable_schema.code_obj_chemical,
        space=space_materials.code,
        experiment=collection_raw_materials.identifier,
        code=code_obj_erbium,
        props={
            "$name": "Erbium (IEHK)",
            "alt_name": "Er",
            "chemical_type": "RAW_MATERIAL",
            "cas_number": "7440-52-0",
            "used_in": "IMM,IEHK",
            "comments": "Provided by Prof. Dr.-Ing. Hauke Springer",
        },
    )
    register_object(erbium)

    logging.debug("Fill collections with Consumables")

    # Get all existing consumables to avoid that the transaction fails if we add those we have already.

    existing_consumables = oBis.get_objects(
        project=general_codes.project_consumables_code, attrs=["code"]
    )
    existing_consumables_codes = set(existing_consumables.df["code"].tolist())

    transaction = oBis.new_transaction()

    for index, row in df_consumables.iterrows():
        code = row["CODE"]
        logging.debug(
            "--> Adding {} to Inventory {}/{}:".format(
                code, index + 1, len(df_consumables)
            )
        )
        if code in existing_consumables_codes:
            logging.debug("Skipping {}".format(code))
            continue
        props = {
            k.lower(): v
            for k, v in row.to_dict().items()
            if v != "" and k not in ["CODE", "TYPE", "EXPERIMENT"]
        }
        props = {
            k: float(v) if k.upper() in NUMERICAL_PROPERTIES else v
            for k, v in props.items()
        }

        consumable = oBis.new_object(
            code=code,
            type=row["TYPE"],
            space=space_materials.code,
            experiment=row["EXPERIMENT"],  # Collection = Experiment
            props=props,
        )
        transaction.add(consumable)
    transaction.commit()

#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 202 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    _summary_ : Create metadata schema for scientific instruments

    To start with, use a generic instrument type (add others later if needed).
"""

from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

code_obj_instrument = "INSTRUMENT"  # Generic Instrument/Device
code_obj_electron_microscope = "ELECTRON_MICROSCOPE"
code_obj_furnace = "FURNACE"
code_obj_mech_test_device = "MECH_TEST_DEVICE"
code_obj_electrode = "ELECTRODE"


def create_instrument_schema(oBis: Openbis):
    """Creates Instruments / Devices Object Types.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    terms_tensile_motor = [
        {
            "code": "TENSILE_MOTOR_DISPLACEMENT",
            "label": "Displacement controlled motor control for tesile stage",
            "description": "Displacement controlled motor control for stage",
        },
        {
            "code": "TENSILE_MOTOR_LOAD",
            "label": "Load controlled motor control for tensile stage",
            "description": "Load controlled motor control for stage",
        },
    ]
    voc_tensile_motor = oBis.new_vocabulary(
        code="TENSILE_MOTOR_CONTROL",
        description="Types of Motor Control for Tensile Testing",
        terms=terms_tensile_motor,
    )
    register_controlled_vocabulary(oBis, voc_tensile_motor, terms_tensile_motor)

    terms_electrode_types = [
        {
            "code": "CU-CUSO4",
            "label": "Copper/Copper Sulphate Electrode",
            "description": "Copper/Copper Sulphate Electrode",
        },
        {
            "code": "SCE",
            "label": "Saturated Calomel Electrode",
            "description": "Saturated Calomel Electrode",
        },
        {
            "code": "AG-AGCL",
            "label": "Silver/Silver Chloride Electrode",
            "description": "Silver/Silver Chloride Electrode",
        },
        {
            "code": "SHE",
            "label": "Standard Hydrogen Electrode",
            "description": "Standard Hydrogen Electrode",
        },
    ]
    voc_electrode_type = oBis.new_vocabulary(
        code="TENSILE_MOTOR_CONTROL",
        description="Types of Electrodes",
        terms=terms_electrode_types,
    )
    register_controlled_vocabulary(oBis, voc_electrode_type, terms_electrode_types)

    # #########################################################################
    # Property Types
    # #########################################################################

    # General Instrument properties

    pt_maintenance = oBis.new_property_type(
        code="LAST_MAINTENANCE",
        label="Last Maintenance",
        description="Date of the last maintenance",
        dataType="TIMESTAMP",
    )
    register_property_type(oBis, pt_maintenance)

    pt_equipment = oBis.new_property_type(
        code="EQUIPMENT",
        label="Equipment",
        description="Details about equipment at the instrument (detectors, sensors, etc)",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_equipment)

    # Electron Microscope properties

    # Electron Beam

    pt_electron_beam = oBis.new_property_type(
        code="BEAM",
        label="Electron Beam",
        description="Electron beam properties",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_electron_beam)

    # Detector / Camera

    pt_camera = oBis.new_property_type(
        code="DETECTOR_CAMERA",
        label="Detector / Camera",
        description="Details about detectors, cameras, etc.",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_camera)

    # Tensile Stage properties

    # Maximum load

    pt_maximum_load = oBis.new_property_type(
        code="LOAD_MAX",
        label="Max. Load (N)",
        description="Maximum Load [N]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_maximum_load)

    # Displacement range

    pt_displacement_range = oBis.new_property_type(
        code="DISPLACEMENT_RANGE",
        label="Displacement Range (mm)",
        description="Displacement Range [mm]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_displacement_range)

    # Minimum strain rate

    pt_minimum_strain_rate = oBis.new_property_type(
        code="STRAIN_RATE_MIN",
        label="Min. Strain Rate (mm/min)",
        description="Minimum Strain Rate [mm/min]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_minimum_strain_rate)

    # Maximum strain rate

    pt_maximum_strain_rate = oBis.new_property_type(
        code="STRAIN_RATE_MAX",
        label="Max. Strain Rate (mm/min)",
        description="Maximum Strain Rate [mm/min]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_maximum_strain_rate)

    # Motor control

    pt_motor_control = oBis.new_property_type(
        code="TENSILE_MOTOR_CONTROL",
        label="Type of Motor Control",
        description="Type of Motor Control",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_tensile_motor.code,
    )
    register_property_type(oBis, pt_motor_control)

    # Electrode type

    pt_electrode_type = oBis.new_property_type(
        code="ELECTRODE_TYPE",
        label="Type",
        description="Electrode Type",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_electrode_type.code,
    )
    register_property_type(oBis, pt_electrode_type)

    # #########################################################################
    # Objects
    # #########################################################################

    # Generic instrument /device

    obj_instrument = oBis.new_object_type(
        code=code_obj_instrument,
        generatedCodePrefix=code_obj_instrument,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_instrument)
    obj_instrument = oBis.get_object_type(code_obj_instrument)
    obj_instrument.description = "Generic Scientific Instrument / Device"
    register_object_type(oBis, obj_instrument)

    obj_instrument = oBis.get_object_type(code_obj_instrument)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("COMPANY", 0, None),
            ("DEVICE", 0, None),
            ("DEVICEMODEL", 0, None),
            ("LOCATION", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Instrument Details": [
            ("EQUIPMENT", 0, None),
            ("LAST_MAINTENANCE", 0, None),
        ],
    }
    assign_property_types(obj_instrument, sec2props)

    # Electron Microscope

    obj_microscope = oBis.new_object_type(
        code=code_obj_electron_microscope,
        generatedCodePrefix=code_obj_electron_microscope,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_microscope)
    obj_microscope = oBis.get_object_type(code_obj_electron_microscope)
    obj_microscope.description = "Electron Microscope - Instrument / Device"
    register_object_type(oBis, obj_microscope)

    obj_microscope = oBis.get_object_type(code_obj_electron_microscope)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("COMPANY", 0, None),
            ("DEVICE", 0, None),
            ("DEVICEMODEL", 0, None),
            ("LOCATION", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Microscope Details": [
            ("BEAM", 0, None),
            ("DETECTOR_CAMERA", 0, None),
            ("LAST_MAINTENANCE", 0, None),
        ],
    }
    assign_property_types(obj_microscope, sec2props)

    # Mechanical Testing Device

    obj_mech_test_device = oBis.new_object_type(
        code=code_obj_mech_test_device,
        generatedCodePrefix=code_obj_mech_test_device,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_mech_test_device)
    obj_mech_test_device = oBis.get_object_type(code_obj_mech_test_device)
    obj_mech_test_device.description = "Mechanical Testing Instrument / Device"
    register_object_type(oBis, obj_mech_test_device)

    obj_mech_test_device = oBis.get_object_type(code_obj_mech_test_device)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("COMPANY", 0, None),
            ("DEVICE", 0, None),
            ("DEVICEMODEL", 0, None),
            ("LOCATION", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Mechanical Testing Device Details": [
            ("LOAD_MAX", 0, None),
            ("DISPLACEMENT_RANGE", 0, None),
            ("STRAIN_RATE_MIN", 0, None),
            ("STRAIN_RATE_MAX", 0, None),
            ("SAMPLING_RATE_MIN", 0, None),
            ("SAMPLING_RATE_MAX", 0, None),
            ("TEMPERATURE_MAX", 0, None),
            ("DEFORMATION_MODE", 0, None),
            ("TENSILE_MOTOR_CONTROL", 0, None),
            ("SAMPLE_GEOMETRY_MAX", 0, None),
            ("LAST_MAINTENANCE", 0, None),
        ],
    }
    assign_property_types(obj_mech_test_device, sec2props)

    # Furnace

    obj_furnace = oBis.new_object_type(
        code=code_obj_furnace,
        generatedCodePrefix=code_obj_furnace,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_furnace)
    obj_furnace = oBis.get_object_type(code_obj_furnace)
    obj_furnace.description = "Furnace - Instrument / Device"
    register_object_type(oBis, obj_furnace)

    obj_furnace = oBis.get_object_type(code_obj_furnace)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("COMPANY", 0, None),
            ("DEVICE", 0, None),
            ("DEVICEMODEL", 0, None),
            ("LOCATION", 1, None),
            ("COMMENTS", 0, None),
        ],
        "Furnace Details": [
            ("TEMPERATURE_MAX", 0, None),
            ("WEIGHT_MAX", 0, None),
            ("LAST_MAINTENANCE", 0, None),
        ],
    }
    assign_property_types(obj_furnace, sec2props)

    # Electrode

    obj_electrode = oBis.new_object_type(
        code=code_obj_electrode,
        generatedCodePrefix=code_obj_electrode,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_electrode)
    obj_electrode = oBis.get_object_type(code_obj_electrode)
    obj_electrode.description = "Electrode"
    register_object_type(oBis, obj_electrode)

    obj_electrode = oBis.get_object_type(code_obj_electrode)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("$BARCODE", 0, None),
            ("ELECTRODE_TYPE", 0, None),
            ("COMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_electrode, sec2props)

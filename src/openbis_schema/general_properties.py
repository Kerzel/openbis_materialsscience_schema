#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
    Copyright 2025 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
    
    _summary_ : Create general controlled vocabularies, properties, etc.

"""
    
from pybis import Openbis
from schema_helpers import *


MAX_NUM_ELEMENTS = 8 # Maximum number of elements in a sample
MAX_NUM_DETECTORS = 6 # Maximum number of detectors SEM/(S)TEM


def create_general_properties(oBis: Openbis):
    """Creates general Controlled Vocabularies, Property Types & Object Types.

    Protocols that are closely related and are a set of complex of activities 
    are better defined in a separate file.
    
    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    #  Controlled Vocabularies
    # #########################################################################

    # Mesured / Calculated Quantities
    terms_quantity_type = [
        {"code": "HARDNESS", "label": "Hardness", "description": "https://www.materials.fraunhofer.de/ontologies/BWMD_ontology/domain#BWMD_00634"},
        {"code": "VOLUME_FRACTION", "label": "Volume Fraction", "description": "https://w3id.org/pmd/co/VolumeFraction"},
        {"code": "POISSON_RATIO", "label": "Poisson Ratio", "description": "https://www.materials.fraunhofer.de/ontologies/BWMD_ontology/domain#BWMD_00409"},
        {"code": "YOUNG_MODULUS", "label": "Young's Modulus", "description": "https://www.materials.fraunhofer.de/ontologies/BWMD_ontology/domain#BWMD_00471,http://matonto.org/ontologies/matonto#YoungsModulus"},
        {"code": "SHEAR_MODULUS", "label": "Shear Modulus", "description": "https://www.materials.fraunhofer.de/ontologies/BWMD_ontology/domain#BWMD_00619,http://matonto.org/ontologies/matonto#ShearModulus"},
        {"code": "BULK_MODULUS", "label": "Bulk Modulus", "description": "http://matonto.org/ontologies/matonto#YoungsModulus"},
        {"code": "FINAL_TOTAL_ENERGY", "label": "Final Total Energy", "description": "http://purls.helmholtz-metadaten.de/asmo/TotalEnergy"},
        {"code": "FINAL_VOLUME", "label": "Final Volume", "description": "http://purls.helmholtz-metadaten.de/asmo/Volume"},
        {"code": "TOTAL_MAGNETIC_MOMENT", "label": "Total Magnetic Moment", "description": "http://purls.helmholtz-metadaten.de/asmo/TotalMagneticMoment"},
    ]
    voc_quantity_type = oBis.new_vocabulary(
        code = 'QUANTITY_VOCAB',
        description = 'Type of (measured / calculated) physical quantity',
        terms = terms_quantity_type,
    )
    register_controlled_vocabulary(oBis, voc_quantity_type, terms_quantity_type)  


    # Notebook type
    terms_nb_type = [
        {'code': 'PYTHON', 'label': 'Python', 'description': 'Python notebook'},
        {'code': 'MATLAB', 'label': 'Matlab', 'description': 'Matlab notebook'},
        {'code': 'OCTAVE', 'label': 'Octave', 'description': 'Octave notebook'},
    ]
    voc_nb_type = oBis.new_vocabulary(
        code = 'NOTEBOOK_TYPE_VOCAB',
        description = 'Type of notebbok',
        terms = terms_nb_type,
    )
    register_controlled_vocabulary(oBis, voc_nb_type, terms_nb_type)  


    # General Simulation types
    terms_sim_type = [
        {'code': 'DFT', 'label': 'DFT', 'description': 'DFT simulation: http://purls.helmholtz-metadaten.de/asmo/DensityFunctionalTheory'},
        {'code': 'ATOM', 'label': 'Atomistic', 'description': 'Atomistic simulation'},
        {'code': 'EBSD', 'label': 'EBSD', 'description': 'EBSD simulation'}
    ]
    voc_sim_type = oBis.new_vocabulary(
        code = 'SIMULATION_TYPE',
        description = 'Type of simulation',
        terms = terms_sim_type,
    )
    register_controlled_vocabulary(oBis, voc_sim_type, terms_sim_type)


    # EBSD Simulation types
    terms_ebsd_sim = [
        {'code': 'MC', 'label': 'Monte-Carlo', 'description': 'Monte-Carlo simulation for EBSD'},
        {'code': 'MASTER', 'label': 'Master', 'description': 'EBSD Master pattern simulation'},
        {'code': 'SCREEN', 'label': 'Screen', 'description': 'EBSD Screen pattern simulation'},
    ]
    voc_ebsd_sim_type = oBis.new_vocabulary(
        code = 'EBSDSIM_TYPE',
        description = 'Type of the EBSD Simulation',
        terms = terms_ebsd_sim
    )
    register_controlled_vocabulary(oBis, voc_ebsd_sim_type, terms_ebsd_sim)


    # Atomistic Simulation types
    terms_atom_sim = [
        {'code': 'ATOM-MC', 'label': 'Monte-Carlo (Atomistic)', 'description': 'Monte-Carlo Atomistic simulation: http://purls.helmholtz-metadaten.de/asmo/MonteCarloMethod'},
        {'code': 'ATOM-MD', 'label': 'Molecular Dynamics (Atomistic)', 'description': 'Molecular Dynamics (Atomistic) simulation: http://purls.helmholtz-metadaten.de/asmo/MolecularDynamics'},
        {'code': 'ATOM-MS', 'label': 'Molecular Statics (Atomistic)', 'description': 'Molecular Statics (Atomistic) simulation: http://purls.helmholtz-metadaten.de/asmo/MolecularStatics'},
        {'code': 'ATOM-SG', 'label': 'Sample Generation (Atomistic)', 'description': 'Sample Generation (Atomistic) simultaion'},
    ]
    voc_atom_sim_type = oBis.new_vocabulary(
        code = 'ATOMSIM_TYPE',
        description = 'Type of the Atomistic Simulation',
        terms = terms_atom_sim,
    )
    register_controlled_vocabulary(oBis, voc_atom_sim_type, terms_atom_sim)


    # Euler Angle conventions
    terms_euler_angle = [
        {'code': 'HKL', 'label': 'hkl', 'description': 'hkl'},
        {'code': 'TSL', 'label': 'tsl', 'description': 'tsl'},
    ]
    voc_euler_angle = oBis.new_vocabulary(
        code = 'EULERANGLE_CONVENTION',
        description = 'Convention for Euler angles',
        terms = terms_euler_angle
    )
    register_controlled_vocabulary(oBis, voc_euler_angle, terms_euler_angle)


    # Deformation Mode
    terms_deform_mode = [
        {'code': 'BEND', 'label': 'Bending', 'description': 'Bending tensile testing'},
        {'code': 'COMPRESS', 'label': 'Compression', 'description': 'Compression tensile testing'},
        {'code': 'TENSILE', 'label': 'Tensile', 'description': 'Tensile testing: https://w3id.org/pmd/tto/TensileTestMethod'},
    ]
    voc_deform_mode = oBis.new_vocabulary(
        code = 'DEFORMATION_MODE',
        description = 'Deformation mode for mechanical/tensile testing',
        terms = terms_deform_mode,
    )
    register_controlled_vocabulary(oBis, voc_deform_mode, terms_deform_mode)


    # Manufacturer
    terms_manufacturer = [
        {'code': 'ZEISS', 'label': 'Zeiss', 'description': 'Zeiss'},
        {'code': 'TESCAN', 'label': 'Tescan', 'description': 'Tescan'},
        {'code': 'FEI', 'label': 'FEI / Thermo Fisher Scientific', 'description': 'FEI / ThermoFischer'},
        {'code': 'JEOL', 'label': 'JEOL', 'description': 'JEOL'},
        {'code': 'HITACHI', 'label': 'Hitachi', 'description': 'Hitachi'},
        {'code': 'LEICA', 'label': 'Leica', 'description': 'Leica'},
        {'code': 'BRUKER', 'label': 'Bruker', 'description': 'Bruker'},
        {'code': 'NIKON', 'label': 'Nikon', 'description': 'Nikon'},
        {'code': 'OXFORD', 'label': 'Oxford Instruments', 'description': 'Oxford Instruments'},
        {'code': 'KLA', 'label': 'KLA', 'description': 'KLA'},
        {'code': 'FEMTOTOOLS', 'label': 'FemtoTools', 'description': 'FemtoTools'},
    ]
    voc_manufacturer = oBis.new_vocabulary(
        code = 'MANUFACTURER_VOCAB',
        description = 'Manufacturer',
        terms = terms_manufacturer,
    )
    register_controlled_vocabulary(oBis, voc_manufacturer, terms_manufacturer)

    # Tool used for computation
    terms_compute_type = [
        {'code': 'WORKSTATION', 'label': 'Workstation', 'description': 'Workstation'},
        {'code': 'HPC', 'label': 'HPC cluster', 'description': 'HPC cluster'},
	]
    voc_compute_type = oBis.new_vocabulary(
        code = 'COMPUTE_RESOURCE_VOCAB',
        description = 'Type of Compute Resource',
        terms = terms_compute_type,
    )
    register_controlled_vocabulary(oBis, voc_compute_type, terms_compute_type)

    # #########################################################################
    # Property Types
    # #########################################################################

    pt_finished_flag = oBis.new_property_type(
        code = 'FINISHED_FLAG',
        label = 'Experiment completed',
        description = 'Marks the experiment as finished',
        dataType = 'BOOLEAN',
    )
    register_property_type(oBis, pt_finished_flag)
    
    pt_start_date = oBis.new_property_type(
        code = 'START_DATE',
        label = 'Start date',
        description = 'Start date',
        dataType = 'TIMESTAMP',
    )
    register_property_type(oBis, pt_start_date)

    pt_end_date = oBis.new_property_type(
        code = 'END_DATE',
        label = 'End date',
        description = 'End date',
        dataType = 'TIMESTAMP',
    )
    register_property_type(oBis, pt_end_date)
    
    pt_goals = oBis.new_property_type(
        code = 'EXPERIMENTAL_STEP.EXPERIMENTAL_GOALS',
        label = 'Experimental goals',
        description = 'Goals of the experiment',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_goals)

    pt_description = oBis.new_property_type(
        code = 'EXPERIMENTAL_STEP.EXPERIMENTAL_DESCRIPTION',
        label = 'Experimental description',
        description = 'Description of the experiment',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_description)

    pt_results = oBis.new_property_type(
        code = 'EXPERIMENTAL_STEP.EXPERIMENTAL_RESULTS',
        label = 'Experimental results',
        description = 'Summary of experimental results',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_results)

    pt_spreadsheet = oBis.new_property_type(
        code = 'EXPERIMENTAL_STEP.SPREADSHEET',
        label = 'Spreadsheet',
        description = 'Multi purpose Spreatsheet',
        dataType = 'XML',
    )
    register_property_type(oBis, pt_spreadsheet)

    pt_references = oBis.new_property_type(
        code = 'REFERENCE',
        label = 'References',
        description = 'Useful references // Nützliche Referenzen',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_references)

    pt_publication = oBis.new_property_type(
        code = 'PUBLICATION',
        label = 'Publication',
        description = 'Own publication where this entity is referenced',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_publication)

    pt_notes = oBis.new_property_type(
        code = 'NOTES',
        label = 'Notes',
        description = 'Notes // Notizen',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_notes)
    
    # Presigned URL to download files from S3
    # NOT extracted by the parser, periodically created by an automatic script
    pt_download_link = oBis.new_property_type(
        code = 'S3_DOWNLOAD_LINK',
        label = 'Download Link',
        description = 'Download URL for dataset stored in S3/Coscine',
        dataType = 'HYPERLINK',
    )
    register_property_type(oBis, pt_download_link)
    
    # Institution
    pt_institution = oBis.new_property_type(
        code = 'INSTITUTION',
        label = 'Institution',
        description = 'Company/Institute/Lab where the data was created',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_institution)

    # Notebook type
    pt_nb_type = oBis.new_property_type(
        code = 'NOTEBOOK_TYPE',
        label = 'Notebook type',
        description = 'Type of notebook',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_nb_type.code,
    )
    register_property_type(oBis, pt_nb_type)

    # Comments
    pt_comments = oBis.new_property_type(
        code = 'COMMENTS',
        label = 'Comments',
        description = 'Comments',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_comments)

    # Description - Short description
    pt_description = oBis.new_property_type(
        code = 'DESCRIPTION',
        label = 'Description',
        description = 'Further details',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_description)

    # Description - Short description Multiline
    pt_description_multiline = oBis.new_property_type(
        code = 'DESCRIPTION_MULTILINE',
        label = 'Description',
        description = 'Short description and/or purpose // Kurzbeschreibung und/oder Zweck',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_description_multiline)
    
    # Conceptual Dictionary - Pyiron 
    pt_pyiron_concept_dict = oBis.new_property_type(
        code = 'PYIRON_CONCEPTUAL_DICTIONARY',
        label = 'Conceptual Dictionary',
        description = 'Conceptual dictionary associated with pyiron // Begriffswörterbuch zu pyiron',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_pyiron_concept_dict)

    # Company / Manufacturer / Supplier
    pt_company = oBis.new_property_type(
        code = 'COMPANY',
        label = 'Company',
        description = 'Manufacturer / Supplier',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_company)

    # Address
    pt_address = oBis.new_property_type(
        code = 'ADDRESS',
        label = 'Address',
        description = 'Address',
        dataType = 'MULTILINE_VARCHAR',
    )
    register_property_type(oBis, pt_address) 

    # Time
    pt_time = oBis.new_property_type(
        code = 'TIME',
        label = 'Time',
        description = 'Time',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_time)

    # Date
    pt_time = oBis.new_property_type(
        code = 'DATE',
        label = 'Date',
        description = 'Date (iso-format)',
        dataType = 'DATE',
    )
    register_property_type(oBis, pt_time)

    # Datetime
    pt_datetime = oBis.new_property_type(
        code = 'DATETIME',
        label = 'DateTime',
        description = 'DateTime',
        dataType = 'TIMESTAMP',
    )
    register_property_type(oBis, pt_datetime)

    # Datetime End
    pt_datetime_end = oBis.new_property_type(
        code = 'DATETIME_END',
        label = 'DateTime End',
        description = 'DateTime (End)',
        dataType = 'TIMESTAMP',
    )
    register_property_type(oBis, pt_datetime_end) 

    # Device Name
    pt_device = oBis.new_property_type(
        code = 'DEVICE',
        label = 'Device',
        description = 'Generic Device Name',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_device)

    # Device Model
    pt_device_model = oBis.new_property_type(
        code = 'DEVICEMODEL',
        label = 'Device Model',
        description = 'Generic Device Model',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_device_model)

    # Manufacturer
    pt_device_manufacturer = oBis.new_property_type(
        code = 'DEVICE_MANUFACTURER',
        label = 'Device Manufacturer',
        description = 'Manufacturer of the device',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_manufacturer.code,
    )
    register_property_type(oBis, pt_device_manufacturer)
    
    # Device full name (incl. model code)
    pt_device_name = oBis.new_property_type(
        code = 'DEVICE_NAME',
        label = 'Device Name',
        description = 'Name of the device',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_device_name)

    # Generic location
    pt_location = oBis.new_property_type(
        code = 'LOCATION',
        label = 'Location',
        description = 'Generic location (RWTH-IMM:E01, virtual, processed)',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_location) 
 
    # Specific location
    pt_sublocation = oBis.new_property_type(
        code = 'SUBLOCATION',
        label = 'Sublocation',
        description = 'Specific location (desiccator, workstation, etc.)',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_sublocation) 

    # Jobs submitted to a cluster are usually identified with a unique JobID
    pt_job_id = oBis.new_property_type(
        code = 'CLUSTER_JOBID',
        label = 'JobID',
        description = 'Unique job identifier on compute cluster',
        dataType = 'VARCHAR',
    )   
    register_property_type(oBis, pt_job_id)
    
    # Number of cores used in a job
    pt_job_ncores = oBis.new_property_type(
        code = 'JOB_N_CORES',
        label = '# Cores',
        description = 'Number of cores used by job',
        dataType = 'VARCHAR',
    )   
    register_property_type(oBis, pt_job_ncores)

    # Number of GPUs used in a job
    pt_job_ngpus = oBis.new_property_type(
        code = 'JOB_N_GPUS',
        label = '# GPUs',
        description = 'Number of GPUs used by job',
        dataType = 'VARCHAR',
    )   
    register_property_type(oBis, pt_job_ngpus)

    # Number of nodes used in a job
    pt_job_nnodes = oBis.new_property_type(
        code = 'JOB_N_NODES',
        label = '# Nodes',
        description = 'Number of nodes used by job',
        dataType = 'VARCHAR',
    )   
    register_property_type(oBis, pt_job_nnodes)

    # Job run time
    pt_job_run_time = oBis.new_property_type(
        code = 'JOB_RUN_TIME',
        label = 'Run Time',
        description = 'Job Run Time',
        dataType = 'VARCHAR', 
    )   
    register_property_type(oBis, pt_job_run_time)

    # Requested run time
    pt_job_run_time_req = oBis.new_property_type(
        code = 'JOB_REQ_RUN_TIME',
        label = 'Run Time (Requested)',
        description = 'Requested Run Time',
        dataType = 'VARCHAR', 
    )   
    register_property_type(oBis, pt_job_run_time_req)

   # General Simulation type
    pt_sim_type = oBis.new_property_type(
        code = 'SIMULATION_TYPE',
        label = 'General simulation type',
        description = 'Type of simulation (DFT/Atomistic/EBSD)',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_sim_type.code,
    )
    register_property_type(oBis, pt_sim_type)

    # EBSD Simulation type
    pt_ebsd_sim_type = oBis.new_property_type(
        code = 'EBSDSIM_TYPE',
        label = 'EBSD Simulation Type',
        description = 'Type of the EBSD Simulation',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_ebsd_sim_type.code,
    )
    register_property_type(oBis, pt_ebsd_sim_type)

    # Atomistic Simulation type
    pt_atom_sim_type = oBis.new_property_type(
        code = 'ATOMSIM_TYPE',
        label = 'Atomistic Simulation Type',
        description = 'Type of the Atomistic Simulation',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_atom_sim_type.code,
    )
    register_property_type(oBis, pt_atom_sim_type)
    
    # Flag for STEM images
    pt_is_stem = oBis.new_property_type(
        code = 'IS_STEM',
        label = 'Is STEM',
        description = 'Flag for STEM images',
        dataType = 'BOOLEAN',
    )
    register_property_type(oBis, pt_is_stem)

    # SEM/(S)TEM detectors
    for i in range(1, 1 + MAX_NUM_DETECTORS):
        pt_detector_name = oBis.new_property_type(
            code = f'DETECTOR{i}_NAME',
            label = f'Detector{i}',
            description = f'Detector{i}',
            dataType = 'VARCHAR',
        )
        register_property_type(oBis, pt_detector_name)

        pt_detector_type = oBis.new_property_type(
            code = f'DETECTOR{i}_TYPE',
            label = f'Detector{i} Type',
            description = f'Type of Detector{i}',
            dataType = 'VARCHAR',
        )
        register_property_type(oBis, pt_detector_type)
    
        pt_detector_gain = oBis.new_property_type(
            code = f'DETECTOR{i}_GAIN',
            label = f'Detector{i} Gain',
            description = f'Detector{i}Gain',
            dataType = 'REAL',
        )
        register_property_type(oBis, pt_detector_gain)

        pt_detector_offset = oBis.new_property_type(
            code = f'DETECTOR{i}_OFFSET',
            label = f'Detector{i} OffSet',
            description = f'Detector{i}OffSet',
            dataType = 'REAL',
        )
        register_property_type(oBis, pt_detector_offset)

    # Beam current - SEM / EBSD SIM
    pt_beam_current = oBis.new_property_type(
        code = 'BEAM_CURRENT_NA',
        label = 'Beam Current (nA)',
        description = 'Electron Beam Current in nA: https://w3id.org/pmd/mo/BeamCurrent,http://qudt.org/vocab/unit/NanoA',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_beam_current)

    # Electron Beam Dwell Time - SEM / EBSD SIM
    pt_dwell_time = oBis.new_property_type(
        code = 'DWELL_TIME',
        label = 'Dwell Time (μs)',
        description = 'Electron Beam dwell time in μs: https://w3id.org/pmd/mo/DwellTime,http://qudt.org/vocab/unit/MicroSEC',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_dwell_time)

    # Euler angle convention
    pt_eulerangle_convention = oBis.new_property_type(
        code = 'EULERANGLE_CONVENTION',
        label = 'Euler angle convention',
        description = 'Convention for Euler angles',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_euler_angle.code,
    )
    register_property_type(oBis, pt_eulerangle_convention)

    # Generic (sample) weight
    pt_weight_g = oBis.new_property_type(
        code = 'WEIGHT_G',
        label = 'Weight (g)',
        description = 'Weight [g]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_weight_g)

    # Sample temperature
    pt_sample_temperature = oBis.new_property_type(
        code = 'SAMPLE_TEMP',
        label = 'Sample Temperature (°C)',
        description = 'Sample Temperature [°C]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_sample_temperature) 
    
    # Room temperature
    pt_room_temperature = oBis.new_property_type(
        code = 'ROOM_TEMP',
        label = 'Room Temperature (°C)',
        description = 'Room temperature in which the experiment was performed',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_room_temperature)

    # Room Relative Humidity - SEM
    pt_room_rel_humidity = oBis.new_property_type(
        code = 'ROOM_REL_HUMIDITY',
        label = 'Room relative humidity (%)',
        description = 'Relative humidity of the room where the experiment ' \
            'was performed',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_room_rel_humidity)

    # Gas used in Experiment - Casting / SEM
    pt_gas_exp = oBis.new_property_type(
        code = 'GAS_EXP',
        label = 'Gas in experiment',
        description = 'Gas used in experimental setup',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_gas_exp)

    # Pre tilt angle - EBSD EXP
    pt_pretilt_angle = oBis.new_property_type(
        code = 'PRETILT_ANGLE',
        label = 'Pre-tilt angle (deg)',
        description = 'Pre-tilt angle due to the use of a sample holder ' \
            '(if applicable)',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_pretilt_angle)

    # Point Measurement (value +/- stat + syst_up - syst_down)

    pt_point_measurment_value = oBis.new_property_type(
        code = 'VALUE',
        label = 'Measured Value',
        description = 'Measured Value',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_point_measurment_value)

    pt_statistical_uncertainty = oBis.new_property_type(
        code = 'UNCERTAINTY_STAT',
        label = 'Statistical uncertainty',
        description = 'Statistical uncertainty',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_statistical_uncertainty)

    pt_upper_systematic_uncertainty = oBis.new_property_type(
        code = 'UNCERTAINTY_SYST_UP',
        label = 'Upper systematic uncertainty',
        description = 'Upper systematic uncertainty',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_upper_systematic_uncertainty)

    pt_lower_systematic_uncertainty = oBis.new_property_type(
        code = 'UNCERTAINTY_SYST_LOW',
        label = 'Lower systematic uncertainty',
        description = 'Lower systematic uncertainty',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_lower_systematic_uncertainty)
    
    # Point Measurement Quantity
    pt_point_measurment_quantity = oBis.new_property_type(
        code = 'PHYSICAL_QUANTITY',
        label = 'Physical Quantity',
        description = 'Measured / Calculated Physical Quantity',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_quantity_type.code
    )
    register_property_type(oBis, pt_point_measurment_quantity)

    # Point Measurement Unit 
    pt_point_measurment_unit = oBis.new_property_type(
        code = 'UNIT',
        label = 'Measurement Unit',
        description = 'Measurement Unit',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_point_measurment_unit)

    # Generic voltage
    pt_voltage = oBis.new_property_type(
        code = 'VOLTAGE',
        label = 'Voltage (V)',
        description = 'Voltage in volts',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_voltage)  

    # Generic Temperature
    pt_temperature = oBis.new_property_type(
        code = 'TEMPERATURE',
        label = 'Temperature (K)',
        description = 'Temperature in Kelvin',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_temperature)  

    # Generic minimum temperature
    pt_temperature_min = oBis.new_property_type(
        code = 'TEMPERATURE_MIN',
        label = 'Min. Temperature (°C)',
        description = 'Minimum Temperature [°C]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_temperature_min)

    # Generic maximum temperature
    pt_temperature_max = oBis.new_property_type(
        code = 'TEMPERATURE_MAX',
        label = 'Max. Temperature (°C)',
        description = 'Maximum Temperature [°C]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_temperature_max) 

    # Generic maximum weight allowed by device / instrument
    pt_weightmax = oBis.new_property_type(
        code = 'WEIGHT_MAX',
        label = 'Max. Weight (g)',
        description = 'Maximum Weight [g]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_weightmax)

    # Generic maximum force allowed by device / instrument
    pt_force_max = oBis.new_property_type(
        code = 'FORCE_MAX',
        label = 'Max. Force (kN)',
        description = 'Maximum Force [kN]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_force_max)

    # Generic maximum power allowed by device / instrument
    pt_power_max = oBis.new_property_type(
        code = 'POWER_MAX',
        label = 'Max. Power (W)', 
        description = 'Maximum Power [W]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_power_max)

    # Maximum sample geometry allowed by device / instrument
    pt_sample_geometry_max = oBis.new_property_type(
        code = 'SAMPLE_GEOMETRY_MAX',
        label = 'Max. Sample Geometry (mm mm mm)',
        description = 'Maximum Sample Geometry [mm mm mm]',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_sample_geometry_max)

    # Minimum sampling rate
    pt_sampling_rate_min = oBis.new_property_type(
        code = 'SAMPLING_RATE_MIN',
        label = 'Min. Sampling Rate (Hz)',
        description = 'Minimum Sampling Rate [Hz]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_sampling_rate_min)

    # Maximum sampling rate
    pt_sampling_rate_max = oBis.new_property_type(
        code = 'SAMPLING_RATE_MAX',
        label = 'Max. Sampling Rate (Hz)',
        description = 'Maximum Sampling Rate [Hz]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_sampling_rate_max)

    # Sampling rate
    pt_sampling_rate = oBis.new_property_type(
        code = 'SAMPLING_RATE',
        label = 'Sampling Rate (Hz)',
        description = 'Sampling Rate [Hz]',
        dataType = 'REAL',
    )
    register_property_type(oBis, pt_sampling_rate)
   
    # Internal ID - some internal number used in the organisation
    # e.g. "Stoffnummer" in DaMaRIS used by RWTH for chemicals
    pt_internal_id = oBis.new_property_type(
        code = 'ID_INTERNAL',
        label = 'Internal ID',
        description = 'Internal ID number used within an organisation',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_internal_id)

    # Alternative names
    pt_alternative_name = oBis.new_property_type(
        code = 'ALT_NAME',
        label = 'Alternative names',
        description = 'Aliases or synonyms',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_alternative_name)  

    # Calibration date
    pt_calibration_date = oBis.new_property_type(
        code = 'CALIBRATION_DATE',
        label = 'Calibration Date',
        description = 'Calibration Date',
        dataType = 'TIMESTAMP',
    )
    register_property_type(oBis, pt_calibration_date) 

    # Output filename - EBSD SIM
    pt_output_filename = oBis.new_property_type(
        code = 'OUTPUT_FILENAME',
        label = 'Output Filename',
        description = 'Output Filename',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_output_filename)

    # Crystal structure filename - EBSD SIM
    pt_crystal_structure_filename = oBis.new_property_type(
        code = 'CRYSTAL_STRUCTURE_FILENAME',
        label = 'Filename of the crystal structure',
        description = 'Filename holding details of the crystal structure',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_crystal_structure_filename) 

    # User Name - EBSD SIM / SEM / STEM
    pt_username = oBis.new_property_type(
        code = 'USER_NAME',
        label = 'User Name',
        description = 'User Name',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_username)

    # Data Size X
    pt_data_size_x = oBis.new_property_type(
        code = 'DATA_SIZE_X',
        label = 'DataSize X',
        description = 'Number of columns in data array',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_data_size_x)

    # Data Size Y
    pt_data_size_y = oBis.new_property_type(
        code = 'DATA_SIZE_Y',
        label = 'DataSize Y',
        description = 'Number of rows in data array',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_data_size_y)

    # Number of frames
    pt_number_of_frames = oBis.new_property_type(
        code = 'NUMBER_OF_FRAMES',
        label = 'Number Of Frames',
        description = 'Number of frames in image array',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_number_of_frames)

    # Number of channels
    pt_number_of_channels = oBis.new_property_type(
        code = 'NUMBER_OF_CHANNELS',
        label = 'Number Of Channels',
        description = 'Number of channels in image array',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_number_of_channels)

    # Number of datasets
    # In the case of STEM, each dataset corresponds to a detector
    pt_number_of_datasets = oBis.new_property_type(
        code = 'NUMBER_OF_DATASETS',
        label = 'Number Of Datasets',
        description = 'Number of datasets (number of detectors for STEM data)',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_number_of_datasets)

    # Bit depth 
    pt_bit_depth = oBis.new_property_type(
        code = 'BIT_DEPTH',
        label = 'Bit Depth',
        description = 'Bit Depth',
        dataType = 'INTEGER',
    )
    register_property_type(oBis, pt_bit_depth)

    # Datatype (uint8, float32, etc.)
    pt_datatype = oBis.new_property_type(
        code = 'DATA_TYPE',
        label = 'Data Type',
        description = 'Data Type',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_datatype)

    # Flag for linear LUT 
    pt_linear_lut = oBis.new_property_type(
        code = 'LINEAR_LUT',
        label = 'Linear LUT',
        description = 'True if pixel values are mapped to grayscale (not RGB)',
        dataType = 'BOOLEAN',
    )
    register_property_type(oBis, pt_linear_lut)

    # Instrument (free-text) - Metallography
    # Whenever more than one instrument is used, a number of steps per instrumrent
    # should be given (as indicated by the hint)
    pt_instrument = oBis.new_property_type(
        code = 'INSTRUMENT',
        label = 'Instrument',
        description = 'Instruments (NAME1:NUM_STEPS1+NAME2:NUM_STEPS2+NAME3:NUM_STEPS3)',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_instrument)

    # Deformation Mode
    pt_deformation_mode = oBis.new_property_type(
        code = 'DEFORMATION_MODE',
        label = 'Deformation Mode',
        description = 'Deformation Mode',
        dataType = 'CONTROLLEDVOCABULARY',
        vocabulary = voc_deform_mode,
    )
    register_property_type(oBis, pt_deformation_mode)

    # Electrolyte (free-text)
    pt_electrolyte = oBis.new_property_type(
        code = 'ELECTROLYTE',
        label = 'Electrolyte',
        description = 'Electrolyte',
        dataType = 'VARCHAR',
    )
    register_property_type(oBis, pt_electrolyte)

    # #########################################################################
    # Objects
    # #########################################################################
    
    # Experimental Step - Generic
    code_experimental_step = 'EXPERIMENTAL_STEP'
    obj_experimental_step = oBis.new_object_type(
        code = code_experimental_step,
        generatedCodePrefix = 'EXP',
        autoGeneratedCode = True,
    )
    register_object_type(oBis, obj_experimental_step)
    obj_experimental_step = oBis.get_object_type(code_experimental_step)
    obj_experimental_step.description = 'Experimental Step'
    register_object_type(oBis, obj_experimental_step)

    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props_experimental_step = {
        'General': [
            ('$NAME', 1, None), ('$SHOW_IN_PROJECT_OVERVIEW', 0, None),
            ('FINISHED_FLAG', 0, None),
            ('START_DATE', 1, None), ('END_DATE', 0, None),
        ],
        'Experimental Details': [
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_GOALS', 0, None),
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_DESCRIPTION', 0, None),
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_RESULTS', 0, None),
            ('EXPERIMENTAL_STEP.SPREADSHEET', 0, None),
        ],
        'References': [
            ('REFERENCE', 0, None),
            ('PUBLICATION', 0, None),
        ],
        'Comments': [
            ('NOTES', 0, None),
            ('$XMLCOMMENTS', 0, None),
            # ('$ANNOTATIONS_STATE', 0, None),
        ],
    }
    assign_property_types(obj_experimental_step, sec2props_experimental_step)

    # Simulation run / experiment
    code_simulation_experiment = 'SIMULATION_EXP'
    obj_sim_exp = oBis.new_object_type(
        code = code_simulation_experiment,
        generatedCodePrefix = 'SIM',
        autoGeneratedCode = True,
    )
    register_object_type(oBis, obj_sim_exp)
    obj_sim_exp = oBis.get_object_type(code_simulation_experiment)
    obj_sim_exp.description = 'Simulation job / run / experiement'
    register_object_type(oBis, obj_sim_exp)

    obj_sim_exp = oBis.get_object_type(code_simulation_experiment)

    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props_sim_exp = {
        'General': [
            ('$NAME', 1, None), ('$SHOW_IN_PROJECT_OVERVIEW', 0, None),
            ('FINISHED_FLAG', 0, None),
            ('START_DATE', 1, None), ('END_DATE', 0, None),
            ('COMMENTS', 0, None),
        ],
        'Simulation Details': [
            ('SIMULATION_TYPE', 1, None),
            ('EBSDSIM_TYPE', 0, None),
            ('ATOMSIM_TYPE', 0, None),
            ('CLUSTER_JOBID', 0, None),
            ('JOB_N_CORES', 0, None),
            ('JOB_N_GPUS', 0, None),
            ('JOB_N_NODES', 0, None),
            ('JOB_RUN_TIME', 0, None),
            ('JOB_REQ_RUN_TIME', 0, None),
        ],
        'Experimental Details': [
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_GOALS', 0, None),
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_DESCRIPTION', 0, None),
            ('EXPERIMENTAL_STEP.EXPERIMENTAL_RESULTS', 0, None),
        ],
    }
    assign_property_types(obj_sim_exp, sec2props_sim_exp)

    # Point Measurement
    code_point_meas = 'POINT_MEASUREMENT'
    obj_point_meas = oBis.new_object_type(
        code = code_point_meas,
        generatedCodePrefix = code_point_meas,
        autoGeneratedCode = True,
    )
    register_object_type(oBis, obj_point_meas)  
    obj_point_meas = oBis.get_object_type(code_point_meas)
    obj_point_meas.description = 'Measurement of a single quantity'
    register_object_type(oBis, obj_point_meas)  

    obj_point_meas = oBis.get_object_type(code_point_meas)

    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props_point_meas = {
        'General': [
            ('$NAME', 1, None), ('$SHOW_IN_PROJECT_OVERVIEW', 0, None),
            ('FINISHED_FLAG', 0, None),
            ('START_DATE', 1, None), ('END_DATE', 0, None),
            ('COMMENTS', 0, None),
        ],
        'Measurement Details': [
            ('PHYSICAL_QUANTITY', 0, None),
            ('VALUE', 1, None), ('UNIT', 1, None),
            ('UNCERTAINTY_STAT', 0, None),
            ('UNCERTAINTY_SYST_UP', 0, None),
            ('UNCERTAINTY_SYST_LOW', 0, None),
        ],
    }
    assign_property_types(obj_point_meas, sec2props_point_meas)

    # #########################################################################
    # Dataset types
    # #########################################################################

    # Notebook Type
    ds_notebook = oBis.get_dataset_type('ANALYSIS_NOTEBOOK')
    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props = {
        'General info': [
            ('$NAME', 1, None), ('DATE', 0, None),
            ('NOTEBOOK_TYPE', 0, None), ('S3_DOWNLOAD_LINK', 0, None)
        ],
        'Comments': [
            ('NOTES', 0, None), ('COMMENTS', 0, None),
            ('$XMLCOMMENTS', 0, None), ('$HISTORY_ID', 0, None),
        ],
    }
    assign_property_types(ds_notebook, sec2props)

    # Publication Data
    ds_pub_data = oBis.get_dataset_type('PUBLICATION_DATA')
    ds_pub_data.description = "Bibliography management data"
    ds_pub_data.save()

    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props = {
        'General info': [
            ('$NAME', 1, None), ('DATE', 0, None),
            ('S3_DOWNLOAD_LINK', 0, None),
        ],
        'Comments': [
            ('NOTES', 0, None), ('COMMENTS', 0, None),
            ('$XMLCOMMENTS', 0, None),
        ],
    }
    assign_property_types(ds_pub_data, sec2props)

    # Processed Data
    ds_processed_data = oBis.get_dataset_type('PROCESSED_DATA')
    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props = {
        'General info': [
            ('$NAME', 1, None), ('DATE', 0, None),
            ('S3_DOWNLOAD_LINK', 0, None),
        ],
        'Comments': [
            ('NOTES', 0, None), ('COMMENTS', 0, None),
            ('$XMLCOMMENTS', 0, None),
        ],
    }
    assign_property_types(ds_processed_data, sec2props)

    # Raw Data
    ds_raw_data = oBis.get_dataset_type('RAW_DATA')
    ds_raw_data.description = "Raw Data (Use domain-specific datasetTypes instead)"
    ds_raw_data.save()

    # Define display order
    # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    sec2props = {
        'General info': [
            ('$NAME', 1, None), ('DATE', 0, None),
            ('S3_DOWNLOAD_LINK', 0, None),
        ],
        'Comments': [
            ('NOTES', 0, None), ('COMMENTS', 0, None),
            ('$XMLCOMMENTS', 0, None),
        ],
    }
    assign_property_types(ds_raw_data, sec2props)

    # # Analyzed Data
    # ds_analyzed_data = oBis.get_dataset_type('ANALYZED_DATA')
    # # Define display order
    # # (PROPERTY_CODE, IS_MANADATORY, PLUGIN_CODE)
    # sec2props = {
    #     'General info': [
    #         ('$NAME', 1, None), ('DATE', 0, None),
    #         ('S3_DOWNLOAD_LINK', 0, None),
    #     ],
    #     'Comments': [
    #         ('NOTES', 0, None), ('COMMENTS', 0, None),
    #         ('$XMLCOMMENTS', 0, None),
    #     ],
    # }
    # assign_property_types(ds_analyzed_data, sec2props)

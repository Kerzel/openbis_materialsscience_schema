#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    Copyright 2024 Ulrich Kerzel, Khalil Rejiba

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


    _summary_ : Create metadata schema for sample creation and preparation

    Protocols that are closely related to creating and preparing samples,
    e.g. casting, heat treatment, etc.
"""


from pybis import Openbis
from schema_helpers import *


# openBIS codes for the objects (also used in other modules)

code_obj_casting = "CASTING_PROTOCOL"
code_obj_heat_treatment = "HEAT_TREATMENT_PROTOCOL"
code_obj_cutting = "CUTTING_PROTOCOL"
code_obj_fib_milling = "FIB_MILLING_PROTOCOL"
code_ds_fib_milling = "FIB_MILLING_RECIPE"


def create_sampleprep_protocol_schema(oBis: Openbis):
    """Creates Sample Prepration Object Types.

    Args:
        oBis (pybis.Openbis): OpenBIS python object.
    """

    # #########################################################################
    # Controlled Vocabularies
    # #########################################################################

    # Cooling agent

    terms_coolant = [
        {"code": "AIR", "label": "Air", "description": "Air cooling"},
        {"code": "WATER", "label": "Water", "description": "Water cooling"},
        {"code": "ARGON", "label": "Argon", "description": "Argon cooling"},
        {"code": "NITROGEN", "label": "Nitrogen", "description": "Nitrogen cooling"},
    ]
    voc_coolant = oBis.new_vocabulary(
        code="COOLANT",
        description="Cooling Agent",
        terms=terms_coolant,
    )
    register_controlled_vocabulary(oBis, voc_coolant, terms_coolant)

    # Cutting Technique

    terms_cutting = [
        {
            "code": "EDM",
            "label": "Electrical Discharge Machining",
            "description": "Electrical Discharge Machining",
        },
        {"code": "DWC", "label": "Diamond Wire", "description": "Diamond Wire Cutting"},
        {"code": "SAW", "label": "Saw", "description": "Saw"},
        {"code": "MANUAL-SAW", "label": "Manual Saw", "description": "Manual Saw"},
        {
            "code": "DISC_GRINDER",
            "label": "Disc Grinder",
            "description": "Disc Grinder",
        },
    ]
    voc_cut_tech = oBis.new_vocabulary(
        code="CUT_TECHNIQUE",
        description="Cutting Technique",
        terms=terms_cutting,
    )
    register_controlled_vocabulary(oBis, voc_cut_tech, terms_cutting)

    # Cutting Geometries

    terms_geometry = [
        {"code": "DISK", "label": "Disk", "description": "Disk"},
        {
            "code": "RECT",
            "label": "Rectangle",
            "description": "Strip / Bar / Cuboid / Rectangular Prism",
        },
        {"code": "DOG", "label": "Dog Bone", "description": "Dog Bone"},
        {
            "code": "DOG-SHEAR",
            "label": "Dog Bone - Shear",
            "description": "Dog Bone for Shear Test",
        },
        {
            "code": "CRUCIFORM",
            "label": "Cruciform / Biaxial",
            "description": "Biaxial cruciform specimen",
        },
    ]
    voc_cut_geom = oBis.new_vocabulary(
        code="CUT_GEOMETRY",
        description="Cutting Geometry",
        terms=terms_geometry,
    )
    register_controlled_vocabulary(oBis, voc_cut_geom, terms_geometry)

    # Sample Subtypes produced by FIB milling

    terms_subtypes = [
        {"code": "TEM_LAMELLA", "label": "TEM Lamella", "description": "TEM Lamella"},
        {
            "code": "MICRO_PILLAR",
            "label": "Micro Pillar",
            "description": "Micro Pillar",
        },
        {
            "code": "MICRO_CANTILEVER",
            "label": "Micro Cantilever",
            "description": "Micro Cantilever",
        },
        {"code": "APT_TIP", "label": "APT Tip", "description": "APT Tip"},
    ]
    voc_mill_type = oBis.new_vocabulary(
        code="FIB_MILLING_VOCAB",
        description="Type of FIB Milling",
        terms=terms_subtypes,
    )
    register_controlled_vocabulary(oBis, voc_mill_type, terms_subtypes)

    # #########################################################################
    # Property Types
    # #########################################################################

    # Casting / Heat Treatment
    # Suffix _2 is used to indicate a heating step
    # No suffix is used to indicate a cooling step

    # Cooling Agent

    pt_coolant = oBis.new_property_type(
        code="COOLANT",
        label="Coolant",
        description="Cooling Agent",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_coolant.code,
    )
    register_property_type(oBis, pt_coolant)

    # Heating rate

    pt_heating_rate = oBis.new_property_type(
        code="HEATING_RATE",
        label="Heating Rate (K/s)",
        description="Heating Rate [K/s]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_heating_rate)

    # Cooling rate

    pt_cooling_rate = oBis.new_property_type(
        code="COOLING_RATE",
        label="Cooling Rate (K/s)",
        description="Cooling Rate [K/s]",
        dataType="REAL",
    )
    register_property_type(oBis, pt_cooling_rate)

    # Starting Temperature

    pt_temp_start = oBis.new_property_type(
        code="TEMP_START",
        label="Starting Temperature (K)",
        description="Starting Temperature in Kelvin during cooling",
        dataType="REAL",
    )
    register_property_type(oBis, pt_temp_start)

    pt_temp_start_2 = oBis.new_property_type(
        code="TEMP_START_2",
        label="Starting Temperature (K)",
        description="Starting Temperature in Kelvin during heating",
        dataType="REAL",
    )
    register_property_type(oBis, pt_temp_start_2)

    # Final Temperature

    pt_temp_stop = oBis.new_property_type(
        code="TEMP_STOP",
        label="Final Temperature (K)",
        description="Final Temperature in Kelvin during cooling",
        dataType="REAL",
    )
    register_property_type(oBis, pt_temp_stop)

    pt_temp_stop_2 = oBis.new_property_type(
        code="TEMP_STOP_2",
        label="Final Temperature (K)",
        description="Final Temperature in Kelvin during heating",
        dataType="REAL",
    )
    register_property_type(oBis, pt_temp_stop_2)

    # Cooling profile

    pt_cooling_profile = oBis.new_property_type(
        code="COOLING_PROFILE",
        label="Cooling Profile",
        description="Non-linear or multistep cooling profile",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_cooling_profile)

    # Heating profile

    pt_heating_profile = oBis.new_property_type(
        code="HEATING_PROFILE",
        label="Heating Profile",
        description="Non-linear or multistep heating profile",
        dataType="MULTILINE_VARCHAR",
    )
    register_property_type(oBis, pt_heating_profile)

    # Pressure

    pt_pressure_cool = oBis.new_property_type(
        code="PRESSURE",
        label="Pressure (hPa)",
        description="Pressure in hPa during cooling",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pressure_cool)

    pt_pressure_heat = oBis.new_property_type(
        code="PRESSURE_2",
        label="Pressure (hPa)",
        description="Pressure in hPa during heating",
        dataType="REAL",
    )
    register_property_type(oBis, pt_pressure_heat)

    # Hold time

    pt_hold_time_cool = oBis.new_property_type(
        code="HOLD_TIME",
        label="Holding time (s)",
        description="Holding time in seconds after cooling",
        dataType="REAL",
    )
    register_property_type(oBis, pt_hold_time_cool)  # TOBERECONSIDERED

    pt_hold_time_heat = oBis.new_property_type(
        code="HOLD_TIME_2",
        label="Holding time (s)",
        description="Holding time in seconds after heating",
        dataType="REAL",
    )
    register_property_type(oBis, pt_hold_time_heat)

    # Cutting

    # Sample height

    pt_height = oBis.new_property_type(
        code="SAMPLE_HEIGHT",
        label="Height (mm)",
        description="Height / Thickness of the cut out sample "
        "(Use Height Orientation to clarify the direction)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_height)

    # Sample height orientation
    # This could be process direction / crystal orientation

    pt_height_desc = oBis.new_property_type(
        code="HEIGHT_DESC",
        label="Height Description",
        description="Description of the direction of the height dimension",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_height_desc)

    # Small Diameter

    pt_diameter_min = oBis.new_property_type(
        code="DIAMETER_MIN",
        label="Diameter Min (mm)",
        description="Small Diameter of a Truncated Cone / " "Diameter of disk",
        dataType="REAL",
    )
    register_property_type(oBis, pt_diameter_min)

    # Large Diameter

    pt_diameter_max = oBis.new_property_type(
        code="DIAMETER_MAX",
        label="Diameter Max (mm)",
        description="Large Diameter of a Truncated Cone",
        dataType="REAL",
    )
    register_property_type(oBis, pt_diameter_max)

    # Sample length 1

    pt_length_1 = oBis.new_property_type(
        code="SAMPLE_LENGTH_1",
        label="Length 1 (mm)",
        description="Length (1/2)" "(Use Length1 Orientation to clarify the direction)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_length_1)

    # Sample length 1 orientation

    pt_length_desc_1 = oBis.new_property_type(
        code="LENGTH_1_DESC",
        label="Length 1 Description",
        description="Description of the direction of Length 1",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_length_desc_1)

    # Sample length 2

    pt_length_2 = oBis.new_property_type(
        code="SAMPLE_LENGTH_2",
        label="Length 2 (mm)",
        description="Length (2/2)" "(Use Length2 Orientation to clarify the direction)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_length_2)

    # Sample length 2 orientation

    pt_length_desc_2 = oBis.new_property_type(
        code="LENGTH_2_DESC",
        label="Length 2 Description",
        description="Description of the direction of Length 2",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_length_desc_2)

    # Gauge length for dog bone shaped samples

    pt_gauge_length = oBis.new_property_type(
        code="GAUGE_LENGTH",
        label="Gauge Length (mm)",
        description="Gauge length for dog bone shaped samples",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gauge_length)

    # Gauge width for dog bone shaped samples

    pt_gauge_width = oBis.new_property_type(
        code="GAUGE_WIDTH",
        label="Gauge Width (mm)",
        description="Gauge width for dog bone shaped samples",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gauge_width)

    # Cutting Technique

    pt_cut_tech = oBis.new_property_type(
        code="CUTTING_TECHNIQUE",
        label="Cutting Technique",
        description="Cutting Technique",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_cut_tech.code,
    )
    register_property_type(oBis, pt_cut_tech)

    # Cutting Geometry

    pt_cut_geometry = oBis.new_property_type(
        code="CUTTING_GEOMETRY",
        label="Cutting Geometry",
        description="Cutting Geometry",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_cut_geom.code,
    )
    register_property_type(oBis, pt_cut_geometry)

    # Crucible Material

    pt_crucible_material = oBis.new_property_type(
        code="CRUCIBLE_MATERIAL",
        label="Crucible Material",
        description="Crucible Material",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_crucible_material)

    # Crucible Position

    pt_crucible_position = oBis.new_property_type(
        code="CRUCIBLE_POSITION",
        label="Crucible Position",
        description="Crucible Position",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_crucible_position)

    # Gas flow

    pt_gaz_flow = oBis.new_property_type(
        code="GAZ_FLOW",
        label="Gas Flow Rate (L/h)",
        description="Gas flow rate in liters per hour",
        dataType="REAL",
    )
    register_property_type(oBis, pt_gaz_flow)

    # Mold Material

    pt_mold_material = oBis.new_property_type(
        code="MOLD_MATERIAL",
        label="Mold Material",
        description="Mold Material",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_mold_material)

    # FIB Milling Type

    pt_fib_milling_type = oBis.new_property_type(
        code="FIB_MILLING_TYPE",
        label="Milling Type",
        description="FIB Milling Type",
        dataType="CONTROLLEDVOCABULARY",
        vocabulary=voc_mill_type.code,
    )
    register_property_type(oBis, pt_fib_milling_type)

    # Protective Layer

    pt_protect_layer = oBis.new_property_type(
        code="PROTECT_LAYER",
        label="Protective Layer",
        description="Protective Layer",
        dataType="VARCHAR",
    )
    register_property_type(oBis, pt_protect_layer)

    # Protective Layer Thickness

    pt_protect_layer_thick = oBis.new_property_type(
        code="PROTECT_LAYER_THICK",
        label="Protective Layer Thickness (µm)",
        description="Protective Layer Thickness (µm)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_protect_layer_thick)

    # TEM Lamella Dimensions

    pt_protect_layer_thick = oBis.new_property_type(
        code="TEM_LAMELLA_DIMENSIONS",
        label="Lamella Dimensions (µm x µm x nm)",
        description="Lamella Dimensions (µm x µm x nm)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_protect_layer_thick)

    # Final (Cleaning) Ion Beam Energy

    pt_ion_beam_energy = oBis.new_property_type(
        code="ION_BEAM_ENERGY",
        label="Cleaning Ion Beam Energy (keV)",
        description="Cleaning Ion Beam Energy in keV",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ion_beam_energy)

    # Initial Ion Beam Energy

    pt_ion_beam_energy = oBis.new_property_type(
        code="INITIAL_ION_BEAM_ENERGY",
        label="Ion Beam Energy (keV)",
        description="Ion Beam Energy in keV",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ion_beam_energy)

    # Number of steps used milling

    pt_nsteps_mill = oBis.new_property_type(
        code="NUMBER_STEPS_MILL",
        label="Number of Steps",
        description="Number of Steps in FIB milling",
        dataType="INTEGER",
    )
    register_property_type(oBis, pt_nsteps_mill)

    # We don't add more steps to keep the ELN entry less cluttered

    MAX_NUM_STEPS_MILLING = 8
    for s_id in range(1, 1 + MAX_NUM_STEPS_MILLING):
        pt_ion_beam_current_step = oBis.new_property_type(
            code="ION_BEAM_CURRENT_%s" % s_id,
            label="Ion Beam Current %s (pA)" % s_id,
            description="Ion Beam Current  %s (pA)" % s_id,
            dataType="REAL",
        )
        register_property_type(oBis, pt_ion_beam_current_step)
    # Final (Cleaning) Ion Beam Current

    pt_ion_beam_current = oBis.new_property_type(
        code="ION_BEAM_CURRENT",
        label="Cleaning Ion Beam Current (pA)",
        description="Cleaning Ion Beam current in pA",
        dataType="REAL",
    )
    register_property_type(oBis, pt_ion_beam_current)

    # Feature Aspect Ratio

    pt_aspect_ratio = oBis.new_property_type(
        code="FEATURE_ASPECT_RATIO",
        label="Aspect Ratio",
        description="Aspect ratio of milled feature",
        dataType="REAL",
    )
    register_property_type(oBis, pt_aspect_ratio)

    # APT Tip Shank Angle

    pt_shank_angle = oBis.new_property_type(
        code="SHANK_ANGLE",
        label="Full Shank Angle (deg)",
        description="Full Shank Angle (deg)",
        dataType="REAL",
    )
    register_property_type(oBis, pt_shank_angle)

    # APT Tip Radius

    pt_tip_radius = oBis.new_property_type(
        code="APT_TIP_RADIUS",
        label="Tip Radius (nm)",
        description="APT Tip radius in nm",
        dataType="REAL",
    )
    register_property_type(oBis, pt_tip_radius)

    # #########################################################################
    # Objects
    # #########################################################################

    # Casting

    obj_casting_protocol = oBis.new_object_type(
        code=code_obj_casting,
        generatedCodePrefix=code_obj_casting,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_casting_protocol)
    obj_casting_protocol = oBis.get_object_type(code_obj_casting)
    obj_casting_protocol.description = "Casting / Meltng"
    register_object_type(oBis, obj_casting_protocol)

    obj_casting_protocol = oBis.get_object_type(code_obj_casting)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("$BARCODE", 0, None),
        ],
        "Casting Details": [
            ("CRUCIBLE_MATERIAL", 0, None),
            ("CRUCIBLE_POSITION", 0, None),
            ("GAS_EXP", 0, None),
            ("GAZ_FLOW", 0, None),
            ("MOLD_MATERIAL", 0, None),
        ],
        "Heating Details": [
            ("HOLD_TIME_2", 0, None),
            ("TEMP_START_2", 0, None),
            ("TEMP_STOP_2", 0, None),
            ("HEATING_RATE", 0, None),
            ("HEATING_PROFILE", 0, None),
            ("PRESSURE_2", 0, None),
        ],
        "Cooling Details": [
            ("COOLANT", 0, None),
            ("HOLD_TIME", 0, None),
            ("TEMP_START", 0, None),
            ("TEMP_STOP", 0, None),
            ("COOLING_RATE", 0, None),
            ("COOLING_PROFILE", 0, None),
            ("PRESSURE", 0, None),
        ],
        "Comments": [
            ("COMMENTS", 0, None),
            ("$XMLCOMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_casting_protocol, sec2props)

    # Heat Treatment / Annealing

    obj_heat_treatment_protocol = oBis.new_object_type(
        code=code_obj_heat_treatment,
        generatedCodePrefix=code_obj_heat_treatment,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_heat_treatment_protocol)
    obj_heat_treatment_protocol = oBis.get_object_type(code_obj_heat_treatment)
    obj_heat_treatment_protocol.description = "Heat Treatment / Annealing"
    register_object_type(oBis, obj_heat_treatment_protocol)

    obj_heat_treatment_protocol = oBis.get_object_type(code_obj_heat_treatment)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("$BARCODE", 0, None),
        ],
        "Heating Details": [
            ("HOLD_TIME_2", 0, None),
            ("TEMP_START_2", 0, None),
            ("TEMP_STOP_2", 0, None),
            ("HEATING_RATE", 0, None),
            ("HEATING_PROFILE", 0, None),
            ("PRESSURE_2", 0, None),
        ],
        "Cooling Details": [
            ("COOLANT", 0, None),
            ("HOLD_TIME", 0, None),
            ("TEMP_START", 0, None),
            ("TEMP_STOP", 0, None),
            ("COOLING_RATE", 0, None),
            ("COOLING_PROFILE", 0, None),
            ("PRESSURE", 0, None),
        ],
        "Comments": [
            ("COMMENTS", 0, None),
            ("$XMLCOMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_heat_treatment_protocol, sec2props)

    # Cutting

    obj_cutting_protocol = oBis.new_object_type(
        code=code_obj_cutting,
        generatedCodePrefix=code_obj_cutting,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_cutting_protocol)
    obj_cutting_protocol = oBis.get_object_type(code_obj_cutting)
    obj_cutting_protocol.description = "Metal Cutting"
    register_object_type(oBis, obj_cutting_protocol)

    obj_cutting_protocol = oBis.get_object_type(code_obj_cutting)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("$BARCODE", 0, None),
        ],
        "Cutting Details": [
            ("CUTTING_TECHNIQUE", 1, None),
            ("CUTTING_GEOMETRY", 1, None),
            ("SAMPLE_HEIGHT", 0, None),
            ("HEIGHT_DESC", 0, None),
            ("DIAMETER_MIN", 0, None),
            ("DIAMETER_MAX", 0, None),
            ("SAMPLE_LENGTH_1", 0, None),
            ("LENGTH_1_DESC", 0, None),
            ("SAMPLE_LENGTH_2", 0, None),
            ("LENGTH_2_DESC", 0, None),
            ("GAUGE_LENGTH", 0, None),
            ("GAUGE_WIDTH", 0, None),
        ],
        "Comments": [
            ("COMMENTS", 0, None),
            ("$XMLCOMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_cutting_protocol, sec2props)

    # FIB Milling

    obj_fib_milling_protocol = oBis.new_object_type(
        code=code_obj_fib_milling,
        generatedCodePrefix=code_obj_fib_milling,
        autoGeneratedCode=True,
    )
    register_object_type(oBis, obj_fib_milling_protocol)
    obj_fib_milling_protocol = oBis.get_object_type(code_obj_fib_milling)
    obj_fib_milling_protocol.description = "Focused Ion Beam Milling"
    register_object_type(oBis, obj_fib_milling_protocol)

    obj_fib_milling_protocol = oBis.get_object_type(code_obj_fib_milling)
    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("DESCRIPTION", 0, None),
            ("$BARCODE", 0, None),
        ],
        "General Milling Details": [
            ("FIB_MILLING_TYPE", 0, None),
            ("NUMBER_STEPS_MILL", 1, None),
            ("INITIAL_ION_BEAM_ENERGY", 0, None),
        ]
        + [
            ("ION_BEAM_CURRENT_%s" % s_id, 1 if s_id == 1 else 0, None)
            for s_id in range(1, 1 + MAX_NUM_STEPS_MILLING)
        ]
        + [
            ("PROTECT_LAYER", 0, None),
            ("PROTECT_LAYER_THICK", 0, None),
            ("ION_BEAM_ENERGY", 0, None),
            ("ION_BEAM_CURRENT", 0, None),
            ("TIME", 0, None),
        ],
        "APT Tip Details": [
            ("SHANK_ANGLE", 0, None),
            ("APT_TIP_RADIUS", 0, None),
        ],
        "TEM Lamella Details": [
            ("TEM_LAMELLA_DIMENSIONS", 0, None),
        ],
        "Micropillar Details": [
            ("FEATURE_ASPECT_RATIO", 0, None),
        ],
        "Comments": [
            ("COMMENTS", 0, None),
            ("$XMLCOMMENTS", 0, None),
        ],
    }
    assign_property_types(obj_fib_milling_protocol, sec2props)

    # #########################################################################
    # Dataset Types
    # #########################################################################

    ds_milling_recipe = oBis.new_dataset_type(
        code=code_ds_fib_milling,
        description="FIB Milling Recipe",
        mainDataSetPattern=None,
        mainDataSetPath=None,
        disallowDeletion=False,
        validationPlugin=None,
    )
    register_dataset_type(oBis, ds_milling_recipe)

    ds_milling_recipe = oBis.get_dataset_type(code_ds_fib_milling)

    # Define display order

    sec2props = {
        "General": [
            ("$NAME", 1, None),
            ("DATE", 0, None),
            ("COMMENTS", 0, None),
            ("S3_DOWNLOAD_LINK", 0, None),  # Download Link -> automatic script
        ],
        # 'Section Name': [
        #     ('PROPERTY_CODE', is_mandatory, plugin),
        # ],
    }
    assign_property_types(ds_milling_recipe, sec2props)
